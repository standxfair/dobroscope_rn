import { createStore, combineReducers, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'
import { composeWithDevTools } from 'redux-devtools-extension';

import { user } from '../reducers/user'
import { todos } from '../reducers/todos'
import { motivation } from '../reducers/motivation'
import { feed } from '../reducers/feed'
import { achievements } from '../reducers/achievements'

const rootReducer = combineReducers({
  user,
  todos,
  motivation,
  feed,
  achievements
})
const storeWithMiddleware = applyMiddleware(thunk)

const composeEnhancers = composeWithDevTools({
  name: 'dobroscope'
})

let store = {}
if (process.env.NODE_ENV !== 'production') {
  store = createStore(rootReducer, composeEnhancers(storeWithMiddleware))
} else {
  store = createStore(rootReducer, storeWithMiddleware)
}

export default store