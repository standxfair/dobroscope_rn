import { Dimensions } from 'react-native'

export const THEME = {
  MAIN_COLOR: '#FF4800',
  MAIN_COLOR_LIGHT: '#FFA27E',
  TEXT_COLOR_DARK: '#000',
  TEXT_COLOR_LIGHT: '#fff',
  GREY_DARK: '#7F7F7F',
  GREY_LIGHT: '#EEEEEE',
  BLUE_LIGHT: '#DEFAFF',
  DANGER_COLOR: '#FF0000',

  REGULAR: 'ceraPro-regular',
  BOLD: 'ceraPro-bold',
  BLACK: 'ceraPro-black',
  LIGHT: 'ceraPro-light',
  MEDIUM: 'ceraPro-medium',

  FONT_SIZES: {
    h1: getSize(24),
    h2: getSize(15),
    leadHeader: getSize(28),
    text: getSize(13),
    comment: getSize(10),
  },
  MODAL_WIDTH: Dimensions.get('window').width / 1.2,
}

export const INPUT_ERROR = {
  color: THEME.DANGER_COLOR,
  borderBottomColor: THEME.DANGER_COLOR
}

/** Рассчитываем глобальную переменную для адаптивной вёрстки */
const DESIGN_WIDTH = 320
const PHONE_MAX_WIDTH = 414 // iPhone 11 Pro, Pixel 2 (412)
global.SCREEN_RATIO = Dimensions.get('screen').width <= PHONE_MAX_WIDTH ?
  Dimensions.get('screen').width / DESIGN_WIDTH
  : 1.33


export function getSize(size) {
  return Math.round(size * (global.SCREEN_RATIO || 1))
}