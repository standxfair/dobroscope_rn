import moment from 'moment'
import { FIREBASE_URL } from '../constants/url'
import { DB_DATE_FORMAT } from '../constants/app'
import { FETCH_MOTIVATION_DICT, SET_TODAY_MOTIVATION, SET_MOTIVATION_LOADING } from '../constants/motivation'
import { getMotivation, updateMotivation, isAllDictShown, deleteMotivation, fillMotivation } from '../helpers/motivation'
import { isNeedAddNextDayMotivation } from '../helpers/dateTime'

export const setMotivationDict = () => {
  return async (dispatch) => {
    try {
      const response = await fetch(`${FIREBASE_URL}/motivationDict.json`, {
        method: 'GET',
        headers: { 'Content-Type': 'application/json' }
      })

      const dict = await response.json()

      dispatch({
        type: FETCH_MOTIVATION_DICT,
        payload: dict
      })

    } catch (error) {
      console.log('error', error);
    }
  }
}

export const setTodayMotivation = () => {
  return async (dispatch, getState) => {
    dispatch({
      type: SET_MOTIVATION_LOADING,
      payload: true
    })

    const dict = getState().motivation.dict
    const userId = getState().user.uid
    // получаем текущее состояние экрана мотивации с сервера
    let currentMotivation = await getMotivation(userId)

    if (!currentMotivation) {
      await updateMotivation(userId, dict[0], moment().format(DB_DATE_FORMAT))

      dispatch({
        type: SET_MOTIVATION_LOADING,
        payload: false
      })
      dispatch({
        type: SET_TODAY_MOTIVATION,
        payload: [{ item: { ...dict[0] }, date: moment().format(DB_DATE_FORMAT) }]
      })
    } else {
      let motivationItems = Object.keys(currentMotivation).map(key => ({ ...currentMotivation[key] }))

      // если весь словарь показан - обнуляем историю и показываем заново первый элемент словаря
      if (isAllDictShown(dict, motivationItems)) {
        await deleteMotivation(userId)
        await updateMotivation(userId, dict[0], moment().format(DB_DATE_FORMAT))

        dispatch({
          type: SET_TODAY_MOTIVATION,
          payload: [{ item: { ...dict[0] }, date: moment().format(DB_DATE_FORMAT) }]
        })

        currentMotivation = await getMotivation(userId)
        motivationItems = Object.keys(currentMotivation).map(key => ({ ...currentMotivation[key] }))
      }

      const lastItem = [...motivationItems].pop()
      // if currentDate !== lastItemDate
      if (isNeedAddNextDayMotivation(lastItem.date)) {
        const result = fillMotivation({ dateStart: lastItem.date, dict, history: motivationItems })
        result.forEach(async (element) => {
          await updateMotivation(userId, element.item, element.date)
        })
      }
      dispatch({
        type: SET_MOTIVATION_LOADING,
        payload: false
      })

      dispatch({
        type: SET_TODAY_MOTIVATION,
        payload: motivationItems
      })
    }
  }
}



