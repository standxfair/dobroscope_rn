import moment from 'moment'
import {
  SET_USER_DATA, SET_USER_STATUS, SET_USER_NAME, SET_USER_EMAIL, SET_USER_POSTS_COUNTER, SET_USER_LIKES_COUNTER, USER_MODEL,
  SET_AGREE_RECIVE_EMAILS, SET_AGREE_RECIVE_PUSH
} from '../constants/user'
import { TodosHelper, fetchUserById, isAllTodosDictShown } from '../helpers/todos'
import { setCurrentTodos, setCurrentCustomTodos, setTodoCounter, setFirstTodoDoneStatus, hideLoader } from '../actions/todosActions'
import { getLastLoginTime, setLastLoginTime, saveNewUser, setUserEmailAgreement, setUserPushAgreement } from '../helpers/user'
import { saveTodosState, clearTodayFinishedTodos } from '../helpers/todos'
import { isNextDayVisit } from '../helpers/dateTime'
import { setUserAchievs, getAchievsDict } from '../actions/achievementsActions'


export const setUserData = (user) => {
  return (dispatch) => {
    dispatch({
      type: SET_USER_DATA,
      payload: user
    })
  }
}



// регистрируем юзера в базе при первом входе
export const initializeUser = (newUser = false) => {
  return async (dispatch, getState) => {
    const userId = getState().user.uid
    const displayName = getState().user.displayName
    const todosDict = getState().todos.dict

    let lastLoginInTime = await getLastLoginTime(userId)
    if (!lastLoginInTime) {
      lastLoginInTime = moment().format()
    }
    const setNewUser = async () => {
      const todos = TodosHelper.setTodosForToday(todosDict)
      const activeIds = TodosHelper.getTodosIds(todos)

      await saveNewUser(userId, activeIds, displayName)

      setUserCounters(USER_MODEL.totalPosts, 'totalPosts')(dispatch, getState)
      setUserCounters(USER_MODEL.totalLikes, 'totalLikes')(dispatch, getState)
      setCurrentTodos(todos)(dispatch)
      setUserAchievs(USER_MODEL.achievements)(dispatch)
      getAchievsDict()(dispatch)
      hideLoader()(dispatch)
    }

    // новый пользователь
    if (!!newUser) {
      setNewUser()
      // зарегистрированный пользователь
    } else {
      if (!todosDict) {
        return
      }
      try {
        const userData = await fetchUserById(userId)
        if (userData) {
          let todos = []
          // сохраняем текущие задания
          if (userData.activeIds) {
            if (isNextDayVisit(lastLoginInTime)) {
              const newDayTodos = TodosHelper.getNewRandomTodos({
                dict: todosDict,
                finishedIds: userData.finishedIds
              })
              // обновлаяем активные задание в базе
              await saveTodosState(userId, newDayTodos.map(todo => todo.id), userData.finishedIds)

              // удаляем выполненные задания за прошлый день
              await clearTodayFinishedTodos(userId)
              todos = newDayTodos
            } else {
              todos = TodosHelper.setTodosForToday(
                todosDict,
                userData.activeIds,
                lastLoginInTime,
                userData.finishedIds
              )
            }
          } else {
            let finishedIds = userData.finishedIds ? [...userData.finishedIds] : []

            // если пользователь сделал вообще все задания из базы - показываем задания заново
            let resetTodos = false
            if (isAllTodosDictShown(todosDict, finishedIds)) {
              resetTodos = true
              finishedIds = []
            }

            todos = TodosHelper.setTodosForToday(
              todosDict,
              [],
              lastLoginInTime,
              finishedIds,
              resetTodos
            )

            // обновлаяем активные задание в базе
            await saveTodosState(userId, todos.map(todo => todo.id), finishedIds)
          }
          setCurrentTodos(todos)(dispatch)

          // сохраняем текущие пользовательские задания
          if (userData.customTodos) {
            let result = Object.keys(userData.customTodos).map(key => ({
              ...userData.customTodos[key], id: key
            }))

            result = TodosHelper.filterCustomTodosForToday(result, lastLoginInTime)

            setCurrentCustomTodos(result)(dispatch)
          }

          setUserCounters(userData.totalPosts, 'totalPosts')(dispatch, getState)
          setUserCounters(userData.totalLikes, 'totalLikes')(dispatch, getState)

          setTodoCounter(userData.totalCounter)(dispatch)

          setFirstTodoDoneStatus(userData.isFirstTodoDone)(dispatch)

          setUserAchievs(userData?.achievements || USER_MODEL.achievements)(dispatch)
          getAchievsDict()(dispatch)

          await setLastLoginTime(userId, moment().format())
          hideLoader()(dispatch)
        } else {
          setNewUser()
        }
      } catch (error) {
        console.log('error', error);
      }
    }
  }
}

export const setUserStatus = (status) => {
  return (dispatch) => {
    dispatch({
      type: SET_USER_STATUS,
      payload: status
    })
  }
}


export const setUserName = (value) => {
  return (dispatch) => {
    dispatch({
      type: SET_USER_NAME,
      payload: value
    })
  }
}


export const setUserEmail = (value) => {
  return (dispatch) => {
    dispatch({
      type: SET_USER_EMAIL,
      payload: value
    })
  }
}

export const setUserCounters = (value, type = 'totalPosts') => {
  return (dispatch, getState) => {
    let val

    if (value !== null) {
      val = value
    } {
      val = getState().user[type] + 1
    }

    dispatch({
      type: type === 'totalPosts' ? SET_USER_POSTS_COUNTER : SET_USER_LIKES_COUNTER,
      payload: val
    })
  }
}

export const setAgreeReviceEmails = (value) => {
  return async (dispatch, getState) => {

    const userId = getState().user.uid
    await setUserEmailAgreement(userId, value)

    dispatch({
      type: SET_AGREE_RECIVE_EMAILS,
      payload: value
    })
  }
}

export const setAgreeRevicePush = (value) => {
  return async (dispatch, getState) => {
    const userId = getState().user.uid
    await setUserPushAgreement(userId, value)

    dispatch({
      type: SET_AGREE_RECIVE_PUSH,
      payload: value
    })
  }
}