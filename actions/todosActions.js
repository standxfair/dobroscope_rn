import { FIREBASE_URL } from '../constants/url'
import { FETCH_TODOS_DICT, FETCH_TODOS, SET_CUSTOM_TODO, FETCH_CUSTOM_TODOS, UPDATE_TODO_COUNTER, SET_TODO_COUNTER, SET_FIRST_TODO_DONE_STATUS, DELETE_CUSTOM_TODO, SHOW_LOADER, HIDE_LOADER, FETCH_TODAY_TODOS, SET_DISABLE_TODOS_CHECK } from '../constants/todos'
import { TodosHelper, saveTodosState, updateCustomTodo, pushCustomTodo, updateCounter, checkFirstTodoDone, deleteCustomTodo, saveTodoForTodayFinished, deleteTodayFinishedTodo } from '../helpers/todos'
import { processAchievs } from './achievementsActions'
import { DB_DATE_FORMAT } from '../constants/app'
import moment from 'moment'


export const getTodosDict = () => {
  return async (dispatch) => {
    let data
    let response

    try {
      response = await fetch(`${FIREBASE_URL}/todosDict.json`, {
        method: 'GET',
        headers: { 'Content-Type': 'application/json' }
      })
    } catch (error) {
      console.log('error', error);
    }

    data = await response.json()

    if (data) {
      dispatch({
        type: FETCH_TODOS_DICT,
        payload: data
      })
    }
  }
}

export const setCurrentTodos = (todos) => {
  return (dispatch) => {
    dispatch({
      type: FETCH_TODOS,
      payload: todos
    })
  }
}

export const setTodayTodos = (todayTodos) => {
  return async (dispatch) => {
    dispatch({
      type: FETCH_TODAY_TODOS,
      payload: todayTodos
    })
  }
}

export const setCurrentCustomTodos = (todos) => {
  return (dispatch) => {
    dispatch({
      type: FETCH_CUSTOM_TODOS,
      payload: todos
    })
  }
}

export const setCheckedTodo = (
  id,
  setIsModalOpen,
  setIsDoneModalOpen,
  isCustom = false,
  todoTitle,
  ahievObserve
) => {
  return async (dispatch, getState) => {
    const { currentTodos } = getState().todos
    const { customTodos } = getState().todos
    const { isFirstTodoDone } = getState().todos
    const { todayTodos } = getState().todos
    const userId = getState().user.uid

    let todos_ = !isCustom ? [...currentTodos] : [...customTodos]
    let isPlus = false

    todos_.forEach((todo) => {
      if (todo.id === id) {
        if (!todo.isChecked) {
          isPlus = true
        }
        todo.isChecked = !todo.isChecked
      }
    })

    //обновляем общий счетчик выполненных заданий
    await updateTodoCounter(isPlus)(dispatch, getState)
    const totalCounter = getState().todos.totalCounter
    await processAchievs({ totalCounter, userId, callback: ahievObserve })(dispatch, getState)


    // показваем модалку при выполнении первого задания
    if (todos_.filter(todo => todo.isChecked === true).length === 1 && !isFirstTodoDone) {
      setTimeout(() => {
        setIsModalOpen(true)
      }, 600)

      setFirstTodoDoneStatus(true, true)(dispatch, getState)
    }
    if (todos_.filter(todo => (todo.isChecked === true)).length === (currentTodos.length) && !isCustom) {
      setIsDoneModalOpen(true)
    }

    try {
      if (!isCustom) {
        const activeIds = TodosHelper.getTodosIds(todos_)
        const finishedIds = TodosHelper.getTodosIds(todos_, false)

        await saveTodosState(userId, activeIds, finishedIds)
      } else {
        await updateCustomTodo(userId, id)
      }

      //сохраняю в выполненные на сегодня
      if (isPlus) {
        const date = moment().format(DB_DATE_FORMAT)
        await saveTodoForTodayFinished(userId, todoTitle, id, date)
      } else {
        //удаляю из выполненных на сегодня
        const todoId = todayTodos.find((todo => todo.title === todoTitle)).id
        await deleteTodayFinishedTodo(userId, todoId)
      }
    } catch (error) {
      console.log('error', error);
    }

    dispatch({
      type: !isCustom ? FETCH_TODOS : FETCH_CUSTOM_TODOS,
      payload: todos_
    })
  }
}

export const addCustomTodo = (title) => {
  return async (dispatch, getState) => {
    const userId = getState().user.uid
    const date = moment().format(DB_DATE_FORMAT)
    const data = await pushCustomTodo(userId, title, date)
    if (data.name) {
      dispatch({
        type: SET_CUSTOM_TODO,
        payload: { id: data.name, title, date }
      })
    }
  }
}


export const setTodoCounter = (counter) => {
  return async (dispatch, getState) => {
    dispatch({
      type: SET_TODO_COUNTER,
      payload: counter
    })
  }
}

export const updateTodoCounter = (isPlus = false) => {
  return async (dispatch, getState) => {
    const userId = getState().user.uid
    let totalCounter = getState().todos.totalCounter

    if (isPlus) {
      totalCounter += 1
    } else if (totalCounter !== 0) {
      totalCounter -= 1
    }

    dispatch({
      type: SET_DISABLE_TODOS_CHECK,
      payload: true
    })
    try {
      let resut = null
      resut = await updateCounter(userId, totalCounter)
      if (resut) {
        dispatch({
          type: SET_DISABLE_TODOS_CHECK,
          payload: false
        })
      }
    } catch (error) {
      console.log(error)
    }



    dispatch({
      type: UPDATE_TODO_COUNTER,
      payload: totalCounter
    })
  }
}


export const setFirstTodoDoneStatus = (status, needUpdate = false) => {
  return async (dispatch, getState) => {
    if (needUpdate) {
      const userId = getState().user.uid
      await checkFirstTodoDone(userId)
    }

    dispatch({
      type: SET_FIRST_TODO_DONE_STATUS,
      payload: status
    })
  }
}


export const delCustomTodo = (todoId) => {
  return async (dispatch, getState) => {
    const userId = getState().user.uid
    // updateTodoCounter()(dispatch, getState)

    await deleteCustomTodo(userId, todoId)

    dispatch({
      type: DELETE_CUSTOM_TODO,
      payload: todoId
    })
  }
}

export const showLoader = () => {
  return (dispatch) => {
    dispatch({
      type: SHOW_LOADER
    })
  }
}

export const hideLoader = () => {
  return (dispatch) => {
    dispatch({
      type: HIDE_LOADER
    })
  }
}