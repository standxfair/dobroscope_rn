import { FIREBASE_URL } from '../constants/url'
import { GET_ACHIEVS, GET_ACHIEVS_DICT, SET_ACHIEVS } from '../constants/achievements'

export const getAchievsDict = () => {
  return async (dispatch) => {
    try {
      const response = await fetch(`${FIREBASE_URL}/achievements.json`, {
        method: 'GET',
        headers: { 'Content-Type': 'application/json' }
      })

      const dict = await response.json()

      dispatch({
        type: GET_ACHIEVS_DICT,
        payload: dict
      })

    } catch (error) {
      console.log('error', error);
    }
  }
}

export const setUserAchievs = (data) => {
  return (dispatch) => {
    dispatch({
      type: GET_ACHIEVS,
      payload: data
    })
  }
}

export const processAchievs = ({ totalCounter, callback = () => { }, userId }) => {
  return async (dispatch, getState) => {
    const dict = getState().achievements.dict
    const userAchievements = getState().achievements.achievements
    if (totalCounter === 10 || totalCounter === 50 || totalCounter === 100) {
      const achiev = dict.find(el => parseInt(el.id, 10) === totalCounter)
      if (!userAchievements.includes(achiev.id)) {
        callback(achiev)
        const data = [...userAchievements]
        data.push(achiev.id)

        dispatch({
          type: SET_ACHIEVS,
          payload: data
        })
        try {
          const response = await fetch(`${FIREBASE_URL}/users/${userId}.json`, {
            method: 'PATCH',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ achievements: data })
          })
        } catch (error) {
          console.log('error', error);
        }
      }
    }
  }
}



