import moment from 'moment'
import _ from 'underscore'
import * as firebase from 'firebase';
import { FETCH_FEED, ADD_ITEM_TO_FEED, ADD_COMMENT, LIKE_POST } from '../constants/feed'
import { postFeedItem, postComment, setIsLiked, getPostLikes } from '../helpers/feed'
import { DB_DATE_TIME_FORMAT, DB_DATE_FORMAT } from '../constants/app'
import { getUserAvatar } from '../helpers/user'
import { setUserCounters } from './userActions'
import { addUserTotalPostsCounter, addUserTotalLikesCounter } from '../helpers/user'


export const fetchFeed = (feed) => {
  return async (dispatch) => {
    const payload = { feed, loading: false }

    dispatch({
      type: FETCH_FEED,
      payload,
    })
  }
}

export const addNewItem = (item) => {
  return async (dispatch, getState) => {
    const userId = getState().user.uid
    const userFirebase = firebase.auth().currentUser;
    const avatar = await getUserAvatar(userId)

    const item_ = {
      ...item,
      userId,
      createDate: moment().format('DD MMMM YYYY'),
      date: moment().format(DB_DATE_TIME_FORMAT),
      author: userFirebase.displayName,
      avatar
    }

    const id = await postFeedItem({ ...item_ })

    if (id) {
      dispatch({
        type: ADD_ITEM_TO_FEED,
        payload: { ...item_, id }
      })


      setUserCounters(null, 'totalPosts')(dispatch, getState)

      const totalPosts = getState().user.totalPosts
      await addUserTotalPostsCounter(userId, totalPosts)

    }
  }
}

export const addNewComment = (text, itemId) => {
  return async (dispatch, getState) => {

    const userId = getState().user.uid
    const userFirebase = firebase.auth().currentUser;

    let item = {
      text,
      userId,
      createDate: moment().format('DD MMMM YYYY'),
      date: moment().format(DB_DATE_FORMAT),
      author: userFirebase.displayName,
    }

    const id = await postComment({
      ...item
    }, itemId)

    if (id) {
      item.id = id

      dispatch({
        type: ADD_COMMENT,
        payload: { comment: item, itemId }
      })
    }
  }
}

export const likePost = (postId) => {
  return async (dispatch, getState) => {
    const userId = getState().user.uid

    const currentLikes = await getPostLikes(postId)

    let isUserLiked = false
    let result = []

    if (currentLikes) {
      result = [...currentLikes]
    }

    if (result.find(item => item === userId)) {
      result = _.without(result, userId)
      isUserLiked = false
    } else {
      result.push(userId)
      isUserLiked = true
    }

    await setIsLiked(postId, result)

    setUserCounters(null, 'totalLikes')(dispatch, getState)

    const totalLikes = getState().user.totalPosts
    await addUserTotalLikesCounter(userId, totalLikes)

    dispatch({
      type: LIKE_POST,
      payload: { postId, isUserLiked, likesCount: result.length }
    })
  }
}