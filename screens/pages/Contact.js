import React, { useState } from 'react';
import { StyleSheet, View, TextInput, Image, ScrollView, Alert } from 'react-native';
import moment from 'moment'
import { useSelector } from 'react-redux'
import { AppTextBold } from '../../components/ui/AppTextBold'
import { AppText } from '../../components/ui/AppText'
import { THEME, getSize, INPUT_ERROR } from '../../styles/theme';
import { AppButton } from '../../components/AppButton'
import { CONTACTS_SHCEMA } from '../../constants/validation'
import { validateHandler } from '../../helpers/validateHandler'
import { sendFeedbackMessage } from '../../helpers/feedback'
import { DB_DATE_TIME_FORMAT } from '../../constants/app'

export const Contact = () => {
  const defaultUserName = useSelector(state => state.user.displayName)
  const [userName, setUserName] = useState(defaultUserName ? defaultUserName : '')
  const [email, setEmail] = useState('')
  const [message, setMessage] = useState('')
  const [errors, setErrors] = useState({})

  const inputHandler = (type = 'name') => (text) => {
    validateHandler(type, text, CONTACTS_SHCEMA, errors, setErrors)

    if (type === 'name') {
      setUserName(text)
    } else if (type === 'email') {
      setEmail(text)
    } else {
      setMessage(text)
    }
  }

  const submitHandler = () => {
    const validate = validateHandler(null, null, CONTACTS_SHCEMA, errors, setErrors, { email, name: userName, message })
    if (validate) {
      sendFeedbackMessage({
        email,
        userName,
        message,
        createDate: moment().format('DD MMMM YYYY'),
        date: moment().format(DB_DATE_TIME_FORMAT),
        isNew: true,
      })
      Alert.alert('Ваше сообщение отправлено', '', [{
        text: 'OK',
        onPress: () => {
          setUserName('')
          setEmail('')
          setMessage('')
        }
      }])
    }
  }

  return (
    <ScrollView style={styles.scrollContainer}>
      <View style={styles.container}>
        <View>
          <AppTextBold style={styles.header}>
            Написать разработчикам
          </AppTextBold>
        </View>
        <View>
          <AppText style={styles.text}>
            Мы будем рады ответить на ваши вопросы, ознакомиться с вашими идеями по развитию приложения или просто прочитать ваш отзыв.
          </AppText>
        </View>
        <View style={styles.formWrapper}>
          <View style={styles.formInputWrapper}>
            <AppText style={styles.formLabel}>
              Ваше имя
            </AppText>
            <TextInput
              style={!errors.name ? styles.formInput : { ...styles.formInput, ...INPUT_ERROR }}
              value={userName}
              onChangeText={text => inputHandler('name')(text)}
              autoCorrect={false}
            />
          </View>
          <View style={styles.formInputWrapper}>
            <AppText style={styles.formLabel}>
              Электронная почта
            </AppText>
            <TextInput
              style={!errors.email ? styles.formInput : { ...styles.formInput, ...INPUT_ERROR }}
              value={email}
              onChangeText={text => inputHandler('email')(text)}
              autoCorrect={false}
              autoCapitalize='none'
              keyboardType='email-address'
            />
          </View>
          <View style={styles.formInputWrapper}>
            <AppText style={styles.formLabel}>
              Сообщение
            </AppText>
            <TextInput
              style={!errors.message ? styles.formInput : { ...styles.formInput, ...INPUT_ERROR }}
              value={message}
              onChangeText={text => inputHandler('message')(text)}
              autoCorrect={false}
              placeholder='Текст сообщения'
              multiline={true}
              numberOfLines={4}
            />
          </View>
        </View>
        <AppButton style={styles.button} title='Отправить' onPress={submitHandler} />
      </View>
    </ScrollView>
  )
}

const styles = StyleSheet.create({
  scrollContainer: {
    backgroundColor: THEME.TEXT_COLOR_LIGHT,
  },
  container: {
    paddingHorizontal: getSize(20),
    paddingBottom: getSize(60),
    backgroundColor: THEME.TEXT_COLOR_LIGHT,
    paddingTop: getSize(20),
    flex: 1,
  },
  header: {
    fontSize: THEME.FONT_SIZES.h1,
    paddingBottom: getSize(20),
  },
  text: {
    fontSize: THEME.FONT_SIZES.text,
    paddingBottom: getSize(20),
    lineHeight: getSize(19),
  },
  formLabel: {
    color: THEME.MAIN_COLOR_LIGHT,
    fontSize: getSize(12),
    textTransform: 'uppercase',
  },
  formInput: {
    borderBottomColor: THEME.MAIN_COLOR_LIGHT,
    borderBottomWidth: getSize(2),
    borderStyle: 'solid',
    marginBottom: getSize(20),
    color: THEME.TEXT_COLOR_DARK,
    fontSize: getSize(13),
    paddingTop: getSize(10),
    fontFamily: THEME.REGULAR
  },
  bgImageStyle: {
    position: 'absolute',
    bottom: getSize(20),
    right: getSize(-90),
    zIndex: 1,
  },
})

