import React, { useState, useEffect } from 'react';
import { StyleSheet, View, ScrollView } from 'react-native';
import { AppTextBold } from '../../components/ui/AppTextBold'
import { AppText } from '../../components/ui/AppText'
import { THEME, getSize } from '../../styles/theme';
import TextsApi from "../../helpers/texts";

export const Terms = () => {
  const [text, setText] = useState('')

  useEffect(() => {
    async function fetchText() {
      const res = await TextsApi.getAgreementText()
      setText(res)
    }
    fetchText()
  }, [])

  return (
    <ScrollView style={styles.container}>
      <View>
        <AppTextBold style={styles.header}>
          Пользовательское
          соглашение
        </AppTextBold>
      </View>
      <View style={styles.textContainer}>
        <AppText style={styles.text}>
          {text}
        </AppText>
      </View>
    </ScrollView>
  )
}

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: getSize(20),
    paddingBottom: getSize(60),
    backgroundColor: THEME.TEXT_COLOR_LIGHT,
    paddingTop: getSize(20),
    flex: 1,
  },
  header: {
    fontSize: THEME.FONT_SIZES.h1,
    paddingBottom: getSize(20),
  },
  textContainer: {
    paddingBottom: getSize(80),
  },
  text: {
    fontSize: THEME.FONT_SIZES.text,
    paddingBottom: getSize(20),
    lineHeight: getSize(16),
  }
})