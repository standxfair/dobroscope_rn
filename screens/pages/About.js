import React, { useState, useEffect } from 'react';
import { StyleSheet, View, ScrollView, TouchableOpacity, Linking, Image } from 'react-native';
import { AppTextBold } from '../../components/ui/AppTextBold'
import { AppText } from '../../components/ui/AppText'
import { THEME, getSize } from '../../styles/theme';
import TextsApi from "../../helpers/texts";
import { externalLinks } from '../../constants/url'
import VKicon from '../../assets/VK.svg'
import FacebookIcon from '../../assets/Facebook.svg'
import TelegramIcon from '../../assets/Telegram.svg'
import InstagramIcon from '../../assets/Instagram.svg'


export const About = () => {
  const [text, setText] = useState('')

  const goToWeb = (type) => async () => {
    let link = ''
    if (type === 'homepage') {
      link = externalLinks.HOMEPAGE
    }
    if (type === 'vk') {
      link = externalLinks.VK_GROUP
    }
    if (type === 'tg') {
      link = externalLinks.TG_LINK
    }
    if (type === 'ig') {
      link = externalLinks.INSAGRAM_LINK
    }

    if (link) await Linking.openURL(link)
  }

  useEffect(() => {
    async function fetchText() {
      const res = await TextsApi.getAboutText()
      setText(res)
    }
    fetchText()
  }, [])

  return (
    <ScrollView style={styles.container}>
      <View style={styles.padedWrapper}>
        <View>
          <AppTextBold style={styles.header}>
            О проекте
          </AppTextBold>
        </View>
        <View style={styles.textContainer}>
          <AppText style={styles.text}>
            {text}
          </AppText>
        </View>
        <View style={styles.quote}>
          <AppTextBold style={styles.quoteText}>
            Обратить свои мысли к другим — лучший способ отвлечься от собственных проблем
          </AppTextBold>
        </View>
        <View>
          <AppText style={styles.siteHeader}>
            Наш сайт
          </AppText>
        </View>
        <TouchableOpacity onPress={goToWeb('homepage')}>
          <AppText style={styles.siteLink}>
            dobroscope.com
          </AppText>
        </TouchableOpacity>
        <View>
          <AppText style={styles.siteHeader}>
            Ищите нас в соцсетях
          </AppText>
        </View>
        <View style={styles.social}>
          <View style={styles.socialIcon}>
            <TouchableOpacity onPress={goToWeb('vk')}>
              <VKicon />
            </TouchableOpacity>
          </View>
          {/* <View style={styles.socialIcon}>
            <TouchableOpacity onPress={() => { }}>
              <FacebookIcon />
            </TouchableOpacity>
          </View> */}
          <View style={styles.socialIcon}>
            <TouchableOpacity onPress={goToWeb('tg')}>
              <TelegramIcon />
            </TouchableOpacity>
          </View>
          <View style={styles.socialIcon}>
            <TouchableOpacity onPress={goToWeb('ig')}>
              <InstagramIcon />
            </TouchableOpacity>
          </View>
        </View>
        <Image
          style={styles.bgImageStyle}
          source={require('../../assets/bg_menu.png')}
        />
      </View>
    </ScrollView>
  )
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: THEME.TEXT_COLOR_LIGHT,
    paddingTop: getSize(20),
    flex: 1,
  },
  padedWrapper: {
    paddingHorizontal: getSize(20),
    paddingBottom: getSize(120),
  },
  header: {
    fontSize: THEME.FONT_SIZES.h1,
    paddingBottom: getSize(20),
  },
  textContainer: {
    paddingBottom: getSize(10),
  },
  text: {
    fontSize: THEME.FONT_SIZES.text,
    paddingBottom: getSize(20),
    lineHeight: getSize(16),
  },
  quote: {
    backgroundColor: THEME.BLUE_LIGHT,
    borderRadius: getSize(100),
    flex: 2,
    zIndex: 1,
    marginLeft: getSize(-90),
    marginBottom: getSize(20),
    paddingLeft: getSize(90),
    paddingRight: getSize(0),
    paddingTop: getSize(20),
    paddingBottom: getSize(40)
  },
  quoteText: {
    fontSize: getSize(20),
    color: THEME.MAIN_COLOR,
    lineHeight: getSize(25),
  },
  siteHeader: {
    fontSize: getSize(12),
    color: THEME.GREY_DARK,
    paddingBottom: getSize(10)
  },
  siteLink: {
    fontSize: getSize(16),
    color: THEME.MAIN_COLOR,
    textDecorationLine: 'underline',
    paddingBottom: getSize(20)
  },
  social: {
    flexDirection: 'row',
  },
  socialIcon: {
    marginRight: getSize(10),
    borderWidth: 2,
    borderColor: THEME.MAIN_COLOR,
    borderRadius: 100,
  },
  bgImageStyle: {
    display: Platform.OS === 'ios' ? 'flex' : 'none',
    position: 'absolute',
    bottom: getSize(10),
    right: getSize(-90),
    zIndex: 1,
  }
})