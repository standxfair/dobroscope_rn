import React from 'react';
import { StyleSheet, Dimensions, View } from 'react-native';
import { useSelector } from 'react-redux'
import { AppTextBold } from '../../../components/ui/AppTextBold'
import { AppText } from '../../../components/ui/AppText'
import { THEME, getSize } from '../../../styles/theme'

const HelloText = ({
  description = 'Сегодня отличный день, чтобы сделать несколько добрых дел'
}) => {
  const userName = useSelector(state => state.user.displayName)
  const count = useSelector(state => state.todos.totalCounter)

  return (
    <View style={count !== 0 ? styles.container : { ...styles.container, paddingBottom: getSize(20) }}>
      <AppTextBold style={styles.header}>Привет{userName ? ', ' + userName : ''}</AppTextBold>
      <AppText style={styles.description}>{description}</AppText>
    </View>
  )
};


const styles = StyleSheet.create({
  container: {
    paddingHorizontal: getSize(20),
    backgroundColor: THEME.TEXT_COLOR_LIGHT,
    paddingTop: getSize(20),
  },
  header: {
    fontSize: THEME.FONT_SIZES.h1,
    paddingBottom: getSize(10),
  },
  description: {
    fontSize: getSize(12),
    width: Dimensions.get('window').width / 1.5,
    textAlign: 'left',
  },
});

export default HelloText