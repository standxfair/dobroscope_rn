import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux'
import * as firebase from 'firebase';
import { StyleSheet, View, Alert, ScrollView, ImageBackground, Image } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import moment from 'moment'
import { AppButton } from '../../../components/AppButton';
import AddTodo from '../../../components/AddTodo'
import Todo from '../../../components/Todo'
import { THEME, getSize } from '../../../styles/theme';
import { LinkButton } from '../../../components/LinkButton'
import { TodoModal } from '../../../components/Modals/TodoModal';
import { AppTextLight } from '../../../components/ui/AppTextLight'
import { AppTextBold } from '../../../components/ui/AppTextBold'
import { AppText } from '../../../components/ui/AppText'
import { setCheckedTodo, addCustomTodo, delCustomTodo, setTodayTodos } from '../../../actions/todosActions'
import { TODOS_MIN_LENGTH } from '../../../constants/app';
import { SvgUri } from 'react-native-svg';
import { getFilteredByTodayTodos } from '../../../selectors/todos'



const bgImage = require('../../../assets/todoBg.png')

const TodayList = ({ navigation }) => {
  const currentTodos = useSelector(state => state.todos.currentTodos)
  const customTodos = useSelector(state => state.todos.customTodos)
  const loading = useSelector(state => state.todos.loading)
  const isDisable = useSelector(state => state.todos.isDisable)
  const todayFinishedTodos = useSelector(state => getFilteredByTodayTodos(state))
  const userId = useSelector(state => state.user.uid)

  const [isModalOpen, setIsModalOpen] = useState(false)
  const [isAchivModalOpen, setisAchivModalOpen] = useState(null)
  const [isDoneModalOpen, setIsDoneModalOpen] = useState(false)
  const [shareTextModalOpen, setShareTextModalOpen] = useState(false)
  const [listHeight, setListHeight] = useState(null)
  const dispatch = useDispatch()

  const fetchTodayTodos = (todayTodos) => dispatch(setTodayTodos(todayTodos))

  useEffect(() => {
    const unsubscribe = firebase
      .database()
      .ref('users/' + userId + '/todayFinishedTodos/')
      .on('value', async (snapshot) => {
        let data
        if (snapshot) {
          data = snapshot.val();
        }
        const result = data
          ? Object.keys(data).map(id => ({ id, title: data[id].title, date: data[id].date }))
          : null

        fetchTodayTodos(result)
      });

    return () => unsubscribe()
  }, [])

  const addTodo = (title) => {
    dispatch(addCustomTodo(title))
  }

  const ahievObserve = (achieve) => {
    if (isAchivModalOpen) {
      setisAchivModalOpen(null)
    }
    setisAchivModalOpen(achieve)
  }

  const checkTodo = (type) => (id, todoTitle) => {
    dispatch(setCheckedTodo(
      id,
      setIsModalOpen,
      setIsDoneModalOpen,
      type === 'custom' ? true : false,
      todoTitle,
      ahievObserve
    ))
  }

  const deleteTodo = (type) => (id) => {
    Alert.alert(
      "Внимание!",
      "Вы уверены, что хотите удалить элемент?",
      [
        {
          text: "Отмена",
          onPress: () => { },
          style: "cancel"
        },
        {
          text: "Удалить",
          onPress: async () => {
            if (type === 'custom') {
              await dispatch(delCustomTodo(id))
            } else {

            }
          }
        }
      ],
      { cancelable: false }
    );
  }

  const countCheckedTodos = () => {
    let counter = 0

    currentTodos.forEach((todo) => {
      if (todo.isChecked) {
        counter += 1
      }
    })

    customTodos.forEach((todo) => {
      if (todo.isChecked) {
        counter += 1
      }
    })

    return counter
  }

  const countDoneTodos = () => {
    let result = TODOS_MIN_LENGTH + customTodos.length
    if (currentTodos.length < TODOS_MIN_LENGTH && customTodos.length) {
      result = customTodos.length
    }

    return result
  }

  return (
    <KeyboardAwareScrollView
      enableOnAndroid={true}
      extraHeight={getSize(listHeight * (currentTodos.length + customTodos.length)) / 2}
    >
      <View style={styles.block}>
        <ImageBackground
          style={styles.bgImageStyle}
          source={bgImage}
          imageStyle={styles.bgImage}
        >
        </ImageBackground>
        <AppTextLight style={styles.date}>{moment().format('DD MMMM')}</AppTextLight>
        <View style={styles.headerContainer}>
          <AppTextBold style={styles.header}>Тебя ждут новые дела</AppTextBold>
          <AppText style={styles.counter}>
            <AppTextBold style={styles.counterFirst}>{countCheckedTodos()}</AppTextBold>
            /{countDoneTodos()}
          </AppText>
        </View>

        <AppText style={styles.description}>
          Мы приготовили для тебя список добрых дел
          на сегодня. Ты можешь сделать одно из них или все.
        </AppText>

        <ScrollView>
          {
            !currentTodos.length
              ? loading ? (
                <AppText style={{ color: THEME.TEXT_COLOR_LIGHT }}>
                  Загрузка списка дел...
                </AppText>
              ) : !customTodos.length
                ? (
                  <AppText style={{ color: THEME.TEXT_COLOR_LIGHT }}>
                    Ты сделал все дела на сегодня
                  </AppText>
                )
                : null
              : currentTodos.map((todo) => (
                <Todo
                  key={todo.id}
                  todo={todo}
                  checkTodo={checkTodo()}
                  deleteTodo={deleteTodo()}
                  isDisable={isDisable}
                />
              ))
          }
          {customTodos.length ? customTodos.map((todo) => (
            <Todo
              key={todo.id}
              todo={todo}
              checkTodo={checkTodo('custom')}
              deleteTodo={deleteTodo('custom')}
              isDisable={isDisable}
            />
          )) : null}
        </ScrollView>

        <AddTodo onSubmit={addTodo} setListHeight={setListHeight} />

        {todayFinishedTodos?.length ? (
          <View>
            <View style={styles.descriptionRow}>
              <AppText style={styles.descriptionRowMarker}>—</AppText>
              <AppText style={styles.descriptionRowText}>
                Ты делаешь мир лучше. Расскажи об этом друзьям не все знают, что это так просто!
              </AppText>
            </View>
            <AppButton style={
              {
                shareButtonWrapper: { ...styles.shareButtonWrapper },
                shareButton: { ...styles.shareButton },
                shareButtonText: { ...styles.shareButtonText }
              }
            }
              title='Рассказать друзьям'
              onPress={() => navigation.navigate('ShareTodosList')}
            />

            <LinkButton onPress={() => setShareTextModalOpen(true)} />
          </View>
        ) : null}

        <TodoModal
          modalVisible={isModalOpen}
          onClose={() => { setIsModalOpen(false) }}
          title='Поздравляем!'
          description='Ты отлично выполнил первое задание'
          img={
            <Image source={require('../../../assets/big_logo_modal.png')} />
          }
          footerText='Хорошее начало! У тебя отлично получается, продолжай в том же духе!'
        />

        <TodoModal
          modalVisible={isAchivModalOpen ? true : false}
          onClose={() => { setisAchivModalOpen(null) }}
          title='Ура! Новая награда!'
          description='Супер-прогресс, мы хотим вручить тебе награду'
          img={
            <SvgUri uri={isAchivModalOpen ? isAchivModalOpen.image : ''} />
          }
          footerText=''
          achivTitle={isAchivModalOpen ? isAchivModalOpen.title : ''}
        />

        <TodoModal
          modalVisible={isDoneModalOpen}
          onClose={() => { setIsDoneModalOpen(false) }}
          title='Вот это да!'
          description='Ты сделал все задания на сегодня'
          countComponent={
            <View style={{ textAlign: 'center' }}>
              <AppText style={{ color: THEME.MAIN_COLOR, fontSize: 75, }}>
                <AppTextBold style={{ color: THEME.MAIN_COLOR, fontSize: 116, }}>{countCheckedTodos()}</AppTextBold>/{currentTodos.length + customTodos.length}
              </AppText>
            </View>
          }
          footerText='Такие люди как ты делают этот мир лучше'
        />

        <TodoModal
          modalVisible={shareTextModalOpen}
          onClose={() => { setShareTextModalOpen(false) }}
          title='Почему это не дурной тон'
          description='Мы предлагаем рассказывать о добрых делах в своих соцсетях не для того, чтобы похвастать своими поступками, а для того чтобы вовлечь других в движение доброты.'
          secondaryText={'Возможно, кто-то ещё не знает, что делать хорошие дела это просто и приятно. В повседневной суете мы часто забываем о том, что кому-то ещё нужна помощь, что мы можем простыми действиями сделать мир чуточку лучше.'}
          descriptionTextAlign={'left'}
        />

      </View>

    </KeyboardAwareScrollView>
  )
};

export default TodayList


const styles = StyleSheet.create({
  block: {
    backgroundColor: THEME.MAIN_COLOR,
    paddingHorizontal: getSize(20),
    paddingVertical: getSize(40),
  },
  bgImageStyle: {
    marginLeft: getSize(-200),
  },
  bgImage: {
    height: getSize(514),
    width: getSize(407),
    position: 'absolute',
    marginTop: getSize(150),
  },
  date: {
    color: THEME.TEXT_COLOR_LIGHT,
    fontSize: getSize(12),
  },
  headerContainer: {
    flexDirection: 'row'
  },
  header: {
    color: THEME.TEXT_COLOR_LIGHT,
    fontSize: getSize(28),
    flex: 2,
    paddingTop: getSize(5),
  },
  counter: {
    flex: 1,
    fontSize: getSize(36),
    color: THEME.TEXT_COLOR_LIGHT,
    textAlign: 'right',
    alignItems: 'center',
    justifyContent: 'flex-end'
  },
  counterFirst: {
    color: THEME.TEXT_COLOR_LIGHT,
    fontSize: getSize(56),
  },
  description: {
    fontSize: getSize(12),
    color: THEME.TEXT_COLOR_LIGHT,
    paddingVertical: getSize(20),
  },
  descriptionRow: {
    flexDirection: 'row',
    paddingBottom: getSize(10),
  },
  descriptionRowMarker: {
    flex: 1,
    textAlign: 'right',
    paddingRight: getSize(5),
    color: THEME.TEXT_COLOR_LIGHT,
  },
  descriptionRowText: {
    flex: 2,
    justifyContent: 'flex-end',
    color: THEME.TEXT_COLOR_LIGHT,
  },
  shareButton: {
    marginRight: getSize(25),
    marginLeft: getSize(25),
    paddingTop: getSize(10),
    paddingBottom: getSize(10),
    backgroundColor: THEME.TEXT_COLOR_LIGHT,
    borderRadius: getSize(30),

    shadowColor: THEME.TEXT_COLOR_DARK,
    shadowOffset: {
      width: 0,
      height: 6,
    },
    shadowOpacity: 0.27,
    shadowRadius: 7.49,
    elevation: 12,
  },
  shareButtonText: {
    textAlign: 'center',
    fontSize: getSize(14),
    color: THEME.MAIN_COLOR,
    textTransform: 'uppercase',
  },
  shareButtonWrapper: {
    paddingTop: getSize(20),
    paddingBottom: getSize(20),
  },
});