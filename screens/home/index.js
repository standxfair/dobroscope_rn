import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux'
import * as firebase from 'firebase';
import { ScrollView } from 'react-native';
import { useScrollToTop } from '@react-navigation/native';
import HelloText from './HelloText'
import Share from './Share'
import TodayList from './TodayList'
import Achievements from './Achievements'
import { setUserStatus } from '../../actions/userActions'
import { WellcomeModal } from '../../components/Modals/WellcomeModal';

const Home = ({ navigation }) => {
  const [showWellcomeModal, setShowWellcomeModal] = useState(false)
  const isNewUser = useSelector(state => state.user.isNewUser)
  const dispatch = useDispatch()

  const ref = React.useRef(null);
  useScrollToTop(ref);

  useEffect(() => {
    const userFirebase = firebase.auth().currentUser;
    const { displayName } = userFirebase
    if (!displayName || displayName === " ") {
      setShowWellcomeModal(true)
    }
  }, [])

  useEffect(() => {
    if (isNewUser) {
      dispatch(setUserStatus(false))
    }
  }, [isNewUser])

  return (
    <ScrollView ref={ref}>
      <HelloText />
      <Share navigation={navigation} />
      <TodayList navigation={navigation} />
      <Achievements navigation={navigation} />

      <WellcomeModal modalVisible={showWellcomeModal} setShowWellcomeModal={setShowWellcomeModal} />
    </ScrollView>
  )
}

export default Home;