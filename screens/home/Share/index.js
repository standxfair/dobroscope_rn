import React, { useRef } from 'react';
import { useSelector } from 'react-redux'
import { StyleSheet, View, Image, Dimensions } from 'react-native';
import { AppButton } from '../../../components/AppButton';
import { THEME, getSize } from '../../../styles/theme';
import { AppText } from '../../../components/ui/AppText'
import { AppTextBlack } from '../../../components/ui/AppTextBlack'
import { fixNumSuffix } from '../../../helpers/fixNumSuffix'

const ShareResult = ({ navigation }) => {
  const count = useSelector(state => state.todos.totalCounter)
  const viewRef = useRef();

  if (count === 0) {
    return null
  }

  const makeStringLength = (count) => {
    let coef = 0
    let result = 'о'
    if (count % 10 === 0) {
      coef = count / 10

      for (let i = 1;i <= coef;i++) {
        result += 'о'
      }
    }

    return result
  }

  return (
    <View style={styles.container}>
      <View style={styles.wrapperWithImage} ref={viewRef} collapsable={false}>
        <View style={styles.wrapper}>
          <View>
            <AppText style={styles.description}>Ты сделал мир лучше</AppText>
          </View>
          <View>
            <AppText style={styles.countText}>
              <AppTextBlack style={styles.count}>{count}</AppTextBlack> {fixNumSuffix(count, ['раз', 'раза'])}
            </AppText>
          </View>
          <View>
            <AppText style={styles.descriptionSecond}>
              Это {makeStringLength(count)}чень много!
            </AppText>
          </View>
        </View>
        <Image
          style={styles.image}
          source={require('../../../assets/big_logo.png')}
        />
      </View>

      <View style={styles.descriptionRow}>
        <AppText style={styles.descriptionRowMarker}>—</AppText>
        <AppText style={styles.descriptionRowText}>Кстати, ты можешь рассказать об этом в соцсетях</AppText>
      </View>

      <AppButton onPress={() => { navigation.navigate('ShareCounter') }} />
    </View>
  )
};

export default ShareResult


const styles = StyleSheet.create({
  container: {
    paddingHorizontal: getSize(20),
    paddingTop: getSize(50),
    backgroundColor: THEME.TEXT_COLOR_LIGHT,
  },

  count: {
    fontSize: getSize(113),
    color: THEME.MAIN_COLOR,
    lineHeight: getSize(105),
  },
  countText: {
    color: THEME.MAIN_COLOR,
    fontSize: getSize(12),
  },
  description: {
    fontSize: getSize(17),
  },
  descriptionSecond: {
    fontSize: getSize(17),
    paddingBottom: getSize(20),
    marginTop: getSize(-15),
  },
  descriptionRow: {
    flexDirection: 'row',
    paddingBottom: getSize(10),
    width: Dimensions.get('window').width / 1.3,
  },
  descriptionRowMarker: {
    flex: 1,
    textAlign: 'right',
    paddingRight: getSize(5),
  },
  descriptionRowText: {
    flex: 2,
    justifyContent: 'flex-end',
  },

  wrapperWithImage: {
    flexDirection: 'row',
    height: getSize(180),
  },
  wrapper: {
    backgroundColor: THEME.BLUE_LIGHT,
    borderRadius: getSize(100),
    flex: 2,
    zIndex: 1,
    position: 'absolute',
    left: getSize(-80),
    top: 0,
    paddingLeft: getSize(80),
    paddingRight: getSize(40),
    paddingTop: getSize(15),
  },
  image: {
    flex: 1,
    zIndex: 2,
    position: 'absolute',
    top: getSize(-90),
    right: getSize(-200),
    height: getSize(332),
    width: getSize(306),
  }
});