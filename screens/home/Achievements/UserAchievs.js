import React, { useState, useEffect } from 'react';
import { useSelector } from 'react-redux'
import { AppText } from '../../../components/ui/AppText'
import { AppTextBold } from '../../../components/ui/AppTextBold'
import { StyleSheet, ScrollView, View } from 'react-native';
import { THEME, getSize } from '../../../styles/theme';
import { SvgUri } from 'react-native-svg';
import { Dimensions } from 'react-native';


const opacity = {
  HALF: 0.5,
  FULL: 1
}

const UserAchievs = () => {
  const [currentAchievs, setCurrentAchievs] = useState([])
  const dict = useSelector(state => state.achievements.dict)
  const achievements = useSelector(state => state.achievements.achievements)

  useEffect(() => {
    if (achievements && dict) {
      setCurrentAchievs(mapAchievsToUser(achievements, dict))
    }
  }, [dict, achievements])

  const mapAchievsToUser = (userAchievs, dict) => {
    let result = []
    dict.forEach(item => {
      const item_ = { ...item }
      item_.isActive = userAchievs.includes(item.id)
      result.push(item_)
    })
    return result.sort(item => {
      if (item.isActive) {
        return -1
      } else {
        return 1
      }
    })
  }
  // Dimensions.get('window').width <= 320

  return <ScrollView style={styles.scrollContainer}>
    <View style={styles.container}>
      <View>
        <AppTextBold style={styles.header}>
          Твои достижения
        </AppTextBold>
      </View>
      <View style={styles.wrap}>
        {currentAchievs.map((item) => <View style={styles.listItem} key={item.id}>
          <View>
            <SvgUri style={{ ...styles.itmImg, opacity: item.isActive ? opacity.FULL : opacity.HALF }} uri={item.image} />
          </View>
          <AppText style={{ ...styles.listIitemTitle, opacity: item.isActive ? opacity.FULL : opacity.HALF }}>{item.title}</AppText>
        </View>)}
      </View>
    </View>
  </ScrollView>
}


const styles = StyleSheet.create({
  scrollContainer: {
    backgroundColor: THEME.TEXT_COLOR_LIGHT,
  },
  container: {
    paddingHorizontal: getSize(20),
    paddingBottom: getSize(60),
    backgroundColor: THEME.TEXT_COLOR_LIGHT,
    paddingTop: getSize(20),
    flex: 1,
  },
  header: {
    fontSize: THEME.FONT_SIZES.h1,
    paddingBottom: getSize(20),
  },
  wrap: {
    flexDirection: 'row',
    alignItems: 'flex-start',
    flex: 1,
    flexWrap: 'wrap',
    justifyContent: 'center',
  },
  listItem: {
    margin: getSize(20),
    width: getSize(100),
    justifyContent: 'center',
    alignItems: 'center',
  },
  listIitemTitle: {
    textAlign: 'center',
    opacity: opacity.HALF,
    paddingTop: getSize(5),
  },
  itmImg: {
    width: getSize(90),
    height: getSize(90),
    opacity: opacity.HALF,
  }
})

export default UserAchievs