import React from 'react';
import { StyleSheet, View } from 'react-native';
import { getSize } from '../../../styles/theme';
import { SvgUri } from 'react-native-svg';
import { AppText } from '../../../components/ui/AppText'

const Preview = ({ userAchievs, dict }) => {
  return <View style={style.ahivList}>
    {userAchievs.map((id, key) => {
      const element = dict.find((item) => item.id === id)

      if (key > 2 || !element) return

      return <View
        style={style.ahivListIcon}
        key={element.id}>
        <SvgUri style={style.ahivListIcon} uri={element.image} />
        <AppText style={style.descr}>{element.title}</AppText>
      </View>
    })}
  </View>
}

const style = StyleSheet.create({
  ahivList: {
    flexDirection: 'row',
    paddingVertical: getSize(20),
    alignItems: 'center',
  },
  ahivListIcon: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  ahivListIcon2: {
    flex: 1,
  },
  ahivListIcon3: {
    flex: 1,
  },
  descr: {
    paddingTop: getSize(10),
    textAlign: 'center'
  }
})

export default Preview