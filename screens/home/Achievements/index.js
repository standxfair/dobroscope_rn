import React from 'react';
import { useSelector } from 'react-redux'
import { StyleSheet, Dimensions, View, Image, TouchableHighlight } from 'react-native';
import { THEME, getSize } from '../../../styles/theme';
import { AppTextBold } from '../../../components/ui/AppTextBold'
import { AppText } from '../../../components/ui/AppText'
import Preview from './Preview'


const Achievements = ({ navigation }) => {
  const achievements = useSelector(state => state.achievements.achievements)
  const dict = useSelector(state => state.achievements.dict)

  if (!achievements || !dict) return null

  let buttonText = 'Посмотреть доступные достижения'
  if (achievements.length > 2) {
    buttonText = 'Больше твоих достижений'
  }
  return (
    <View style={style.block}>
      <AppTextBold style={style.header}>
        Твои достижения
      </AppTextBold>
      {achievements.length ? <Preview userAchievs={achievements}
        dict={dict} /> : null}

      <View style={style.shareButtonWrapperNoPaddingTop}>
        <TouchableHighlight
          style={style.shareButtonWhy}
          onPress={() => navigation.navigate('UserAchievs')}
        >
          <AppText style={achievements.length ? style.shareButtonTextWhy : { ...style.shareButtonTextWhy, paddingTop: getSize(10) }}>
            {buttonText}
          </AppText>
        </TouchableHighlight>
      </View>
    </View>
  )
};

export default Achievements

const style = StyleSheet.create({
  block: {
    paddingVertical: getSize(40),
    paddingHorizontal: getSize(20),
    backgroundColor: THEME.TEXT_COLOR_LIGHT,
  },
  header: {
    fontSize: THEME.FONT_SIZES.leadHeader,
    width: Dimensions.get('window').width / 1.5,
  },
  descr: {
    paddingTop: getSize(10),
    textAlign: 'center'
  },
  ahivList: {
    flexDirection: 'row',
    paddingVertical: getSize(20),
    alignItems: 'center',
  },
  ahivListIcon: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  ahivListIcon2: {
    flex: 1,
  },
  ahivListIcon3: {
    flex: 1,
  },

  shareButtonWrapperNoPaddingTop: {
    paddingBottom: 0,
  },
  shareButtonTextWhy: {
    fontSize: getSize(14),
    textDecorationColor: THEME.TEXT_COLOR_DARK,
    textDecorationStyle: 'solid',
    color: THEME.TEXT_COLOR_DARK,
    textDecorationLine: 'underline',
    paddingBottom: getSize(60),
  }
})