import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux'
import moment from 'moment'
import { useScrollToTop } from '@react-navigation/native';
import { StyleSheet, View, Image, TouchableOpacity, ImageBackground, FlatList } from 'react-native';
import { AppTextLight } from '../../components/ui/AppTextLight'
import { AppTextBold } from '../../components/ui/AppTextBold'
import { THEME, getSize } from '../../styles/theme';
import { setTodayMotivation } from '../../actions/motivationActions'
import { getSortedMotivation, getCurrentDayMotivation } from '../../selectors/motivation'
import { DB_DATE_FORMAT } from '../../constants/app';

function usePrevious(value) {
  const ref = React.useRef();
  useEffect(() => {
    ref.current = value;
  });
  return ref.current;
}

export const Motivation = ({ navigation }) => {
  const data = useSelector(state => getSortedMotivation(state))
  const todayItem = useSelector(state => getCurrentDayMotivation(state))
  const loading = useSelector(state => state.motivation.loading)


  const dispatch = useDispatch()

  const ref = React.useRef(null);
  useScrollToTop(ref);

  const prevProps = usePrevious({ loading });
  useEffect(() => {
    if (prevProps?.loading && !loading) {
      if (!todayItem) dispatch(setTodayMotivation())
    }
  }, [loading])

  useEffect(() => {
    dispatch(setTodayMotivation())
  }, [dispatch])

  const renderItem = ({ item }) => {
    return <Item
      title={item.item.title}
      date={item.date}
      author={item.item.author}
      type={item.item.type}
    />
  }

  if (!data.length && !todayItem) {
    return (
      <View style={styles.container}>
        <Header data={[]} navigation={navigation} />
      </View>
    )
  }

  return (
    <FlatList
      ref={ref}
      style={styles.container}
      contentContainerStyle={{
        paddingBottom: getSize(80),
      }}
      ListHeaderComponent={
        <>
          <Header
            data={todayItem || []}
            navigation={navigation}
          />
          {data.length ? (
            <AppTextBold style={styles.listTitle}>
              Показаны раньше
            </AppTextBold>
          ) : null}
        </>
      }
      data={data.length ? data : []}
      renderItem={renderItem}
      keyExtractor={(item, index) => index.toString()}
    />
  )
}

const Item = ({ title, date, author, type }) => {
  return (
    <View style={styles.item}>
      <View style={styles.itemHeader}>
        <View style={styles.itemHeaderTextWrapper}>
          <AppTextLight style={styles.itemHeaderText}>
            {type}
          </AppTextLight>
        </View>
        <View style={styles.itemHeaderDateWrapper}>
          <AppTextLight style={styles.itemHeaderDate}>
            {moment(date, DB_DATE_FORMAT).format('DD MMMM')}
          </AppTextLight>
        </View>
      </View>
      <View>
        <AppTextBold style={styles.title}>{title}</AppTextBold>
      </View>
      {author
        ? <View style={styles.itemAuthorDiv}>
          <AppTextLight style={styles.itemAuthor}>{author}</AppTextLight>
        </View>
        : null}
    </View>
  )
}

export const Header = ({ data, navigation }) => {
  if (!data) {
    return null
  }

  return (
    <ImageBackground
      style={styles.quoteOfTheDay}
      source={require('../../assets/motiv_bg.png')}
    >
      <View style={styles.quoteOfTheDayHeader}>
        <View style={styles.quoteOfTheDayHeaderTitle}>
          {!data.type ? (
            <AppTextLight style={styles.quoteOfTheDayHeaderTitleText}>
            </AppTextLight>
          ) : (
            <AppTextLight style={styles.quoteOfTheDayHeaderTitleText}>
              {data.type === 'Цитата' ? 'Цитата дня' : 'Мотивация дня'}
            </AppTextLight>
          )}

        </View>
        <View style={styles.quoteOfTheDayHeaderDate}>
          <AppTextLight style={styles.quoteOfTheDayHeaderDateText}>
            {moment().format('DD MMMM')}
          </AppTextLight>
        </View>
      </View>

      <View style={styles.quoteOfTheDayTitle}>
        {!data.title ? (
          <AppTextBold style={styles.quoteOfTheDayTitleText}>
          </AppTextBold>
        ) : (
          <AppTextBold style={styles.quoteOfTheDayTitleText}>
            {data.title}
          </AppTextBold>
        )}

      </View>

      <View style={styles.quoteOfTheDayFooter}>
        <View style={styles.quoteOfTheDayFooterTitle}>
          <TouchableOpacity
            style={styles.share}
            onPress={() => {
              navigation.navigate('ShareMotivationCard', { data })
            }}
          >
            <Image source={require('../../assets/share.png')} />
            <AppTextLight style={styles.quoteOfTheDayFooterTitleText}>
              Поделиться</AppTextLight>
          </TouchableOpacity>
        </View>
        <View style={styles.quoteOfTheDayFooterAuthor}>
          {data.author ?
            <AppTextLight style={styles.quoteOfTheDayFooterAuthorText}>
              {data.author}
            </AppTextLight> : null}
        </View>
      </View>
    </ImageBackground>
  )
}

const styles = StyleSheet.create({
  container: {
    paddingTop: getSize(20),
    paddingHorizontal: getSize(20),
    flex: 1,
    backgroundColor: THEME.TEXT_COLOR_LIGHT,
  },

  quoteOfTheDay: {
    backgroundColor: THEME.MAIN_COLOR,
    marginBottom: getSize(30),
    padding: getSize(25),
    borderRadius: getSize(20),
  },
  quoteOfTheDayHeaderTitle: {
    flex: 2,
    color: THEME.TEXT_COLOR_LIGHT,
  },
  quoteOfTheDayHeaderTitleText: {
    fontSize: getSize(12),
    color: THEME.TEXT_COLOR_LIGHT,
  },
  quoteOfTheDayHeader: {
    flexDirection: 'row',
    paddingBottom: getSize(12),
  },
  quoteOfTheDayHeaderDate: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    textAlign: 'right',
  },
  quoteOfTheDayHeaderDateText: {
    textTransform: 'uppercase',
    color: THEME.TEXT_COLOR_LIGHT,
  },
  quoteOfTheDayTitle: {
    paddingBottom: getSize(50),
  },
  quoteOfTheDayTitleText: {
    fontSize: getSize(18),
    color: THEME.TEXT_COLOR_LIGHT,
  },
  quoteOfTheDayFooter: {
    flexDirection: 'row',
    // position: 'absolute',
    bottom: getSize(20),
    // right: 0,
    // left: 0,
    // paddingLeft: getSize(25),
    // paddingRight: getSize(25),
  },
  quoteOfTheDayFooterTitle: {
    fontSize: getSize(12),
    flex: 2,
  },
  quoteOfTheDayFooterAuthor: {
    flex: 2,
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    textAlign: 'right',
  },
  quoteOfTheDayFooterTitleText: {
    fontSize: getSize(12),
    color: THEME.TEXT_COLOR_LIGHT,
    paddingLeft: getSize(5),
  },
  quoteOfTheDayFooterAuthorText: {
    fontSize: getSize(12),
    color: THEME.TEXT_COLOR_LIGHT,
  },
  share: {
    flexDirection: 'row',
    alignItems: 'center',
  },

  listWrapper: {
  },
  listTitle: {
    fontSize: getSize(18),
  },
  item: {
    paddingVertical: getSize(15),
    borderBottomColor: THEME.MAIN_COLOR_LIGHT,
    borderBottomWidth: getSize(1),
    borderStyle: 'solid',
  },
  itemHeader: {
    flexDirection: 'row',
    paddingBottom: getSize(15),
  },
  itemHeaderTextWrapper: {
    flex: 2,
  },
  itemHeaderDateWrapper: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    textAlign: 'right',
  },
  itemHeaderText: {
    fontSize: THEME.FONT_SIZES.comment,
  },
  itemHeaderDate: {
    fontSize: THEME.FONT_SIZES.comment,
    textTransform: 'uppercase',
  },
  title: {
    fontSize: THEME.FONT_SIZES.h2,
  },
  itemAuthorDiv: {
    paddingTop: getSize(10)
  },
  itemAuthor: {
    fontSize: THEME.FONT_SIZES.comment,
    textAlign: 'right',
  },
})