import React, { useState, useEffect } from 'react';
import { StyleSheet, View, TextInput, Switch, TouchableOpacity } from 'react-native';
import { useSelector, useDispatch } from 'react-redux'
import { AppTextBold } from '../../components/ui/AppTextBold'
import { AppTextMedium } from '../../components/ui/AppTextMedium'
import { AppText } from '../../components/ui/AppText'
import { THEME, getSize, INPUT_ERROR } from '../../styles/theme';
import { setUserData } from '../../actions/userActions'
import * as firebase from 'firebase';
import { setEulaAccept, updateUser } from '../../helpers/user'
import { validateHandler } from '../../helpers/validateHandler'
import { USER_AUTH_SCHEMA } from '../../constants/validation'
import { AppButton } from '../../components/AppButton';
import { EulaTextModal } from '../../components/Modals/EulaTextModal'


export const ConfirmUser = ({ navigation, setShowConfirmUser }) => {
  const isEulaAccepted = useSelector(state => state.user.isEulaAccepted)
  const user = useSelector(state => state.user)
  const isAnonymous = useSelector(state => state.user.isAnonymous)
  const [email, setEmail] = useState(user.email)
  const [password, setPassword] = useState('')
  const [errors, setErrors] = useState({})
  const [needConfirm, setNeedConfirm] = useState({})
  const [showEulaModal, setShowEulaModal] = useState(false)

  const dispatch = useDispatch()

  useEffect(() => {
    const result = {}
    if (!isEulaAccepted) {
      result.eulaAccept = true
    }
    if (isAnonymous) {
      result.email = true
    }
    setNeedConfirm(result)
  }, [])



  const acceptEula = async () => {
    const accept = !isEulaAccepted
    await setEulaAccept({ isEulaAccepted: accept, userId: user.uid })
    dispatch(setUserData({ ...user, isEulaAccepted: accept }))
  }

  const saveUserData = (type) => async (data) => {
    validateHandler(type, data, USER_AUTH_SCHEMA, errors, setErrors)
    if (type === 'email') {
      setEmail(data)
    } else {
      setPassword(data)
    }
  }

  const registerUser = async () => {
    const valid = validateHandler('email', email, USER_AUTH_SCHEMA, errors, setErrors) && validateHandler('password', password, USER_AUTH_SCHEMA, errors, setErrors) && isEulaAccepted

    if (valid) {
      const credential = firebase.auth.EmailAuthProvider.credential(email, password);
      const usercred = await firebase.auth().currentUser.linkWithCredential(credential)
      if (usercred) {
        await updateUser({ userId: user.uid, data: usercred.user })
        await firebase.auth().currentUser.sendEmailVerification()

        dispatch(setUserData({ ...user, ...usercred.user }))
        navigation.navigate('Home')
      }
    }
  }

  const closeEulaModal = () => {
    setShowEulaModal(false)
  }

  return <View style={styles.container}>
    <View style={styles.pageTitileWrapper}>
      <AppTextBold style={styles.pageTitle}>
        Осталось совсем чуть-чуть! Чтобы делиться своими добрыми делами необходимо закончить регистрацию в приложении и повтерить email
      </AppTextBold>
    </View>
    <View>
      <View style={styles.switchContainer}>
        <Switch
          trackColor={{ false: THEME.MAIN_COLOR_LIGHT, true: THEME.MAIN_COLOR_LIGHT }}
          thumbColor={THEME.MAIN_COLOR}
          ios_backgroundColor={THEME.MAIN_COLOR_LIGHT}
          onValueChange={acceptEula}
          value={isEulaAccepted}
        />
        <AppText style={styles.switchContainerLabel}>
          Я принимаю условия <TouchableOpacity onPress={() => {
            setShowEulaModal(true)
          }}>
            <AppText style={styles.switchContainerLabelLink}>
              пользовательского соглашения
            </AppText>
          </TouchableOpacity>
        </AppText>
      </View>
    </View>
    {needConfirm.email && <View>
      <AppText style={styles.emailHeader}>
        Пожалуйста, завершите регистрацию
      </AppText>
      <View style={styles.formWrapper}>
        <View style={styles.formInputWrapper}>
          <AppTextMedium style={styles.formLabel}>
            Электронная почта
          </AppTextMedium>
        </View>
        <TextInput
          style={!errors.email ? styles.formInput : { ...styles.formInput, ...INPUT_ERROR }}
          value={email}
          onChangeText={text => saveUserData('email')(text)}
          autoCorrect={false}
          autoCapitalize='none'
          placeholder='Ваш email'
        />
        <View style={styles.formInputWrapper}>
          <AppTextMedium style={styles.formLabel}>
            Пароль
          </AppTextMedium>
        </View>
        <TextInput
          style={!errors.email ? styles.formInput : { ...styles.formInput, ...INPUT_ERROR }}
          value={password}
          onChangeText={text => saveUserData('password')(text)}
          autoCorrect={false}
          autoCapitalize='none'
          secureTextEntry={true}
          placeholder='Придумайте пароль'
        />
      </View>
      <View>
        <AppButton
          title='Закончить регистрацию'
          onPress={registerUser}
          style={{
            shareButtonWrapper: {
              paddingTop: getSize(28),
              paddingBottom: getSize(60),
              zIndex: 10,
            },
            shareButton: {
              paddingHorizontal: getSize(40),
              paddingTop: getSize(10),
              paddingBottom: getSize(10),
              backgroundColor: isEulaAccepted ? THEME.MAIN_COLOR : THEME.MAIN_COLOR_LIGHT,
              borderRadius: getSize(30),
              shadowColor: THEME.MAIN_COLOR,
              shadowOffset: {
                width: 0,
                height: 6,
              },
              shadowOpacity: (0.37),
              shadowRadius: (7.49),
              elevation: (12),
            },
            shareButtonText: {
              textAlign: 'center',
              fontSize: getSize(14),
              color: THEME.TEXT_COLOR_LIGHT,
              textTransform: 'uppercase',
            },
          }}
        />
      </View>
    </View>}
    <EulaTextModal modalVisible={showEulaModal} onClose={closeEulaModal} />
  </View>
}


const styles = StyleSheet.create({
  loading: {
    flex: 1,
    justifyContent: "center",
    backgroundColor: THEME.TEXT_COLOR_LIGHT,
  },
  container: {
    paddingTop: getSize(20),
    paddingHorizontal: getSize(20),
    paddingBottom: getSize(60),
    flex: 1,
    backgroundColor: THEME.TEXT_COLOR_LIGHT,
  },
  pageTitileWrapper: {
    flexDirection: 'row',
    borderStyle: 'solid',
    alignItems: 'center',
    paddingBottom: getSize(20),
  },
  pageTitle: {
    fontSize: THEME.FONT_SIZES.h1,
    flex: 3,
  },
  plusButton: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
  },
  itemFooter: {
    flexDirection: 'row',
  },
  like: {
    flex: 3,
    flexDirection: 'row',
    alignItems: 'center'
  },
  likesCount: {
    fontSize: getSize(12),
    color: THEME.MAIN_COLOR,
    paddingLeft: getSize(5),
  },
  leaveCommentButton: {
    flex: 1,
    color: THEME.MAIN_COLOR,
    fontSize: getSize(12),
    textDecorationStyle: 'solid',
    textDecorationColor: THEME.MAIN_COLOR,
    textDecorationLine: 'underline',
  },

  item: {
    paddingVertical: getSize(15),
    borderBottomColor: THEME.MAIN_COLOR_LIGHT,
    borderBottomWidth: getSize(1),
    borderStyle: 'solid',
  },
  itemHeader: {
    flexDirection: 'row',
    paddingBottom: getSize(15),
    alignItems: 'center',
  },
  itemHeaderTextWrapper: {
    paddingLeft: getSize(15),
  },
  itemHeaderDateWrapper: {

  },
  itemHeaderText: {
    fontSize: THEME.FONT_SIZES.h2,
    paddingBottom: getSize(5),
  },
  itemHeaderDate: {
    fontSize: THEME.FONT_SIZES.comment,
    textTransform: 'uppercase',
  },
  text: {
    paddingBottom: getSize(20),
    fontSize: THEME.FONT_SIZES.text,
  },
  avatar: {
    width: getSize(40),
    height: getSize(40),
    borderRadius: getSize(80 / 2),
  },
  postImage: {
    marginBottom: getSize(15),
    borderRadius: getSize(20),
    width: '100%',
    height: getSize(195),
    resizeMode: 'cover',
  },
  comment: {
    paddingVertical: getSize(15),
    marginHorizontal: getSize(25),
    borderBottomColor: THEME.MAIN_COLOR_LIGHT,
    borderBottomWidth: getSize(1),
    borderStyle: 'solid',
  },
  commentText: {
    fontSize: getSize(12)
  },
  commentLast: {
    marginHorizontal: getSize(25),
    paddingVertical: getSize(15),
  },
  commentArrow: {
    position: 'absolute',
    top: getSize(15),
    left: getSize(-23),
  },
  containerLoader: {
    position: "absolute",
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    justifyContent: "center",
    alignItems: "center",
  },
  likeButton: {
    padding: getSize(4),
  },

  checkbox: {
    marginRight: getSize(5),
    marginTop: getSize(-10),
  },
  checkboxWrapper: {
    borderColor: THEME.MAIN_COLOR,
    borderWidth: getSize(0.4),
    borderStyle: 'solid',
    width: getSize(20),
    height: getSize(20),
    borderRadius: getSize(50),
  },
  checkboxWrapperChecked: {
    backgroundColor: THEME.MAIN_COLOR,
  },
  todo: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingTop: getSize(10),
    paddingRight: getSize(10),
    paddingBottom: getSize(10),
  },
  textCheckbox: {
    paddingLeft: getSize(5),
  },
  switchContainer: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    marginBottom: getSize(20),
  },
  switchContainerLabel: {
    fontSize: getSize(16),
    paddingLeft: Platform.OS === 'ios' ? getSize(10) : 0,
  },
  switchContainerLabelLink: {
    fontSize: getSize(16),
    paddingLeft: Platform.OS === 'ios' ? getSize(10) : 0,
    textDecorationLine: 'underline'
  },
  emailHeader: {
    fontSize: getSize(16),
    paddingTop: getSize(10)
  },
  formWrapper: {
    paddingTop: getSize(20),
  },
  formLabel: {
    color: THEME.MAIN_COLOR_LIGHT,
    fontSize: getSize(12),
    textTransform: 'uppercase',
  },
  formInput: {
    borderBottomColor: THEME.TEXT_COLOR_DARK,
    borderBottomWidth: getSize(1),
    borderStyle: 'solid',
    marginBottom: getSize(20),
    color: THEME.TEXT_COLOR_DARK,
    fontSize: getSize(16),
    paddingTop: getSize(10),
    fontFamily: THEME.MEDIUM
  }
})