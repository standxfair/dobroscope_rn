import React, { useState, useEffect } from 'react';
import { StyleSheet, View, Image, TouchableOpacity, FlatList, ActivityIndicator } from 'react-native';
import { useSelector, useDispatch } from 'react-redux'
import { AntDesign } from '@expo/vector-icons';
import * as firebase from 'firebase';
import { useScrollToTop } from '@react-navigation/native';
import { MaterialIcons } from '@expo/vector-icons';
import { AppTextLight } from '../../components/ui/AppTextLight'
import { AppTextBold } from '../../components/ui/AppTextBold'
import { AppTextMedium } from '../../components/ui/AppTextMedium'
import { AppText } from '../../components/ui/AppText'
import { THEME, getSize } from '../../styles/theme';
import { fetchFeed, likePost } from '../../actions/feedActions'
import { AddNewFeedItem } from '../../components/Modals/AddNewFeedItem'
import { AddComment } from '../../components/Modals/AddComment'
import { getSortedFeed } from '../../selectors/feed'
import { mapFeedItems } from '../../helpers/feed'
import { ConfirmUser } from './ConfirmUser'


export const Feed = ({ navigation }) => {
  const [showModal, setShowModal] = useState(false)
  const [showModalComment, setShowModalComment] = useState(false)
  const [commentedItemId, setCommentedItemId] = useState(null)
  const [refreshing, setRefreshing] = useState(false);
  const [showConfirmUser, setShowConfirmUser] = useState(false);

  const dispatch = useDispatch()
  const feedItems = useSelector(state => getSortedFeed(state))
  const userId = useSelector(state => state.user.uid)
  const isAnonymous = useSelector(state => state.user.isAnonymous)
  const loading = useSelector(state => state.feed.loading)

  const fetch = (feed) => dispatch(fetchFeed(feed))

  useEffect(() => {
    const unsubscribe = firebase
      .database()
      .ref('feed')
      .on('value', async (snapshot) => {
        const data = snapshot?.val();
        if (data) {
          const feed = await mapFeedItems(data, userId)
          fetch(feed)
        } else {
          fetch([])
        }
      });

    return () => unsubscribe()
  }, [])

  useEffect(() => {
    if (!isAnonymous && showConfirmUser) {
      setShowConfirmUser(false)
    }
  }, [isAnonymous])

  const ref = React.useRef(null);
  useScrollToTop(ref);

  const likePostHandler = (id) => () => {
    dispatch(likePost(id))
  }

  const renderItem = ({ item }) => {
    return (
      <Item
        text={item.text}
        image={item.imgUrl}
        likesCount={!!item.likesCount ? item.likesCount : 0}
        isUserLiked={item.isUserLiked}
        comments={item.comments}
        date={item.createDate}
        author={item.author}
        avatar={item.avatar}
        id={item.id}
        setCommentedItemId={setCommentedItemId}
        setShowModalComment={setShowModalComment}
        likePostHandler={likePostHandler}
        isAnonymous={isAnonymous}
      />
    )
  }

  const closeModal = () => {
    setShowModal(false)
  }

  const closeModalComment = () => {
    setShowModalComment(null)
  }

  const addPostHandler = () => {
    if (isAnonymous) {
      setShowConfirmUser(true)
    } else {
      setShowModal(true)
    }
  }

  if (loading) {
    return (
      <View style={styles.loading}>
        <ActivityIndicator size="large" color={THEME.MAIN_COLOR} />
      </View>
    )
  }
  else if (showConfirmUser) {
    return <ConfirmUser navigation={navigation} setShowConfirmUser={setShowConfirmUser} />
  }
  else {
    return (
      <>
        <FlatList
          ref={ref}
          style={styles.container}
          refreshing={refreshing}
          ListHeaderComponent={<>
            <View style={styles.pageTitileWrapper}>
              <AppTextBold style={styles.pageTitle}>
                Расскажите о ваших добрых делах
              </AppTextBold>
              <TouchableOpacity
                style={styles.plusButton}
                onPress={addPostHandler}>
                <AntDesign name="pluscircle" size={43} color={THEME.MAIN_COLOR} />
              </TouchableOpacity>
            </View>
          </>}
          contentContainerStyle={{
            paddingBottom: getSize(100),
          }}
          data={feedItems || []}
          renderItem={renderItem}
          keyExtractor={item => item.id}
        />

        <AddNewFeedItem modalVisible={showModal} onClose={closeModal} />

        <AddComment
          modalVisible={showModalComment}
          onClose={closeModalComment}
          commentedItemId={commentedItemId}
        />
      </>
    )
  }


}

const Item = ({ text, image, likesCount, comments, date, author, id, setCommentedItemId, setShowModalComment, likePostHandler, isUserLiked, avatar, isAnonymous }) => {
  const [loader, setLoader] = useState(false)
  const renderItemComment = (data) => ({ item, index }) => (
    <ItemComment text={item.text} author={item.author} index={index} data={data} />
  )

  const loaderHandler = (value) => () => {
    setLoader(value)
  }

  return (
    <View style={styles.item}>
      <View style={styles.itemHeader}>
        {avatar ? (
          <Image
            style={styles.avatar}
            source={{ uri: avatar }}
          />
        ) : (
          <View
            style={styles.avatarMock}>
            <AntDesign
              style={styles.userMock}
              name="user"
              size={32}
              color={THEME.MAIN_COLOR}
            />
          </View>
        )}
        <View style={styles.itemHeaderTextWrapper}>
          <AppTextBold style={styles.itemHeaderText}>
            {author}
          </AppTextBold>

          <View style={styles.itemHeaderDateWrapper}>
            <AppTextLight style={styles.itemHeaderDate}>
              {date}
            </AppTextLight>
          </View>
        </View>
      </View>

      <View>
        {image ? <Image
          source={{ uri: image }}
          style={styles.postImage}
          onLoadStart={loaderHandler(true)}
          onLoadEnd={loaderHandler(false)}
        />
          : null}
        {loader &&
          <View style={styles.containerLoader}>
            <ActivityIndicator size="small" color={THEME.MAIN_COLOR} />
          </View>}
        <AppText style={styles.text}>{text}</AppText>
      </View>

      <View style={styles.itemFooter}>
        <View style={styles.like}>

          {!!isUserLiked ? (
            <TouchableOpacity
              style={styles.likeButton}
              onPress={likePostHandler(id)}
            >
              <AntDesign name="heart" size={14} color={THEME.MAIN_COLOR} />
            </TouchableOpacity>
          ) : (
            <TouchableOpacity
              style={styles.likeButton}
              onPress={likePostHandler(id)}
            >
              <AntDesign name="hearto" size={14} color={THEME.MAIN_COLOR} />
            </TouchableOpacity>
          )}

          {!!likesCount ? <AppTextLight style={styles.likesCount}>{likesCount}</AppTextLight> : null}
        </View>
        {!isAnonymous && <TouchableOpacity onPress={() => {
          setShowModalComment(true)
          setCommentedItemId(id)
        }}>
          <AppTextLight style={styles.leaveCommentButton}>
            Оставить комментарий
          </AppTextLight>
        </TouchableOpacity>}

      </View>

      {!!comments && comments.length ? (
        <View style={styles.comments}>
          <View>
            <FlatList
              data={comments}
              renderItem={renderItemComment(comments)}
              keyExtractor={item => item.id}
            />
          </View>
        </View>
      ) : null}
    </View>
  )
}

const ItemComment = ({ text, author, index, data }) => (
  <View style={index === data.length - 1 ? styles.commentLast : styles.comment}>
    {index === 0 ? (
      <MaterialIcons style={styles.commentArrow} name="subdirectory-arrow-right" size={24} color="#7F7F7F" />
    ) : null}
    <AppTextMedium style={styles.commentText}>{author}</AppTextMedium>
    <AppTextMedium style={styles.commentText}>{text}</AppTextMedium>
  </View>
)


const styles = StyleSheet.create({
  loading: {
    flex: 1,
    justifyContent: "center",
    backgroundColor: THEME.TEXT_COLOR_LIGHT,
  },
  container: {
    paddingTop: getSize(20),
    paddingHorizontal: getSize(20),
    paddingBottom: getSize(60),
    flex: 1,
    backgroundColor: THEME.TEXT_COLOR_LIGHT,
  },
  pageTitileWrapper: {
    flexDirection: 'row',
    borderBottomColor: THEME.MAIN_COLOR_LIGHT,
    borderBottomWidth: getSize(1),
    borderStyle: 'solid',
    alignItems: 'center',
    paddingBottom: getSize(20),
  },
  pageTitle: {
    fontSize: THEME.FONT_SIZES.h1,
    flex: 3,
  },
  plusButton: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
  },
  itemFooter: {
    flexDirection: 'row',
  },
  like: {
    flex: 3,
    flexDirection: 'row',
    alignItems: 'center'
  },
  likesCount: {
    fontSize: getSize(12),
    color: THEME.MAIN_COLOR,
    paddingLeft: getSize(5),
  },
  leaveCommentButton: {
    flex: 1,
    color: THEME.MAIN_COLOR,
    fontSize: getSize(12),
    textDecorationStyle: 'solid',
    textDecorationColor: THEME.MAIN_COLOR,
    textDecorationLine: 'underline',
  },

  item: {
    paddingVertical: getSize(15),
    borderBottomColor: THEME.MAIN_COLOR_LIGHT,
    borderBottomWidth: getSize(1),
    borderStyle: 'solid',
  },
  itemHeader: {
    flexDirection: 'row',
    paddingBottom: getSize(15),
    alignItems: 'center',
  },
  itemHeaderTextWrapper: {
    paddingLeft: getSize(15),
  },
  itemHeaderDateWrapper: {

  },
  itemHeaderText: {
    fontSize: THEME.FONT_SIZES.h2,
    paddingBottom: getSize(5),
  },
  itemHeaderDate: {
    fontSize: THEME.FONT_SIZES.comment,
    textTransform: 'uppercase',
  },
  text: {
    paddingBottom: getSize(20),
    fontSize: THEME.FONT_SIZES.text,
  },
  avatar: {
    width: getSize(40),
    height: getSize(40),
    borderRadius: getSize(80 / 2),
  },
  postImage: {
    marginBottom: getSize(15),
    borderRadius: getSize(20),
    width: '100%',
    height: getSize(195),
    resizeMode: 'cover',
  },
  comment: {
    paddingVertical: getSize(15),
    marginHorizontal: getSize(25),
    borderBottomColor: THEME.MAIN_COLOR_LIGHT,
    borderBottomWidth: getSize(1),
    borderStyle: 'solid',
  },
  commentText: {
    fontSize: getSize(12)
  },
  commentLast: {
    marginHorizontal: getSize(25),
    paddingVertical: getSize(15),
  },
  commentArrow: {
    position: 'absolute',
    top: getSize(15),
    left: getSize(-23),
  },
  containerLoader: {
    position: "absolute",
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    justifyContent: "center",
    alignItems: "center",
  },
  likeButton: {
    padding: getSize(4),
  }
})