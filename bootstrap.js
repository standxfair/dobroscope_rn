import * as Font from 'expo-font'
import { Asset } from 'expo-asset';

export const bootstrap = async () => {
  await Font.loadAsync({
    'ceraPro-black': require('./assets/fonts/CeraPro-Black.ttf'),
    'ceraPro-bold': require('./assets/fonts/CeraPro-Bold.ttf'),
    'ceraPro-regular': require('./assets/fonts/CeraPro-Regular.ttf'),
    'ceraPro-light': require('./assets/fonts/CeraPro-Light.ttf'),
    'ceraPro-medium': require('./assets/fonts/CeraPro-Medium.ttf'),
  })
}