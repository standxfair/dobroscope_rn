import React, { useEffect, useRef, useState } from 'react';
import { captureRef } from 'react-native-view-shot';
import { StyleSheet, View, Share, Image, TouchableOpacity } from 'react-native';
import { useSelector } from 'react-redux'
import { THEME, getSize } from '../../styles/theme';
import { AppTextBold } from '../ui/AppTextBold'
import { AppText } from '../ui/AppText'
import Logo from '../../assets/project-logo-full.svg'
import Todo from '../../components/Todo'
import AppStore from '../../assets/app_store.svg'
import GooglePlay from '../../assets/google_play.svg'
import { TODOS_MIN_LENGTH } from '../../constants/app'
import { getFilteredByTodayTodos } from '../../selectors/todos'
import { fetchTodayFinishedCustomTodos } from '../../helpers/todos'
import moment from 'moment';
import { DB_DATE_FORMAT } from '../../constants/app'
import * as Sharing from 'expo-sharing';

export const ShareTodosList = ({ navigation }) => {
  const todayFinishedTodos = useSelector(state => getFilteredByTodayTodos(state))
  const userId = useSelector(state => state.user.uid)
  const viewRef = useRef();
  const [customTodos, setCustomTodos] = useState([])

  useEffect(() => {
    const fetchCustomTodos = async (id) => {
      let data = await fetchTodayFinishedCustomTodos(id)
      data = data.filter(todo => {
        if (todo.date) {
          return (moment(todo.date, DB_DATE_FORMAT).format(DB_DATE_FORMAT) === moment().format(DB_DATE_FORMAT)) && (todo.isChecked === true)
        } else {
          return false
        }
      })
      setCustomTodos(data)
    }
    fetchCustomTodos(userId)
  }, [])

  useEffect(() => {
    const share = async () => {
      await onShare()
    }
    if (todayFinishedTodos) {
      share()
    }
  }, [todayFinishedTodos])

  const goBack = () => navigation.goBack()

  const onShare = async () => {
    try {
      const uri = await captureRef(viewRef, {
        format: 'png',
        quality: 0.8,
      });

      if (Platform.OS === "ios") {
        const result = await Share.share({ url: uri });
        if (result.action === Share.sharedAction) {
          if (result.activityType) {
            goBack()
            // shared with activity type of result.activityType
          } else {
            goBack()
            // shared
          }
        } else if (result.action === Share.dismissedAction) {
          goBack()
          // dismissed
        }
      } else {
        if (!(await Sharing.isAvailableAsync())) {
          alert(`Uh oh, sharing isn't available on your platform`);
          return;
        }

        await Sharing.shareAsync(uri);
      }
    } catch (error) {
      console.log(error.message);
    }
  }

  if (!todayFinishedTodos) {
    return null
  }
  return (
    <View style={styles.modalWrapper} ref={viewRef}>
      <View style={styles.logo}>
        <Logo width={getSize(200)} height={getSize(50)} />
      </View>
      <View style={styles.headerContainer}>
        <AppTextBold style={styles.header}>Сегодня сделал
          добрые дела</AppTextBold>
        <AppText style={styles.counter}>
          <AppTextBold style={styles.counterFirst}>{todayFinishedTodos.length}</AppTextBold>
          /{TODOS_MIN_LENGTH + customTodos.length}
        </AppText>
      </View>

      <View>
        {todayFinishedTodos
          .map(todo => ({ ...todo, isChecked: true }))
          .map(todo => (
            <Todo
              key={todo.id}
              todo={todo}
              checkTodo={() => { }}
              deleteTodo={() => { }}
            />
          ))}
      </View>
      <View style={styles.social}>
        <View style={styles.socialIcon}>
          <TouchableOpacity onPress={() => { }}>
            <AppStore width={getSize(100)} height={getSize(50)} />
          </TouchableOpacity>
        </View>
        <View style={styles.socialIcon}>
          <TouchableOpacity onPress={() => { }}>
            <GooglePlay width={getSize(100)} height={getSize(50)} />
          </TouchableOpacity>
        </View>
      </View>
      <Image
        style={styles.bgImageStyle}
        source={require('../../assets/bg_menu.png')}
      />
    </View>
  )
}

const styles = StyleSheet.create({
  modalWrapper: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: THEME.MAIN_COLOR,
  },
  logo: {
    paddingBottom: getSize(20),
  },
  headerContainer: {
    flexDirection: 'row',
    paddingHorizontal: getSize(25),
    paddingBottom: getSize(20),
  },
  header: {
    color: THEME.TEXT_COLOR_LIGHT,
    fontSize: getSize(20),
    flex: 2,
    paddingTop: getSize(5),
  },
  counter: {
    flex: 1,
    fontSize: getSize(36),
    color: THEME.TEXT_COLOR_LIGHT,
    textAlign: 'right',
    alignItems: 'center',
    justifyContent: 'flex-end'
  },
  counterFirst: {
    color: THEME.TEXT_COLOR_LIGHT,
    fontSize: getSize(56),
  },
  social: {
    flexDirection: 'row',
    paddingTop: getSize(20),
    justifyContent: 'space-around'
  },
  socialIcon: {
    marginRight: getSize(10),
    borderWidth: 2,
    borderColor: THEME.MAIN_COLOR,
    borderRadius: 100,
  },
  bgImageStyle: {
    position: 'absolute',
    bottom: getSize(10),
    right: getSize(-90),
    zIndex: 1,
  }
})