import React, { useState, useEffect } from 'react';
import { StyleSheet, View, Modal, TextInput, TouchableWithoutFeedback, Keyboard, Dimensions } from 'react-native';
import { useDispatch } from 'react-redux'
import { THEME, getSize } from '../../styles/theme';
import { AppButton } from '../AppButton';
import { CloseModalButton } from './CloseModalButton';
import { AppTextBold } from '../ui/AppTextBold'
import { addNewComment } from '../../actions/feedActions'
import { AppText } from '../ui/AppText';
import { COMMENT_TEXT_LENGHT } from '../../constants/app'


export const AddComment = ({ modalVisible, onClose, commentedItemId }) => {
  const [errors, setErrors] = useState({})
  const [text, setText] = useState('')
  const dispatch = useDispatch()
  const [isFocused, setIsFocused] = useState(false)
  const [count, setCount] = useState(0)
  const [isKeyboardVisible, setKeyboardVisible] = useState(false);

  useEffect(() => {
    if (isFocused) {
      setIsFocused(false)
    }
    const keyboardDidShowListener = Keyboard.addListener(
      'keyboardDidShow',
      () => {
        setKeyboardVisible(true); // or some other action
      }
    );
    const keyboardDidHideListener = Keyboard.addListener(
      'keyboardDidHide',
      () => {
        setKeyboardVisible(false); // or some other action
      }
    );
    return () => {
      keyboardDidHideListener.remove();
      keyboardDidShowListener.remove();
    }
  }, [])

  const inputHandler = (text) => {
    if (text.length <= COMMENT_TEXT_LENGHT) {
      setText(text)
      setCount(text.length)
    }
  }

  const focusHandler = (type = 'onFocus') => {
    if (type === 'onFocus') {
      setIsFocused(true)
    } else {
      setIsFocused(false)
    }
  }

  const submit = () => {
    if (text) {
      dispatch(addNewComment(text, commentedItemId))
      setIsFocused(false)
      setText('')
      onClose()
    }
  }

  return (
    <Modal
      visible={modalVisible}
      animationType="slide"
      transparent={true}
      propagateSwipe={true}
    >
      <TouchableWithoutFeedback
        onPress={() => {
          Keyboard.dismiss()
          setIsFocused(false)
        }}
        accessible={false}
      >
        <View style={styles.modalWrapper}>
          <View style={isKeyboardVisible
            ? styles.modalContentKeyboardOpen
            : styles.modalContent
          }
          >
            <CloseModalButton
              style={styles.modalClose}
              onClose={() => {
                setIsFocused(false)
                onClose()
              }}
            />

            <AppTextBold style={styles.modalHeader}>
              Ваш комментарий
            </AppTextBold>

            <View styles={styles.formInputWrapper}>
              <TextInput
                style={styles.formInput}
                value={text}
                onChangeText={text => inputHandler(text)}
                autoCorrect={false}
                placeholder='Текст комментария'
                multiline={false}
                onFocus={() => focusHandler()}
                onBlur={() => focusHandler('onBlur')}
              />
            </View>
            <AppText style={styles.counter}>{count}/{COMMENT_TEXT_LENGHT}</AppText>

            <AppButton
              onPress={submit}
              title="Отправить"
              style={{
                shareButtonWrapper: {
                  paddingTop: getSize(28),
                  paddingBottom: getSize(60),
                  zIndex: 10,
                },
                shareButton: {
                  paddingHorizontal: getSize(40),
                  paddingTop: getSize(10),
                  paddingBottom: getSize(10),
                  backgroundColor: THEME.MAIN_COLOR,
                  borderRadius: getSize(30),
                  shadowColor: THEME.MAIN_COLOR,
                  shadowOffset: {
                    width: 0,
                    height: 6,
                  },
                  shadowOpacity: (0.37),
                  shadowRadius: (7.49),
                  elevation: (12),
                },
                shareButtonText: {
                  textAlign: 'center',
                  fontSize: getSize(14),
                  color: THEME.TEXT_COLOR_LIGHT,
                  textTransform: 'uppercase',
                },
              }}
            />
          </View>
        </View>
      </TouchableWithoutFeedback>
    </Modal>
  )
}

const styles = StyleSheet.create({
  modalWrapper: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(52, 52, 52, 0.8)',
  },
  modalContent: {
    backgroundColor: THEME.TEXT_COLOR_LIGHT,
    opacity: 1,
    alignItems: 'center',
    borderRadius: getSize(30),
    padding: getSize(40),
    marginBottom: getSize(-480)
  },
  modalContentKeyboardOpen: {
    backgroundColor: THEME.TEXT_COLOR_LIGHT,
    opacity: 1,
    alignItems: 'center',
    borderRadius: getSize(30),
    padding: getSize(40),
    marginBottom: 0
  },
  modalClose: {
    position: 'absolute',
    right: getSize(15),
    top: getSize(10),
    zIndex: 99999,
  },
  modalHeader: {
    color: THEME.MAIN_COLOR,
    fontSize: Dimensions.get('window').width <= 320 ? getSize(20) : THEME.FONT_SIZES.h1,
    paddingVertical: getSize(20),
    textAlign: 'center',
  },
  modalDescription: {
    color: THEME.GREY_DARK,
    fontSize: THEME.FONT_SIZES.h2,
    paddingBottom: getSize(40),
    textAlign: 'center',
  },
  modalFooter: {
    color: THEME.GREY_DARK,
    fontSize: THEME.FONT_SIZES.h2,
    textAlign: 'center',
    paddingHorizontal: getSize(30),
    paddingTop: getSize(40),
  },
  achivTitle: {
    color: THEME.TEXT_COLOR_DARK,
    fontSize: getSize(17),
    textAlign: 'center',
    paddingHorizontal: getSize(90),
    paddingTop: getSize(40),
  },
  formInputWrapper: {
    alignItems: 'center',
  },
  formInput: {
    textAlign: 'center',
    borderBottomColor: THEME.GREY_DARK,
    borderBottomWidth: getSize(1),
    borderStyle: 'solid',
    color: THEME.GREY_DARK,
    fontSize: getSize(12),
    fontFamily: THEME.MEDIUM,
    width: Dimensions.get('window').width / 2
  },
  counter: {
    color: THEME.GREY_DARK,
    textAlign: 'right',
    paddingTop: getSize(5),
    width: getSize(70) + '%',
  }
})