import React, { useEffect, useRef } from 'react';
import { captureRef } from 'react-native-view-shot';
import { StyleSheet, View, Image, TouchableOpacity, Dimensions, Platform, Share } from 'react-native';
import { useSelector } from 'react-redux'
import { THEME, getSize } from '../../styles/theme';
import { AppText } from '../ui/AppText'
import Logo from '../../assets/project-logo-full-main.svg'
import AppStore from '../../assets/app_store-main.svg'
import GooglePlay from '../../assets/google_play-main.svg'
import { AppTextBlack } from '../../components/ui/AppTextBlack'
import { fixNumSuffix } from '../../helpers/fixNumSuffix'
import { getFilteredByTodayTodos } from '../../selectors/todos'
import * as Sharing from 'expo-sharing';


export const ShareCounter = ({ navigation }) => {
  const todayFinishedTodos = useSelector(state => getFilteredByTodayTodos(state))
  const count = useSelector(state => state.todos.totalCounter)
  const viewRef = useRef();

  useEffect(() => {
    const share = async () => {
      await onShare()
    }

    if (todayFinishedTodos) {
      share()
    }
  }, [todayFinishedTodos])

  const goBack = () => navigation.goBack()

  const onShare = async () => {
    try {
      const uri = await captureRef(viewRef, {
        format: 'png',
        quality: 0.8,
      });

      if (Platform.OS === "ios") {
        const result = await Share.share({ url: uri });
        if (result.action === Share.sharedAction) {
          if (result.activityType) {
            goBack()
            // shared with activity type of result.activityType
          } else {
            goBack()
            // shared
          }
        } else if (result.action === Share.dismissedAction) {
          goBack()
          // dismissed
        }
      } else {
        if (!(await Sharing.isAvailableAsync())) {
          alert(`Uh oh, sharing isn't available on your platform`);
          return;
        }

        await Sharing.shareAsync(uri);
      }
    } catch (error) {
      console.log(error.message);
    }
  }

  if (!todayFinishedTodos) {
    return null
  }

  return (
    <View style={styles.container} ref={viewRef}>
      <View style={styles.logo}>
        <Logo width={getSize(200)} height={getSize(50)} style={{ flex: 1 }} />
      </View>
      <View style={styles.wrapperWithImage} collapsable={false}>
        <View style={styles.wrapper}>
          <View>
            <AppText style={styles.description}>Я сделал мир лучше</AppText>
          </View>
          <View>
            <AppText style={styles.countText}>
              <AppTextBlack style={styles.count}>{count}</AppTextBlack> {fixNumSuffix(count, ['раз', 'раза'])}
            </AppText>
          </View>
          <View>
            <AppText style={styles.descriptionSecond}>
              Это очень много!
            </AppText>
          </View>
        </View>
        <Image
          style={styles.image}
          source={require('../../assets/big_logo.png')}
        />
      </View>
      <View style={styles.descriptionRow}>
        <AppText style={styles.descriptionRowMarker}>—</AppText>
        <AppText style={styles.descriptionRowText}>Кстати, ты можешь тоже</AppText>
      </View>

      <View style={styles.social}>
        <View style={styles.socialIcon}>
          <TouchableOpacity onPress={() => { }}>
            <AppStore width={getSize(100)} height={getSize(50)} />
          </TouchableOpacity>
        </View>
        <View style={styles.socialIcon}>
          <TouchableOpacity onPress={() => { }}>
            <GooglePlay width={getSize(100)} height={getSize(50)} />
          </TouchableOpacity>
        </View>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  logo: {
    paddingBottom: getSize(20)
  },
  social: {
    flexDirection: 'row',
    paddingTop: getSize(20),
    justifyContent: 'space-around'
  },
  socialIcon: {
    marginRight: getSize(10)
  },
  container: {
    paddingHorizontal: getSize(20),
    paddingTop: getSize(50),
    backgroundColor: THEME.TEXT_COLOR_LIGHT,
  },
  count: {
    fontSize: getSize(113),
    color: THEME.MAIN_COLOR,
    lineHeight: getSize(105),
  },
  countText: {
    color: THEME.MAIN_COLOR,
    fontSize: getSize(12),
  },
  description: {
    fontSize: getSize(17),
  },
  descriptionSecond: {
    fontSize: getSize(17),
    paddingBottom: getSize(20),
    marginTop: getSize(-15),
  },
  descriptionRow: {
    flexDirection: 'row',
    paddingBottom: getSize(10),
    width: Dimensions.get('window').width / 1.3,
  },
  descriptionRowMarker: {
    flex: 1,
    textAlign: 'right',
    paddingRight: getSize(5),
  },
  descriptionRowText: {
    flex: 2,
    justifyContent: 'flex-end',
  },
  wrapperWithImage: {
    flexDirection: 'row',
    height: getSize(180),
  },
  wrapper: {
    backgroundColor: THEME.BLUE_LIGHT,
    borderRadius: getSize(100),
    flex: 2,
    zIndex: 1,
    position: 'absolute',
    left: getSize(-80),
    top: 0,
    paddingLeft: getSize(80),
    paddingRight: getSize(40),
    paddingTop: getSize(15),
  },
  image: {
    flex: 1,
    zIndex: 2,
    position: 'absolute',
    top: getSize(-90),
    right: getSize(-200),
    height: getSize(332),
    width: getSize(306),
  }
})