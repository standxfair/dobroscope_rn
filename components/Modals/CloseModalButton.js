import React from 'react';
import { TouchableOpacity, Image, View } from 'react-native';
import { AntDesign } from '@expo/vector-icons';
import { THEME, getSize } from '../../styles/theme'


export const CloseModalButton = ({ onClose = () => { }, styleMode, style }) => {
  return (
    <View style={style}>
      <TouchableOpacity onPress={onClose}>
        <AntDesign name="close" size={getSize(32)} color={styleMode === 'main' ? THEME.TEXT_COLOR_LIGHT : THEME.MAIN_COLOR} />
      </TouchableOpacity>
    </View>
  )
}