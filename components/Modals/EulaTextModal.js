import React, { useState, useEffect } from 'react';
import { StyleSheet, View, Modal, ScrollView } from 'react-native';
import { THEME, getSize } from '../../styles/theme';
import { CloseModalButton } from './CloseModalButton';
import { AppText } from '../ui/AppText'
import TextsApi from "../../helpers/texts";


export const EulaTextModal = ({
  modalVisible,
  onClose
}) => {
  const [text, setText] = useState('')

  useEffect(() => {
    async function fetchText() {
      const res = await TextsApi.getAgreementText()
      setText(res)
    }
    fetchText()
  }, [])
  return (
    <Modal
      visible={modalVisible}
      animationType="fade"
      transparent={true}
      propagateSwipe={true}
    >
      <ScrollView contentContainerStyle={styles.modalWrapper}>
        <View style={styles.modalContent}>
          <CloseModalButton
            style={styles.modalClose}
            onClose={onClose}
          />
          <AppText style={styles.modalDescription}>
            {text}
          </AppText>
        </View>
      </ScrollView>
    </Modal>
  )
}


const styles = StyleSheet.create({
  modalWrapper: {
    // flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(52, 52, 52, 0.8)',
  },
  modalContent: {
    backgroundColor: THEME.TEXT_COLOR_LIGHT,
    opacity: 1,
    alignItems: 'center',
    borderRadius: getSize(30),
    margin: getSize(20),
    padding: getSize(35),
  },
  modalClose: {
    position: 'absolute',
    right: getSize(15),
    top: getSize(10),
    zIndex: 99999,
  },
  modalHeader: {
    color: THEME.MAIN_COLOR,
    fontSize: THEME.FONT_SIZES.h1,
    paddingVertical: getSize(20),
    textAlign: 'center',
  },
  modalDescription: {
    color: THEME.GREY_DARK,
    fontSize: THEME.FONT_SIZES.h2,
    paddingBottom: getSize(40),
    textAlign: 'left',
    lineHeight: getSize(15)
  },
  modalFooter: {
    color: THEME.GREY_DARK,
    fontSize: THEME.FONT_SIZES.h2,
    textAlign: 'center',
    paddingHorizontal: getSize(30),
    paddingTop: getSize(40),
  },
  achivTitle: {
    color: THEME.TEXT_COLOR_DARK,
    fontSize: getSize(17),
    textAlign: 'center',
    paddingHorizontal: getSize(90),
    paddingTop: getSize(40),
  },
})