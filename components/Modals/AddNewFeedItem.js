import React, { useState } from 'react';
import { StyleSheet, View, Modal, TextInput, TouchableWithoutFeedback, Keyboard, Dimensions } from 'react-native';
import { useDispatch, useSelector } from 'react-redux'
import * as firebase from 'firebase';
import { THEME, getSize } from '../../styles/theme';
import moment from 'moment'
import { AppButton } from '../AppButton';
import { CloseModalButton } from './CloseModalButton';
import { AppTextBold } from '../ui/AppTextBold'
import { addNewItem } from '../../actions/feedActions'
import { PhotoPickerPost } from '../PhotoPickerPost'
import { prepareBlob } from '../../helpers/image'
import { COMMENT_TEXT_LENGHT } from '../../constants/app'
import { AppText } from '../ui/AppText';


export const AddNewFeedItem = ({ modalVisible, onClose, }) => {
  const [errors, setErrors] = useState({})
  const [text, setText] = useState('')
  const [image, setImage] = useState(null)
  const [loading, setLoading] = useState(false)
  const [count, setCount] = useState(0)

  const userId = useSelector(state => state.user.uid)
  const dispatch = useDispatch()

  const inputHandler = (text) => {
    if (text.length <= COMMENT_TEXT_LENGHT) {
      setText(text)
      setCount(text.length)
    }
  }

  const uploadImage = async (imageUri, userId) => {
    const ref = firebase.storage().ref('users').child(userId).child('post_' + moment().unix())
    setLoading(true)
    try {
      // convert to blob
      const blob = await prepareBlob(imageUri)
      const snapshot = await ref.put(blob)
      let downloadUrl = await ref.getDownloadURL()

      blob.close()

      setLoading(false)

      return downloadUrl
    } catch (error) {
      console.log('error', error.message);
    }
  }

  const submit = async () => {
    if (loading) {
      return
    }

    if (text) {

      let imgUrl
      if (image) {
        imgUrl = await uploadImage(image, userId)
        setImage(null)
      }
      let item = { text }
      if (imgUrl) {
        item.imgUrl = imgUrl
      }

      dispatch(addNewItem(item))

      setText('')

      onClose()
    }
  }

  return (
    <Modal
      visible={modalVisible}
      animationType="fade"
      transparent={true}
      statusBarTranslucent={true}
      hardwareAccelerated={true}
    >
      <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
        <View style={styles.modalWrapper}>
          <View style={styles.modalContent}>
            <CloseModalButton
              style={styles.modalClose}
              onClose={onClose}
            />

            <AppTextBold style={styles.modalHeader}>
              Написать в ленту
            </AppTextBold>

            <View style={styles.inputContainer}>
              <TextInput
                style={!errors.login ? styles.formInput : { ...styles.formInput, ...INPUT_ERROR }}
                value={text}
                onChangeText={text => inputHandler(text)}
                autoCorrect={false}
                placeholder='Кратко расскажите о том, что сделали'
                multiline={true}
              />
            </View>
            <AppText style={styles.counter}>
              {count}/{COMMENT_TEXT_LENGHT}
            </AppText>

            <PhotoPickerPost image={image} onPick={setImage} />

            <AppButton
              onPress={submit}
              title={!loading ? "Разместить" : "Загрузка..."}
              style={{
                shareButtonWrapper: {
                  zIndex: 10,
                },
                shareButton: {
                  paddingHorizontal: getSize(40),
                  paddingTop: getSize(10),
                  paddingBottom: getSize(10),
                  backgroundColor: THEME.MAIN_COLOR,
                  borderRadius: getSize(30),
                  shadowColor: THEME.MAIN_COLOR,
                  shadowOffset: {
                    width: 0,
                    height: 6,
                  },
                  shadowOpacity: (0.37),
                  shadowRadius: (7.49),
                  elevation: (12),
                },
                shareButtonText: {
                  textAlign: 'center',
                  fontSize: getSize(14),
                  color: THEME.TEXT_COLOR_LIGHT,
                  textTransform: 'uppercase',
                },
              }}
            />
          </View>
        </View>
      </TouchableWithoutFeedback>
    </Modal>
  )
}


const styles = StyleSheet.create({
  modalWrapper: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(52, 52, 52, 0.8)',
  },
  modalContent: {
    backgroundColor: THEME.TEXT_COLOR_LIGHT,
    opacity: 1,
    alignItems: 'center',
    borderRadius: getSize(30),
    margin: getSize(20),
    paddingTop: getSize(40),
    paddingBottom: getSize(10),
    paddingLeft: getSize(10),
    paddingRight: getSize(10),
  },
  modalClose: {
    position: 'absolute',
    right: getSize(15),
    top: getSize(10),
    zIndex: 99999,
  },
  modalHeader: {
    color: THEME.MAIN_COLOR,
    fontSize: THEME.FONT_SIZES.h1,
    paddingVertical: getSize(20),
    textAlign: 'center',
  },
  modalDescription: {
    color: THEME.GREY_DARK,
    fontSize: THEME.FONT_SIZES.h2,
    paddingBottom: getSize(40),
    textAlign: 'center',
  },
  modalFooter: {
    color: THEME.GREY_DARK,
    fontSize: THEME.FONT_SIZES.h2,
    textAlign: 'center',
    paddingHorizontal: getSize(30),
    paddingTop: getSize(40),
  },
  achivTitle: {
    color: THEME.TEXT_COLOR_DARK,
    fontSize: getSize(17),
    textAlign: 'center',
    paddingHorizontal: getSize(90),
    paddingTop: getSize(40),
  },
  inputContainer: {
    alignSelf: 'stretch',
  },
  formInput: {
    borderBottomColor: THEME.GREY_DARK,
    borderBottomWidth: getSize(1),
    borderStyle: 'solid',
    color: THEME.GREY_DARK,
    fontSize: getSize(12),
    paddingTop: getSize(10),
    fontFamily: THEME.MEDIUM,
    maxHeight: getSize(60),
    minHeight: getSize(40),

  },
  counter: {
    color: THEME.GREY_DARK,
    textAlign: 'right',
    paddingTop: getSize(5),
    width: getSize(70) + '%',
  }
})