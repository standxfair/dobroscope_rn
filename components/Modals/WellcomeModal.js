import React, { useState } from 'react';
import { StyleSheet, View, Modal, TextInput, Dimensions, TouchableOpacity, Switch } from 'react-native';
import { THEME, getSize } from '../../styles/theme';
import { useSelector, useDispatch } from 'react-redux'
import * as firebase from 'firebase';
import { AppButton } from '../AppButton';
import { AppTextBold } from '../ui/AppTextBold'
import { validateHandler } from '../../helpers/validateHandler'
import { LOGIN_SHCEMA } from '../../constants/validation'
import { setUserData, initializeUser } from '../../actions/userActions'
import { AppText } from '../ui/AppText'
import { EulaTextModal } from '../../components/Modals/EulaTextModal'
import { setEulaAccept } from '../../helpers/user'


export const WellcomeModal = ({ modalVisible, setShowWellcomeModal }) => {
  const [name, setName] = useState('')
  const [errors, setErrors] = useState({})
  const [isEulaAccepted, setIsEulaAccepted] = useState(false);
  const [showEulaModal, setShowEulaModal] = useState(false)
  const user = useSelector(state => state.user)
  const dispatch = useDispatch()

  const inputHandler = (type = 'name') => (text) => {
    validateHandler(type, text, LOGIN_SHCEMA, errors, setErrors)
    setName(text)
  }

  const submit = async () => {
    if (!isEulaAccepted) {
      return
    }
    //сохраняем юзернейм
    if (validateHandler('name', name, LOGIN_SHCEMA, errors, setErrors)) {
      const userFirebase = firebase.auth().currentUser;
      try {
        await userFirebase.updateProfile({ displayName: name })
        dispatch(setUserData({ ...user, displayName: name, isEulaAccepted }))
        await setEulaAccept({ isEulaAccepted, userId: user.uid })

        dispatch(initializeUser(true))
        setShowWellcomeModal(false)
      } catch (error) {
        console.log('error', error);
      }
    }
  }

  const acceptEula = async () => {
    const accept = !isEulaAccepted
    await setEulaAccept({ isEulaAccepted: accept, userId: user.uid })
    setIsEulaAccepted(accept)
    dispatch(setUserData({ ...user, isEulaAccepted: accept }))
  }

  const closeEulaModal = () => {
    setShowEulaModal(false)
  }

  return (
    <Modal
      visible={modalVisible}
      animationType="fade"
      transparent={true}
    >
      <View style={styles.modalWrapper}>
        <View style={styles.modalContent}>

          <AppTextBold style={styles.modalHeader}>
            Привет, представьтесь, пожалуйста
          </AppTextBold>

          <View style={{ width: getSize(60) + '%' }}>
            <TextInput
              style={!errors.login ? styles.formInput : { ...styles.formInput, ...INPUT_ERROR }}
              value={name}
              onChangeText={text => inputHandler()(text)}
              autoCorrect={false}
              placeholder='Введите свое имя'
            />
          </View>

          <View style={styles.switchContainer}>
            <Switch
              trackColor={{ false: THEME.MAIN_COLOR_LIGHT, true: THEME.MAIN_COLOR_LIGHT }}
              thumbColor={THEME.MAIN_COLOR}
              ios_backgroundColor={THEME.MAIN_COLOR_LIGHT}
              onValueChange={acceptEula}
              value={isEulaAccepted}
            />
            <AppText style={styles.switchContainerLabel}>
              Я принимаю условия <TouchableOpacity onPress={() => {
                setShowEulaModal(true)
              }}>
                <AppText style={styles.switchContainerLabelLink}>
                  пользовательского соглашения
                </AppText>
              </TouchableOpacity>
            </AppText>
          </View>

          <AppButton
            onPress={submit}
            title="Продолжить"
            style={{
              shareButtonWrapper: {
                paddingTop: getSize(28),
                paddingBottom: getSize(60),
                zIndex: 10,
              },
              shareButton: {
                paddingHorizontal: getSize(40),
                paddingTop: getSize(10),
                paddingBottom: getSize(10),
                backgroundColor: isEulaAccepted && name ? THEME.MAIN_COLOR : THEME.MAIN_COLOR_LIGHT,
                borderRadius: getSize(30),
                shadowColor: THEME.MAIN_COLOR,
                shadowOffset: {
                  width: 0,
                  height: 6,
                },
                shadowOpacity: (0.37),
                shadowRadius: (7.49),
                elevation: (12),
              },
              shareButtonText: {
                textAlign: 'center',
                fontSize: getSize(14),
                color: THEME.TEXT_COLOR_LIGHT,
                textTransform: 'uppercase',
              },
            }}
          />
        </View>
      </View>
      <EulaTextModal modalVisible={showEulaModal} onClose={closeEulaModal} />
    </Modal>
  )
}


const styles = StyleSheet.create({
  modalWrapper: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(52, 52, 52, 0.8)',
  },
  modalContent: {
    backgroundColor: THEME.TEXT_COLOR_LIGHT,
    opacity: 1,
    alignItems: 'center',
    borderRadius: getSize(30),
    margin: getSize(20),
    padding: getSize(35),
  },
  modalClose: {
    position: 'absolute',
    right: getSize(15),
    top: getSize(10),
    zIndex: 99999,
  },
  modalHeader: {
    color: THEME.MAIN_COLOR,
    fontSize: Dimensions.get('window').width <= 320 ? getSize(20) : THEME.FONT_SIZES.h1,
    paddingVertical: getSize(20),
    textAlign: 'center',
  },
  modalDescription: {
    color: THEME.GREY_DARK,
    fontSize: THEME.FONT_SIZES.h2,
    paddingBottom: getSize(40),
    textAlign: 'center',
  },
  modalFooter: {
    color: THEME.GREY_DARK,
    fontSize: THEME.FONT_SIZES.h2,
    textAlign: 'center',
    paddingHorizontal: getSize(30),
    paddingTop: getSize(40),
  },
  achivTitle: {
    color: THEME.TEXT_COLOR_DARK,
    fontSize: getSize(17),
    textAlign: 'center',
    paddingHorizontal: getSize(90),
    paddingTop: getSize(40),
  },
  formInput: {
    borderBottomColor: THEME.GREY_DARK,
    borderBottomWidth: getSize(2),
    borderStyle: 'solid',
    marginBottom: getSize(20),
    color: THEME.GREY_DARK,
    fontSize: getSize(16),
    paddingTop: getSize(10),
  },
  checkbox: {
    marginRight: getSize(5),
    marginTop: getSize(2),
  },
  checkboxWrapper: {
    borderColor: THEME.MAIN_COLOR,
    borderWidth: getSize(0.4),
    borderStyle: 'solid',
    width: getSize(16),
    height: getSize(16),
    borderRadius: getSize(50),
  },
  checkboxWrapperChecked: {
    backgroundColor: THEME.MAIN_COLOR,
  },
  todo: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingTop: getSize(10),
    paddingRight: getSize(10),
    paddingBottom: getSize(10),
  },
  text: {
    paddingLeft: getSize(5),
  },
  switchContainer: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    marginBottom: getSize(20),
  },
  switchContainerLabel: {
    fontSize: getSize(13),
    paddingLeft: Platform.OS === 'ios' ? getSize(10) : 0,
  },
  switchContainerLabelLink: {
    fontSize: getSize(13),
    paddingLeft: Platform.OS === 'ios' ? getSize(10) : 0,
    textDecorationLine: 'underline'
  },
})