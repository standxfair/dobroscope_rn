import React, { useEffect, useRef, } from 'react';
import { captureRef } from 'react-native-view-shot';
import { StyleSheet, Platform, Share, View, ImageBackground } from 'react-native';
import * as Sharing from 'expo-sharing';
import { AppTextLight } from '../../components/ui/AppTextLight'
import { AppTextBold } from '../../components/ui/AppTextBold'
import { THEME, getSize } from '../../styles/theme';
import { setTodayMotivation } from '../../actions/motivationActions'
import { getSortedMotivation, getCurrentDayMotivation } from '../../selectors/motivation'
import { DB_DATE_FORMAT } from '../../constants/app';
import moment from 'moment'


export const ShareMotivationCard = ({ navigation, route }) => {
  const viewRef = useRef();

  useEffect(() => {
    const share = async () => {
      await onShare()
    }

    share()
  }, [])

  const goBack = () => navigation.goBack()

  const onShare = async () => {
    try {
      const uri = await captureRef(viewRef, {
        format: 'png',
        quality: 0.8,
      });

      if (Platform.OS === "ios") {
        const result = await Share.share({ url: uri });
        if (result.action === Share.sharedAction) {
          if (result.activityType) {
            goBack()
            // shared with activity type of result.activityType
          } else {
            goBack()
            // shared
          }
        } else if (result.action === Share.dismissedAction) {
          goBack()
          // dismissed
        }
      } else {
        if (!(await Sharing.isAvailableAsync())) {
          alert(`Uh oh, sharing isn't available on your platform`);
          return;
        }

        await Sharing.shareAsync(uri);
      }
    } catch (error) {
      console.log(error.message);
    }
  }
  const { params: { data } } = route

  return (
    <ImageBackground
      style={styles.quoteOfTheDay}
      source={require('../../assets/motiv_bg.png')}
      collapsable={false}
      ref={viewRef}
    >
      <View style={styles.quoteOfTheDayHeader}>
        <View style={styles.quoteOfTheDayHeaderTitle}>
          {!data.type ? (
            <AppTextLight style={styles.quoteOfTheDayHeaderTitleText}>
            </AppTextLight>
          ) : (
            <AppTextLight style={styles.quoteOfTheDayHeaderTitleText}>
              {data.type === 'Цитата' ? 'Цитата дня' : 'Мотивация дня'}
            </AppTextLight>
          )}

        </View>
        <View style={styles.quoteOfTheDayHeaderDate}>
          <AppTextLight style={styles.quoteOfTheDayHeaderDateText}>
            {moment().format('DD MMMM')}
          </AppTextLight>
        </View>
      </View>

      <View style={styles.quoteOfTheDayTitle}>
        {!data.title ? (
          <AppTextBold style={styles.quoteOfTheDayTitleText}>
          </AppTextBold>
        ) : (
          <AppTextBold style={styles.quoteOfTheDayTitleText}>
            {data.title}
          </AppTextBold>
        )}

      </View>

      <View style={styles.quoteOfTheDayFooter}>
        <View style={styles.quoteOfTheDayFooterTitle}>

        </View>
        <View style={styles.quoteOfTheDayFooterAuthor}>
          {data.author ?
            <AppTextLight style={styles.quoteOfTheDayFooterAuthorText}>
              {data.author}
            </AppTextLight> : null}
        </View>
      </View>
    </ImageBackground>
  )
}

const styles = StyleSheet.create({
  container: {
    paddingTop: getSize(20),
    paddingHorizontal: getSize(20),
    flex: 1,
    backgroundColor: THEME.TEXT_COLOR_LIGHT,
  },

  quoteOfTheDay: {
    backgroundColor: THEME.MAIN_COLOR,
    marginBottom: getSize(30),
    padding: getSize(25),
    // borderRadius: getSize(20),
  },
  quoteOfTheDayHeaderTitle: {
    flex: 2,
    color: THEME.TEXT_COLOR_LIGHT,
  },
  quoteOfTheDayHeaderTitleText: {
    fontSize: getSize(12),
    color: THEME.TEXT_COLOR_LIGHT,
  },
  quoteOfTheDayHeader: {
    flexDirection: 'row',
    paddingBottom: getSize(12),
  },
  quoteOfTheDayHeaderDate: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    textAlign: 'right',
  },
  quoteOfTheDayHeaderDateText: {
    textTransform: 'uppercase',
    color: THEME.TEXT_COLOR_LIGHT,
  },
  quoteOfTheDayTitle: {
    paddingBottom: getSize(50),
  },
  quoteOfTheDayTitleText: {
    fontSize: getSize(18),
    color: THEME.TEXT_COLOR_LIGHT,
  },
  quoteOfTheDayFooter: {
    flexDirection: 'row',
    // position: 'absolute',
    bottom: getSize(20),
    // right: 0,
    // left: 0,
    // paddingLeft: getSize(25),
    // paddingRight: getSize(25),
  },
  quoteOfTheDayFooterTitle: {
    fontSize: getSize(12),
    flex: 2,
  },
  quoteOfTheDayFooterAuthor: {
    flex: 2,
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    textAlign: 'right',
  },
  quoteOfTheDayFooterTitleText: {
    fontSize: getSize(12),
    color: THEME.TEXT_COLOR_LIGHT,
    paddingLeft: getSize(5),
  },
  quoteOfTheDayFooterAuthorText: {
    fontSize: getSize(12),
    color: THEME.TEXT_COLOR_LIGHT,
  },
  share: {
    flexDirection: 'row',
    alignItems: 'center',
  },

  listWrapper: {
  },
  listTitle: {
    fontSize: getSize(18),
  },
  item: {
    paddingVertical: getSize(15),
    borderBottomColor: THEME.MAIN_COLOR_LIGHT,
    borderBottomWidth: getSize(1),
    borderStyle: 'solid',
  },
  itemHeader: {
    flexDirection: 'row',
    paddingBottom: getSize(15),
  },
  itemHeaderTextWrapper: {
    flex: 2,
  },
  itemHeaderDateWrapper: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    textAlign: 'right',
  },
  itemHeaderText: {
    fontSize: THEME.FONT_SIZES.comment,
  },
  itemHeaderDate: {
    fontSize: THEME.FONT_SIZES.comment,
    textTransform: 'uppercase',
  },
  title: {
    fontSize: THEME.FONT_SIZES.h2,
  },
  itemAuthor: {
    fontSize: THEME.FONT_SIZES.comment,
    textAlign: 'right',
  },
})