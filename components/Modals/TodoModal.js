import React from 'react';
import { StyleSheet, View, Modal, Dimensions } from 'react-native';
import { THEME, getSize } from '../../styles/theme';
import { LinkButton } from '../LinkButton';
import { AppButton } from '../AppButton';
import { CloseModalButton } from './CloseModalButton';
import { AppTextBold } from '../ui/AppTextBold'
import { AppText } from '../ui/AppText'


export const TodoModal = ({
  modalVisible,
  needShare = false,
  onClose,
  title,
  description,
  img,
  footerText,
  achivTitle,
  countComponent,
  descriptionTextAlign = 'center',
  secondaryText
}) => {
  return (
    <Modal
      visible={modalVisible}
      animationType="fade"
      transparent={true}
    >
      <View style={styles.modalWrapper}>
        <View style={styles.modalContent}>
          <CloseModalButton
            style={styles.modalClose}
            onClose={onClose}
          />

          <AppTextBold style={styles.modalHeader}>{title}</AppTextBold>
          <AppText style={{
            ...styles.modalDescription,
            paddingBottom: secondaryText
              ? getSize(20)
              : getSize(40),
            textAlign: descriptionTextAlign
          }}>
            {description}
          </AppText>

          {secondaryText
            ? <AppText style={{ ...styles.modalDescription, textAlign: descriptionTextAlign }}>
              {secondaryText}
            </AppText>
            : null}

          {img ? img : null}

          {countComponent ? countComponent : null}

          {footerText ? (
            <AppText style={styles.modalFooter}>
              {footerText}
            </AppText>
          ) : null}

          {achivTitle ? (
            <AppText style={styles.achivTitle}>
              {footerText}
            </AppText>
          ) : null}

          {needShare ? (
            <View>
              <AppButton />
              <LinkButton />
            </View>
          ) : null}
        </View>
      </View>
    </Modal>
  )
}


const styles = StyleSheet.create({
  modalWrapper: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(52, 52, 52, 0.8)',
  },
  modalContent: {
    backgroundColor: THEME.TEXT_COLOR_LIGHT,
    opacity: 1,
    alignItems: 'center',
    borderRadius: getSize(30),
    margin: getSize(20),
    padding: getSize(35),
  },
  modalClose: {
    position: 'absolute',
    right: getSize(15),
    top: getSize(10),
    zIndex: 99999,
  },
  modalHeader: {
    color: THEME.MAIN_COLOR,
    fontSize: THEME.FONT_SIZES.h1,
    paddingVertical: getSize(20),
    textAlign: 'center',
  },
  modalDescription: {
    color: THEME.GREY_DARK,
    fontSize: THEME.FONT_SIZES.h2,
    paddingBottom: getSize(40),
    textAlign: 'center',
    lineHeight: getSize(15)
  },
  modalFooter: {
    color: THEME.GREY_DARK,
    fontSize: THEME.FONT_SIZES.h2,
    textAlign: 'center',
    paddingHorizontal: getSize(30),
    paddingTop: getSize(40),
  },
  achivTitle: {
    color: THEME.TEXT_COLOR_DARK,
    fontSize: getSize(17),
    textAlign: 'center',
    paddingHorizontal: getSize(90),
    paddingTop: getSize(40),
  },
})