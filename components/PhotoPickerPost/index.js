import React from 'react'
import * as ImagePicker from 'expo-image-picker';
import { View, StyleSheet, Image, TouchableOpacity, Dimensions } from 'react-native';
import { useActionSheet } from '@expo/react-native-action-sheet'
import { getSize } from '../../styles/theme'
import CameraIcon from '../../assets/addPhoto1.svg'
import { askForPermissions } from '../../helpers/image'

export const PhotoPickerPost = ({ onPick = () => { }, image }) => {
  const { showActionSheetWithOptions } = useActionSheet()

  const addImage = () => {
    const options = ['Выбрать фото', 'Сделать фото', 'Отмена'];
    const cancelButtonIndex = 2;

    showActionSheetWithOptions(
      {
        options,
        cancelButtonIndex,
        useModal: true,
      },
      buttonIndex => {
        if (buttonIndex === 0) {
          //фотопоток
          getPhotoFromLibrary()
        } else if (buttonIndex === 1) {
          //камера
          takePhoto()
        }
      },
    );
  }

  const getPhotoFromLibrary = async () => {
    const hasPermissions = await askForPermissions()
    if (!hasPermissions) {
      return
    }

    const img = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.All,
      allowsEditing: true,
      aspect: [16, 16],
      quality: 0.3,
    })

    onPick(img.uri)
  }

  const takePhoto = async () => {
    const hasPermissions = await askForPermissions()
    if (!hasPermissions) {
      return
    }

    const img = await ImagePicker.launchCameraAsync({
      quality: 0.3,
      allowsEditing: true,
      aspect: [16, 16]
    })

    onPick(img.uri)
  }

  return (
    <View style={styles.wrapper}>
      <TouchableOpacity
        onPress={addImage}
        activeOpacity={100}
      >
        {
          image ? (
            <Image
              source={{ uri: image }}
              style={styles.img}
            />
          ) : (
            <View style={styles.icon}>
              <CameraIcon width={Dimensions.get('window').width <= 320 ? 200 : 300} height={200} />
            </View>
          )
        }
      </TouchableOpacity>
    </View>
  )
}

const styles = StyleSheet.create({
  wrapper: {
    marginTop: getSize(75),
    marginBottom: getSize(20),
  },
  icon: {
    alignItems: 'center',
  },
  img: {
    borderRadius: getSize(20),
    height: getSize(160),
    width: Dimensions.get('window').width / 1.5,
    resizeMode: 'cover',
  },
})