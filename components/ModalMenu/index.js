import React, { useState, useEffect } from 'react';
import { StyleSheet, View, Platform } from 'react-native';
import * as firebase from 'firebase';
import { THEME, getSize } from '../../styles/theme';
import { CloseModalButton } from '../../components/Modals/CloseModalButton'
import { UserProfile } from '../UserProfile'
import { EditProfile } from '../UserProfile/EditProfile'


export const ModalMenu = ({ navigation }) => {
  const [showEditProfile, setShowEditProfile] = useState(false)
  const [avatar, setAvatar] = useState(null)

  const getUserPhoto = async () => {
    const userFirebase = firebase.auth().currentUser;
    const storage = firebase.storage().ref('users').child(userFirebase.uid).child('photo')

    await storage.getDownloadURL()
      .then((url) => {
        setAvatar(url)
      })
      .catch((e) => {
        // console.log('e', e);
      })
  }

  useEffect(() => {
    getUserPhoto()
  }, [])

  return (
    <View style={styles.wrapper}>
      <CloseModalButton
        onClose={() => navigation.goBack()}
        styleMode='main'
      />
      {showEditProfile
        ? <EditProfile
          setShowEditProfile={setShowEditProfile}
          avatar={avatar}
          setAvatar={setAvatar}
        />
        : <UserProfile
          setShowEditProfile={setShowEditProfile}
          navigation={navigation}
          avatar={avatar}
        />}
    </View>
  )
}

const styles = StyleSheet.create({
  wrapper: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    backgroundColor: THEME.MAIN_COLOR,
    paddingTop: Platform.OS === 'ios' ? getSize(35) : getSize(5),
    paddingHorizontal: getSize(20),
    zIndex: 99999999,
    flex: 1,

  }
})