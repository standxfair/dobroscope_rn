import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native';
import { THEME, getSize } from '../../styles/theme';
import { AppText } from '../ui/AppText'

export const LinkButton = ({ style, title = 'Почему это не дурной тон?', onPress = () => { } }) => {
  return (
    <View style={style ? style.shareButtonWrapperNoPaddingTop : styles.shareButtonWrapperNoPaddingTop}>
      <TouchableOpacity
        style={style ? style.shareButtonWhy : styles.shareButtonWhy}
        onPress={onPress}
      >
        <AppText style={style ? style.shareButtonTextWhy : styles.shareButtonTextWhy}>
          {title}
        </AppText>
      </TouchableOpacity>
    </View>
  )
}


const styles = StyleSheet.create({
  shareButtonWrapperNoPaddingTop: {
    paddingBottom: getSize(10),
  },
  shareButtonTextWhy: {
    textAlign: 'center',
    fontSize: getSize(14),
    fontFamily: THEME.REGULAR,
    textDecorationColor: THEME.TEXT_COLOR_LIGHT,
    textDecorationStyle: 'solid',
    color: THEME.TEXT_COLOR_LIGHT,
    textDecorationLine: 'underline',
  }
})