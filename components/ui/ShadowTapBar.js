import React from 'react';
import { StyleSheet, Dimensions, Platform } from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';
import { getSize } from '../../styles/theme'

const SHADOW_COLOR = 'rgba(0,0,0,0.2)'

const ShadowTabBar = () => (
  <>
    <LinearGradient
      colors={['transparent', 'transparent', SHADOW_COLOR]}
      style={styles.gradientLeft}
    />
    <LinearGradient
      colors={['transparent', 'transparent', SHADOW_COLOR]}
      style={styles.gradientRight}
    />
  </>
);

const styles = StyleSheet.create({
  gradientLeft: {
    height: 10,
    position: 'absolute',
    left: 0,
    top: Platform.OS === 'ios' ? getSize(-51) : getSize(-48),
    right: getSize(Dimensions.get('window').width / 2.05),
  },
  gradientRight: {
    height: 10,
    position: 'absolute',
    left: getSize(Dimensions.get('window').width / 2.05),
    top: Platform.OS === 'ios' ? getSize(-51) : getSize(-48),
    right: 0,
  }
});

export default ShadowTabBar;