import React from 'react';
import { Text, StyleSheet } from 'react-native'
import { THEME } from '../../styles/theme';


export const AppTextBlack = ({ children, style }) => (
  <Text style={{ ...styles.default, ...style }}>{children}</Text>
)

const styles = StyleSheet.create({
  default: {
    fontFamily: THEME.BLACK,
  }
})