import React, { useState } from 'react';
import { StyleSheet, View, TouchableOpacity, TextInput, Switch, Platform } from 'react-native';
import { useSelector, useDispatch } from 'react-redux'
import { AntDesign } from '@expo/vector-icons';
import * as firebase from 'firebase';
import { AppText } from '../ui/AppText'
import { AppTextBold } from '../ui/AppTextBold'
import { AppTextLight } from '../ui/AppTextLight'
import { AppTextMedium } from '../ui/AppTextMedium'
import { THEME, getSize, INPUT_ERROR } from '../../styles/theme';
import { validateHandler } from '../../helpers/validateHandler'
import { USER_SHCEMA } from '../../constants/validation'
import { PhotoPicker } from '../PhotoPicker'
import { setUserName, setUserEmail, setAgreeReviceEmails, setAgreeRevicePush } from '../../actions/userActions'
import { setUserEmailOnFirebase } from '../../helpers/user'

export const EditProfile = ({ setShowEditProfile, avatar, setAvatar }) => {
  const userId = useSelector(state => state.user.uid)
  const userName = useSelector(state => state.user.displayName)
  const email = useSelector(state => state.user.email)
  const agreeReciveEmails = useSelector(state => state.user.agreeReciveEmails)
  const agreeRecivePush = useSelector(state => state.user.agreeRecivePush)

  const [errors, setErrors] = useState({})

  const dispatch = useDispatch()

  const toggleSwitch = (type = 'email') => async () => {
    if (type === 'email') {
      const valid = validateHandler('email', email, USER_SHCEMA, errors, setErrors)

      if (valid) {
        dispatch(setAgreeReviceEmails(!agreeReciveEmails))

        await setUserEmailOnFirebase(userId, email)
      }
    } else {
      dispatch(setAgreeRevicePush(!agreeRecivePush))
    }
  }

  const inputHandler = (type = 'name') => (text) => {
    validateHandler(type, text, USER_SHCEMA, errors, setErrors)

    if (type === 'name') {
      dispatch(setUserName(text))
    } else {
      dispatch(setUserEmail(text))
    }
  }

  const saveUserData = (type = 'name') => async () => {
    if (type === 'name') {
      if (validateHandler(type, userName, USER_SHCEMA, errors, setErrors)) {
        const userFirebase = firebase.auth().currentUser;

        if (userFirebase.displayName !== userName) {
          await userFirebase.updateProfile({ displayName: userName })
        }
      }
    } else {
      if (validateHandler(type, email, USER_SHCEMA, errors, setErrors)) {
        const userFirebase = firebase.auth().currentUser;

        if (userFirebase.email !== email) {
          await userFirebase.updateProfile({ email })
        }
      }
    }
  }

  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <TouchableOpacity onPress={() => setShowEditProfile(false)}>
          <View style={styles.headerWrapper}>
            <View style={styles.arrow}>
              <AntDesign name="left" size={24} color={THEME.TEXT_COLOR_LIGHT} />
            </View>

            <View style={styles.textContainer}>
              <AppTextBold style={styles.textContainerHead}>Аккаунт</AppTextBold>
              <AppTextLight style={styles.textContainerDescription}>Личные данные и уведомления</AppTextLight>
            </View>
          </View>
        </TouchableOpacity>
      </View>

      <View style={styles.menu}>
        <View style={styles.avatarWrapper}>

          <PhotoPicker onPick={setAvatar} image={avatar} />

          <View style={styles.avatarText}>
            <AppTextMedium style={styles.menuText}>
              Ваше фото
            </AppTextMedium>
            <AppTextLight style={styles.textContainerDescription}>
              Показывается в ленте дел
            </AppTextLight>
          </View>
        </View>
      </View>

      <View style={styles.formWrapper}>
        <View style={styles.formInputWrapper}>
          <AppTextMedium style={styles.formLabel}>
            Ваше имя
            </AppTextMedium>
        </View>
        <TextInput
          style={!errors.name ? styles.formInput : { ...styles.formInput, ...INPUT_ERROR }}
          value={userName}
          onChangeText={text => inputHandler()(text)}
          autoCorrect={false}
          onEndEditing={saveUserData()}
        />
        <View style={styles.formInputWrapper}>
          <AppTextMedium style={styles.formLabel}>
            Электронная почта
            </AppTextMedium>
        </View>
        <TextInput
          style={!errors.email ? styles.formInput : { ...styles.formInput, ...INPUT_ERROR }}
          value={email}
          onChangeText={text => inputHandler('email')(text)}
          autoCorrect={false}
          autoCapitalize='none'
          onEndEditing={saveUserData('email')}
        />
      </View>
      <View style={styles.switchContainer}>
        <Switch
          trackColor={{ false: THEME.MAIN_COLOR_LIGHT, true: THEME.TEXT_COLOR_LIGHT }}
          thumbColor={THEME.MAIN_COLOR}
          ios_backgroundColor={THEME.MAIN_COLOR_LIGHT}
          onValueChange={toggleSwitch()}
          value={agreeReciveEmails}
        />
        <AppText style={styles.switchContainerLabel}>
          Получать e-mail рассылки
        </AppText>
      </View>
      <View style={styles.switchContainer}>
        <Switch
          trackColor={{ false: THEME.MAIN_COLOR_LIGHT, true: THEME.TEXT_COLOR_LIGHT }}
          thumbColor={THEME.MAIN_COLOR}
          ios_backgroundColor={THEME.MAIN_COLOR_LIGHT}
          onValueChange={toggleSwitch('push')}
          value={agreeRecivePush}
        />
        <AppText style={styles.switchContainerLabel}>
          Push-уведомления
        </AppText>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: getSize(20),
  },
  header: {},
  headerWrapper: {
    flexDirection: 'row',
    alignItems: 'center',
    borderBottomColor: THEME.TEXT_COLOR_LIGHT,
    borderBottomWidth: getSize(1),
    borderStyle: 'solid',
    paddingBottom: getSize(20),
    marginBottom: getSize(30),
    justifyContent: 'flex-start',
  },
  avatarWrapper: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  avatarText: {},
  textContainer: {
    flex: 5,
  },
  textContainerHead: {
    fontSize: getSize(15),
    color: THEME.TEXT_COLOR_LIGHT,
  },
  textContainerDescription: {
    fontSize: getSize(12),
    color: THEME.TEXT_COLOR_LIGHT,
    textTransform: 'uppercase',
    paddingTop: getSize(5),
  },
  arrow: {
    marginRight: getSize(20),
  },
  menu: {
    justifyContent: 'flex-start',
  },
  menuText: {
    color: THEME.TEXT_COLOR_LIGHT,
    fontSize: getSize(15),
    textTransform: 'uppercase',
  },
  formWrapper: {
    paddingTop: getSize(20),
  },
  formLabel: {
    color: THEME.MAIN_COLOR_LIGHT,
    fontSize: getSize(12),
    textTransform: 'uppercase',
  },
  formInput: {
    borderBottomColor: THEME.TEXT_COLOR_LIGHT,
    borderBottomWidth: getSize(2),
    borderStyle: 'solid',
    marginBottom: getSize(20),
    color: THEME.TEXT_COLOR_LIGHT,
    fontSize: getSize(16),
    paddingTop: getSize(10),
    fontFamily: THEME.MEDIUM
  },
  switchContainer: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    marginBottom: getSize(20),
  },
  switchContainerLabel: {
    fontSize: getSize(16),
    color: THEME.TEXT_COLOR_LIGHT,
    paddingLeft: Platform.OS === 'ios' ? getSize(10) : 0,
  },
})