import React from 'react';
import { StyleSheet, View, Image, TouchableOpacity, Linking } from 'react-native';
import { AntDesign } from '@expo/vector-icons';
import { AppTextBold } from '../ui/AppTextBold'
import { AppTextLight } from '../ui/AppTextLight'
import { AppTextMedium } from '../ui/AppTextMedium'
import { THEME, getSize } from '../../styles/theme';
import VKicon from '../../assets/VK.svg'
import FacebookIcon from '../../assets/Facebook.svg'
import TelegramIcon from '../../assets/Telegram.svg'
import InstagramIcon from '../../assets/Instagram.svg'
import { externalLinks } from '../../constants/url'


export const UserProfile = ({ setShowEditProfile, navigation, avatar }) => {
  const goToSocial = (type) => async () => {
    let url = ''
    if (type === 'vk') {
      url = externalLinks.VK_GROUP
    }
    if (type === 'tg') {
      url = externalLinks.TG_LINK
    }
    if (type === 'ig') {
      url = externalLinks.INSAGRAM_LINK
    }
    if (url) await Linking.openURL(url)
  }

  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <TouchableOpacity style={styles.accountButton} onPress={() => setShowEditProfile(true)}>
          <View style={styles.headerWrapper}>
            {avatar ? (
              <View style={styles.avatar}>
                <Image style={styles.avatarImg} source={{ uri: avatar }} />
              </View>
            ) : (
              <View
                style={styles.avatarMock}>
                <AntDesign
                  style={styles.userMock}
                  name="user"
                  size={32}
                  color={THEME.MAIN_COLOR}
                />
              </View>
            )}

            <View style={styles.textContainer}>
              <AppTextBold style={styles.textContainerHead}>Аккаунт</AppTextBold>
              <AppTextLight style={styles.textContainerDescription}>Личные данные и уведомления</AppTextLight>
            </View>

            <View style={styles.arrow}>
              <AntDesign name="right" size={24} color={THEME.TEXT_COLOR_LIGHT} />
            </View>
          </View>
        </TouchableOpacity>
      </View>

      <View style={styles.menu}>
        <TouchableOpacity onPress={() => {
          navigation.navigate('About')
        }}>
          <AppTextMedium style={styles.menuText}>О проекте</AppTextMedium>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => {
          navigation.navigate('Contacts')
        }}>
          <AppTextMedium style={styles.menuText}>Написать разработчикам</AppTextMedium>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => navigation.navigate('Terms')}>
          <AppTextMedium style={styles.menuText}>Соглашение</AppTextMedium>
        </TouchableOpacity>

      </View>
      <View style={styles.footer}>
        <View style={styles.footerWrapper}>
          <View style={styles.textContainerFooter}>
            <AppTextMedium style={styles.textContainerHead}>
              Наши соцсети
            </AppTextMedium>
            <AppTextLight style={styles.textContainerDescription}>Подписывайтесь и следите за новостями</AppTextLight>
          </View>

          <View style={styles.social}>
            <View style={styles.socialIcon}>
              <TouchableOpacity onPress={goToSocial('vk')}>
                <VKicon />
              </TouchableOpacity>
            </View>
            {/* <View style={styles.socialIcon}>
              <TouchableOpacity onPress={goToSocial('fb')}>
                <FacebookIcon />
              </TouchableOpacity>
            </View> */}
            <View style={styles.socialIcon}>
              <TouchableOpacity onPress={goToSocial('tg')}>
                <TelegramIcon />
              </TouchableOpacity>
            </View>
            <View style={styles.socialIcon}>
              <TouchableOpacity onPress={goToSocial('ig')}>
                <InstagramIcon />
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </View>
      <Image
        style={styles.bgImageStyle}
        source={require('../../assets/bg_menu.png')}
      />
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: getSize(20),
  },
  header: {
  },
  headerWrapper: {
    flexDirection: 'row',
    alignItems: 'flex-start',
    borderBottomColor: THEME.TEXT_COLOR_LIGHT,
    borderBottomWidth: getSize(1),
    borderStyle: 'solid',
    paddingBottom: getSize(20),
    marginBottom: getSize(30),
  },
  avatar: {
    flex: 1,
  },
  avatarImg: {
    width: getSize(40),
    height: getSize(40),
    borderRadius: getSize(80 / 2),
  },
  textContainer: {
    flex: 5,
    marginLeft: getSize(15),
  },
  textContainerHead: {
    fontSize: getSize(20),
    color: THEME.TEXT_COLOR_LIGHT,
  },
  textContainerDescription: {
    fontSize: getSize(12),
    color: THEME.TEXT_COLOR_LIGHT,
  },
  arrow: {
    alignItems: 'flex-end',
    flex: 1,
  },
  menu: {
    flex: 2,
    justifyContent: 'flex-start',
    zIndex: 999,
  },
  menuText: {
    color: THEME.TEXT_COLOR_LIGHT,
    fontSize: getSize(18),
    marginBottom: getSize(40),
  },
  footer: {
    flex: 1,
    zIndex: 10,
  },
  footerWrapper: {
    borderTopColor: THEME.TEXT_COLOR_LIGHT,
    borderTopWidth: getSize(1),
    borderStyle: 'solid',
    paddingTop: getSize(20),
  },
  social: {
    flexDirection: 'row',
    paddingTop: getSize(10),
  },
  socialIcon: {
    paddingRight: getSize(20),
  },
  bgImageStyle: {
    position: 'absolute',
    bottom: getSize(10),
    right: getSize(-90),
    zIndex: 1,
  },
  accountButton: {
    // position: 'absolute',
    zIndex: 10,
  },
  avatarMock: {
    width: getSize(40),
    height: getSize(40),
    borderRadius: getSize(40 / 2),
    backgroundColor: THEME.TEXT_COLOR_LIGHT,
  },
  userMock: {
    textAlign: 'center',
    paddingTop: getSize(5),
  },
})