import React from 'react';
import { StyleSheet, Dimensions, View, TouchableOpacity, Image } from 'react-native';
import { THEME, getSize } from '../../styles/theme';
import { AppText } from '../ui/AppText'


const Todo = ({ todo, checkTodo, deleteTodo, isDisable }) => {
  return (
    <TouchableOpacity
      activeOpacity={0.5}
      onPress={() => {
        if (!isDisable) {
          checkTodo(todo.id, todo.title)
        }
      }}
      onLongPress={() => {
        if (!isDisable) {
          deleteTodo(todo.id)
        }
      }}
    >
      <View style={styles.todo}>
        <Image style={styles.checkbox} source={todo.isChecked ? require('../../assets/checkbox_checked.png') : require('../../assets/checkbox_empty.png')} />
        <AppText style={styles.text}>
          {todo.title}
        </AppText>
      </View>
    </TouchableOpacity>
  )
};

export default Todo


const styles = StyleSheet.create({
  todo: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingTop: getSize(10),
    paddingRight: getSize(10),
    paddingBottom: getSize(10),
    width: Dimensions.get('window').width / 1.2,
  },
  text: {
    fontSize: THEME.FONT_SIZES.h2,
    color: THEME.TEXT_COLOR_LIGHT,
  },
  checkbox: {
    marginRight: getSize(5),
    marginTop: getSize(2),
  }
});