import React, { useEffect, useCallback } from 'react';
import { Alert } from 'react-native'
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import * as firebase from 'firebase';
import { useDispatch, useSelector } from 'react-redux'
import TopBar from '../TopBar'
import { BottomMenu } from '../BottomMenu'
import { ModalMenu } from '../ModalMenu'
import Home from '../../screens/home'
import { Terms } from '../../screens/pages/Terms'
import { About } from '../../screens/pages/About'
import { Contact } from '../../screens/pages/Contact'
import { Motivation } from '../../screens/motivation'
import { Feed } from '../../screens/feed'
import { setUserData, initializeUser, setUserStatus } from '../../actions/userActions'
import { getTodosDict } from '../../actions/todosActions'
import { AppLoader } from '../AppLoader'
import { setMotivationDict } from '../../actions/motivationActions'
import { ShareTodosList } from '../../components/Modals/ShareTodosList'
import { ShareCounter } from '../../components/Modals/ShareCounter'
import UserAchievs from '../../screens/home/Achievements/UserAchievs'
import { ShareMotivationCard } from "../../components/Modals/ShareMotivationCard";

const HomeStack = createStackNavigator();
const MotivationStack = createStackNavigator();
const FeedStack = createStackNavigator();
const Tab = createBottomTabNavigator();
const RootStack = createStackNavigator();

const headerOptions = {
  header: ({ previous, navigation }) => {
    return (
      <TopBar
        previous={previous}
        navigation={navigation}
      />
    )
  },
  headerTitleAlign: 'left',
}

function HomeStackScreen() {
  return (
    <HomeStack.Navigator headerMode='screen'>
      <HomeStack.Screen
        name="Home"
        component={Home}
        options={{ ...headerOptions }}
      />
    </HomeStack.Navigator>
  );
}

function MotivationStackScreen() {
  return (
    <MotivationStack.Navigator headerMode='screen'>
      <MotivationStack.Screen
        name="Motivation"
        component={Motivation}
        options={{ ...headerOptions }}
      />
    </MotivationStack.Navigator>
  );
}

function FeedStackScreen() {
  return (
    <FeedStack.Navigator headerMode='screen'>
      <FeedStack.Screen
        name="Feed"
        component={Feed}
        options={{ ...headerOptions }}
      />
    </FeedStack.Navigator>
  );
}

function TermsStackScreen() {
  return (
    <FeedStack.Navigator headerMode='screen'>
      <FeedStack.Screen
        name="Terms"
        component={Terms}
        options={{ ...headerOptions }}
      />
    </FeedStack.Navigator>
  );
}

function AboutStackScreen() {
  return (
    <FeedStack.Navigator headerMode='screen'>
      <FeedStack.Screen
        name="About"
        component={About}
        options={{ ...headerOptions }}
      />
    </FeedStack.Navigator>
  );
}

function ContactsStackScreen() {
  return (
    <FeedStack.Navigator headerMode='screen'>
      <FeedStack.Screen
        name="Contacts"
        component={Contact}
        options={{ ...headerOptions }}
      />
    </FeedStack.Navigator>
  );
}

function AchievsStackScreen() {
  return (
    <FeedStack.Navigator headerMode='screen'>
      <FeedStack.Screen
        name="UserAchievs"
        component={UserAchievs}
        options={{ ...headerOptions }}
      />
    </FeedStack.Navigator>
  );
}

function HomeTabs() {
  return (
    <Tab.Navigator
      tabBar={props => <BottomMenu {...props} />}
      initialRouteName="Home"
    >
      <Tab.Screen
        name="Motivation"
        component={MotivationStackScreen}
        options={{
          tabBarLabel: "Мотивация",
        }}
      />
      <Tab.Screen
        name="Home"
        component={HomeStackScreen}
      />
      <Tab.Screen
        name="Feed"
        component={FeedStackScreen}
        options={{
          tabBarLabel: "Лента дел"
        }}
      />
    </Tab.Navigator>
  );
}

function TermsTabs() {
  return (
    <Tab.Navigator
      tabBar={props => <BottomMenu {...props} />}
      initialRouteName="Terms"
    >
      <Tab.Screen
        name="Terms"
        component={TermsStackScreen}
      />
      <Tab.Screen
        name="Motivation"
        component={MotivationStackScreen}
        options={{
          tabBarLabel: "Мотивация",
        }}
      />
      <Tab.Screen
        name="Home"
        component={HomeStackScreen}
      />
      <Tab.Screen
        name="Feed"
        component={FeedStackScreen}
        options={{
          tabBarLabel: "Лента дел"
        }}
      />
    </Tab.Navigator>
  );
}

function AboutTabs() {
  return (
    <Tab.Navigator
      tabBar={props => <BottomMenu {...props} />}
      initialRouteName="About"
    >
      <Tab.Screen
        name="About"
        component={AboutStackScreen}
      />
      <Tab.Screen
        name="Motivation"
        component={MotivationStackScreen}
        options={{
          tabBarLabel: "Мотивация",
        }}
      />
      <Tab.Screen
        name="Home"
        component={HomeStackScreen}
      />
      <Tab.Screen
        name="Feed"
        component={FeedStackScreen}
        options={{
          tabBarLabel: "Лента дел"
        }}
      />
    </Tab.Navigator>
  );
}

function ContactsTabs() {
  return (
    <Tab.Navigator
      tabBar={props => <BottomMenu {...props} />}
      initialRouteName="Contacts"
    >
      <Tab.Screen
        name="Contacts"
        component={ContactsStackScreen}
      />
      <Tab.Screen
        name="Motivation"
        component={MotivationStackScreen}
        options={{
          tabBarLabel: "Мотивация",
        }}
      />
      <Tab.Screen
        name="Home"
        component={HomeStackScreen}
      />
      <Tab.Screen
        name="Feed"
        component={FeedStackScreen}
        options={{
          tabBarLabel: "Лента дел"
        }}
      />
    </Tab.Navigator>
  );
}

function AchievTabs() {
  return (
    <Tab.Navigator
      tabBar={props => <BottomMenu {...props} />}
      initialRouteName="UserAchievs"
    >
      <Tab.Screen
        name="UserAchievs"
        component={AchievsStackScreen}
      />
      <Tab.Screen
        name="Motivation"
        component={MotivationStackScreen}
        options={{
          tabBarLabel: "Мотивация",
        }}
      />
      <Tab.Screen
        name="Home"
        component={HomeStackScreen}
      />
      <Tab.Screen
        name="Feed"
        component={FeedStackScreen}
        options={{
          tabBarLabel: "Лента дел"
        }}
      />
    </Tab.Navigator>
  );
}

export const AppContainer = ({ }) => {
  const userId = useSelector(state => state.user.uid)
  const loading = useSelector(state => state.todos.loading)
  const todosDict = useSelector(state => state.todos.dict)

  const dispatch = useDispatch()

  const loginUser = async () => {
    try {
      const response = await firebase.auth().signInAnonymously()
      if (response.user) {
        //сохраняем пользователя в редакс
        dispatch(setUserData(response.user))
      }
      if (response.additionalUserInfo.isNewUser) {
        dispatch(setUserStatus(true))
        dispatch(initializeUser(true))
      } else {
        dispatch(initializeUser())
      }

    } catch (error) {
      Alert('Ошибка подключения. Проверьте интернет-соединение')
    }
  }

  const login = useCallback(async () => await loginUser(), [loginUser])

  useEffect(() => {
    dispatch(getTodosDict())
    dispatch(setMotivationDict())
  }, [])

  useEffect(() => {
    // логиним пользователя только после загрузки словарей
    if (todosDict) {
      login()
    }

  }, [todosDict])

  if (!userId || loading) {
    return <AppLoader />
  }

  return (
    <NavigationContainer>
      <RootStack.Navigator mode="modal" headerMode="none">

        <RootStack.Screen name="Home" component={HomeTabs} />

        <RootStack.Screen name="Terms" component={TermsTabs} />
        <RootStack.Screen name="Contacts" component={ContactsTabs} />
        <RootStack.Screen name="About" component={AboutTabs} />
        <RootStack.Screen name="UserAchievs" component={AchievTabs} />

        <RootStack.Screen name="ModalMenu" component={ModalMenu} />
        <RootStack.Screen name="ShareTodosList" component={ShareTodosList} />
        <RootStack.Screen name="ShareCounter" component={ShareCounter} />
        <RootStack.Screen name="ShareMotivationCard" component={ShareMotivationCard} />


      </RootStack.Navigator>
    </NavigationContainer>
  )
}