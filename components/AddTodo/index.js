import React, { useState } from 'react';
import { StyleSheet, View, TextInput, Keyboard, Alert, Image, TouchableHighlight, Dimensions } from 'react-native';
import { THEME, getSize } from '../../styles/theme';


const AddTodo = ({ onSubmit, setListHeight }) => {
  const [value, setValue] = useState('')

  const pressHandler = (fromSubmit = true) => () => {
    if (!fromSubmit && !value.trim()) {
      return
    }

    if (value.trim()) {
      onSubmit(value)

      setValue('')
      Keyboard.dismiss()
    } else {
      Alert.alert('Название дела не может быть пустым')
    }
  }

  return (
    <View style={styles.block}>
      <TouchableHighlight onPress={pressHandler()}>
        <Image source={require('../../assets/checkbox_empty.png')} />
      </TouchableHighlight>
      <TextInput
        style={styles.input}
        onChangeText={setValue}
        value={value}
        placeholder='Укажи сам'
        autoCorrect={false}
        autoCapitalize='none'
        onBlur={pressHandler(false)}
        onLayout={(event) => {
          var { x, y, width, height } = event.nativeEvent.layout;
          setListHeight(height)
        }}
      />
    </View>
  )
};

export default AddTodo


const styles = StyleSheet.create({
  block: {
    flexDirection: 'row',
    paddingBottom: getSize(20),
    alignItems: 'center',
  },
  input: {
    width: Dimensions.get('window').width / 1.3,
    borderStyle: 'solid',
    borderBottomWidth: getSize(2),
    borderBottomColor: THEME.TEXT_COLOR_LIGHT,
    color: THEME.TEXT_COLOR_LIGHT,
    padding: getSize(10),
  }

});