import React, { useState, useEffect } from 'react';
import { StyleSheet, View, TouchableOpacity, Image, Platform, Keyboard, Dimensions } from 'react-native';
import { THEME, getSize } from '../../styles/theme';
import { AppText } from '../ui/AppText'
// import ShadowTabBar from '../ui/ShadowTapBar'
import HomeBotton from '../../assets/homeButton.svg'
import FeedIcon from '../../assets/feed.svg'
import MotivationIcon from '../../assets/motivation.svg'
import { BoxShadow } from 'react-native-shadow'

const { height, width } = Dimensions.get('window');
const aspectRatio = height / width;
let isIpad = false
if (aspectRatio < 1.6) {
  isIpad = true
}

const shadowOpt = {
  width: getSize(25),
  height: getSize(20),
  color: "#000",
  border: 15,
  radius: 9,
  opacity: 0.2,
  x: Platform.OS !== 'ios'
    ? getSize(41)
    : isIpad
      ? getSize(80)
      : getSize(50),
  y: Platform.OS !== 'ios' ? getSize(-20) : getSize(-28),
  style: {
    flex: 1,
    alignItems: 'center'
  }
}


export function BottomMenu({ state, descriptors, navigation }) {
  const [showTab, setShowTab] = useState(true);

  useEffect(() => {
    Keyboard.addListener('keyboardDidShow', _keyboardDidShow);
    Keyboard.addListener('keyboardDidHide', _keyboardDidHide);

    return () => {
      Keyboard.removeListener('keyboardDidShow', _keyboardDidShow);
      Keyboard.removeListener('keyboardDidHide', _keyboardDidHide);
    };
  }, []);

  const _keyboardDidShow = () => {
    setShowTab(false);
  };

  const _keyboardDidHide = () => {
    setShowTab(true);
  };

  if (showTab) {
    return (
      <View>
        {/* {Platform.OS !== 'ios' ? <ShadowTabBar /> : null} */}
        <View style={styles.navbarWrapper}>

          <View style={styles.navbarBgWrapper}>
            <View style={styles.navbarLeftPart}></View>
            <Image style={styles.navbarImageBg} source={require('../../assets/bottom_menu.png')} />
            <View style={styles.navbarRightPart}></View>
          </View>

          <View style={styles.navbarMenu}>

            {state.routes.map((route, index) => {
              const hiddenButtonNames = ['Terms', 'Contacts', 'UserAchievs', 'About']
              const { options } = descriptors[route.key];
              const label =
                options.tabBarLabel !== undefined
                  ? options.tabBarLabel
                  : options.title !== undefined
                    ? options.title
                    : route.name;

              const isFocused = state.index === index;

              const onPress = () => {
                const event = navigation.emit({
                  type: 'tabPress',
                  target: route.key,
                });

                if (!isFocused && !event.defaultPrevented) {
                  navigation.navigate(route.name);
                }
              };

              const onLongPress = () => {
                navigation.emit({
                  type: 'tabLongPress',
                  target: route.key,
                });
              };

              let iconSrc
              if (label === 'Мотивация') {
                iconSrc = <FeedIcon />
              } else if (label === 'Лента дел') {
                iconSrc = <MotivationIcon />
              }
              if (hiddenButtonNames.includes(route.name)) {
                return null
              }

              if (route.name !== 'Home') {
                return (
                  <TouchableOpacity
                    key={index}
                    accessibilityRole="button"
                    accessibilityState={isFocused ? { selected: true } : {}}
                    accessibilityLabel={options.tabBarAccessibilityLabel}
                    testID={options.tabBarTestID}
                    onPress={onPress}
                    onLongPress={onLongPress}
                    style={styles.navbarMenuButton}
                  >
                    {iconSrc}
                    <AppText style={styles.navbarMenuButtonText}>{label}</AppText>
                  </TouchableOpacity>

                )
                // Home button
              } else {
                return (
                  <BoxShadow setting={shadowOpt} key={index}>
                    <TouchableOpacity
                      accessibilityRole="button"
                      accessibilityState={isFocused ? { selected: true } : {}}
                      accessibilityLabel={options.tabBarAccessibilityLabel}
                      testID={options.tabBarTestID}
                      onPress={onPress}
                      onLongPress={onLongPress}
                      style={styles.navbarMenuButtonMain}
                      activeOpacity={100}
                    >
                      <HomeBotton />
                    </TouchableOpacity>
                  </BoxShadow>
                )
              }
            })}
          </View>
        </View>
      </View>
    )
  } else {
    return null
  }
}
let navbarWrapper = {
  position: 'absolute',
  left: 0,
  right: 0,
  bottom: 0,
  height: Platform.OS !== 'ios' ? getSize(70) : null,
  zIndex: 999
}
if (Platform.OS === 'ios') {
  navbarWrapper = {
    ...navbarWrapper,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 11,
    },
    shadowOpacity: 0.55,
    shadowRadius: 14.78,
    elevation: 22
  }
}

const styles = StyleSheet.create({
  navbarWrapper,
  navbarBgWrapper: {
    flexDirection: 'row',
    paddingTop: Platform.OS !== 'ios' ? getSize(30) : null,
  },
  navbarLeftPart: {
    backgroundColor: THEME.MAIN_COLOR,
    flex: 1,
  },
  navbarImageBg: {
    flex: 1,
    width: getSize(106),
  },
  navbarRightPart: {
    backgroundColor: THEME.MAIN_COLOR,
    flex: 1,
  },
  navbarMenu: {
    position: 'absolute',
    left: 0,
    flexDirection: 'row',
    paddingTop: Platform.OS !== 'ios' ? getSize(40) : getSize(14),
  },
  navbarMenuButton: {
    flex: 1,
    textAlign: 'center',
    alignItems: 'center',
    justifyContent: 'center',
  },
  navbarMenuButtonText: {
    paddingTop: Platform.OS !== 'ios' ? getSize(0) : getSize(3),
    color: THEME.TEXT_COLOR_LIGHT,
    fontSize: THEME.FONT_SIZES.comment,
  },
  navbarMenuButtonMain: {
    marginTop: Platform.OS !== 'ios' ? getSize(-40) : getSize(-50),
    elevation: (Platform.OS === 'android') ? 50 : 0,
  },
  navbarMainButtonRight: {
    flex: 1,
  },
});