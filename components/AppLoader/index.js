import React from 'react';
import { View, StyleSheet } from 'react-native'
import Loader0 from '../../assets/loading_img0.svg'

export const AppLoader = () => {
  return (
    <View style={styles.wrapper}>
      <Loader0 />
    </View>
  )
}

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  }
})