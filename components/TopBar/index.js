import React from 'react';
import { StyleSheet, Platform, View, Dimensions } from 'react-native';
import MenuBurger from './MenuBurger'
import TotalCounter from './TotalCounter'
import AchievCounter from './AchievCounter'
import { THEME, getSize } from '../../styles/theme';


const TopBar = ({ previous, navigation }) => {
  return (
    <View style={styles.navbar}>
      <MenuBurger navigation={navigation} />
      <TotalCounter />
      <AchievCounter navigation={navigation} />
    </View>
  )
};

export default TopBar

const styles = StyleSheet.create({
  navbar: {
    flex: 1,
    backgroundColor: THEME.TEXT_COLOR_LIGHT,
    // backgroundColor: '#333',
    alignItems: 'center',
    justifyContent: 'flex-start',
    flexDirection: 'row',
    paddingHorizontal: getSize(20),
    paddingTop: Platform.OS === 'ios' ? getSize(60) : getSize(40),
    paddingBottom: getSize(30)
  }
});