import React from 'react';
import { StyleSheet, TouchableOpacity, Image } from 'react-native';

const MenuBurger = ({ navigation }) => {
  return (
    <TouchableOpacity
      style={styles.burger}
      onPress={() => navigation.navigate('ModalMenu')}
      hitSlop={{ top: 20, bottom: 20, left: 50, right: 50 }}
    >
      <Image source={require('../../assets/burger.png')} />
    </TouchableOpacity>
  )
};

export default MenuBurger

const styles = StyleSheet.create({
  burger: {
    flex: 1,
  },
});