import React from 'react';
import { useSelector } from 'react-redux'
import { StyleSheet, View, Image, TouchableOpacity } from 'react-native';
import { THEME, getSize } from '../../styles/theme';
import { AppText } from '../ui/AppText'

const AchievCounter = ({ navigation }) => {
  const userAchievements = useSelector(state => state.achievements.achievements)
  const dict = useSelector(state => state.achievements.dict)

  if (!dict) return null

  return (
    <TouchableOpacity
      style={styles.counter}
      onPress={() => navigation.navigate('UserAchievs')}
      underlayColor={THEME.MAIN_COLOR_LIGHT}
      activeOpacity={0.5}
    >
      <Image
        style={styles.icon}
        source={require('../../assets/achiv_header.png')}
      />
      <View style={styles.textContainer}>
        <AppText style={styles.text}>{userAchievements ? userAchievements.length : 0}<AppText style={styles.textMini}>/{dict.length}</AppText> НАГРАД</AppText>
      </View>
    </TouchableOpacity>
  )
};

export default AchievCounter

const styles = StyleSheet.create({
  counter: {
    flex: 2,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  textContainer: {
    backgroundColor: THEME.GREY_LIGHT,
    paddingTop: getSize(5),
    paddingBottom: getSize(5),
    paddingRight: getSize(10),
    paddingLeft: getSize(15),
    borderRadius: getSize(20),
    marginRight: getSize(10),
    height: getSize(20),
  },
  text: {
    fontSize: THEME.FONT_SIZES.comment,
  },
  icon: {
    marginRight: getSize(-10),
    zIndex: 1,
    marginBottom: getSize(5),
  },
  textMini: {
    color: THEME.GREY_DARK,
    fontSize: getSize(7),
  }
});