import React from 'react';
import { useSelector } from 'react-redux'
import { StyleSheet, View, Image, Platform } from 'react-native';
import { THEME, getSize } from '../../styles/theme';
import { AppText } from '../ui/AppText'

const TotalCounter = () => {
  const counter = useSelector(state => state.todos.totalCounter)
  const loading = useSelector(state => state.todos.loading)

  if (loading) {
    return null
  }

  return (
    <View style={styles.counter}>
      <Image
        source={require('../../assets/logo_mini.png')}
        style={styles.icon}
      />
      <AppText style={styles.text}>{counter}</AppText>
    </View>
  )
};

export default TotalCounter

const styles = StyleSheet.create({
  counter: {
    flex: 2,
    flexDirection: 'row',
    alignItems: 'center',
  },
  icon: {
    marginRight: getSize(5)
  },
  text: {
    fontSize: THEME.FONT_SIZES.h2,
    color: THEME.GREY_DARK,
    height: getSize(15)
  }
});