import React from 'react'
import * as ImagePicker from 'expo-image-picker';
import { View, StyleSheet, TouchableOpacity } from 'react-native';
import { useSelector } from 'react-redux'
import * as firebase from 'firebase';
import { AntDesign } from '@expo/vector-icons';
import NetworkImage from 'react-native-image-progress'
import { useActionSheet } from '@expo/react-native-action-sheet'
import ProgressBar from 'react-native-progress/Bar';
import { getSize, THEME } from '../../styles/theme'
import CameraIcon from '../../assets/camera.svg'
import { prepareBlob, askForPermissions } from '../../helpers/image'


export const PhotoPicker = ({ onPick = () => { }, image }) => {
  const userId = useSelector(state => state.user.uid)
  const { showActionSheetWithOptions } = useActionSheet()

  const addImage = () => {
    const options = ['Выбрать фото', 'Сделать фото', 'Отмена'];
    const cancelButtonIndex = 2;

    showActionSheetWithOptions(
      {
        options,
        cancelButtonIndex,
      },
      buttonIndex => {
        if (buttonIndex === 0) {
          //фотопоток
          getPhotoFromLibrary()
        } else if (buttonIndex === 1) {
          //камера
          takePhoto()
        }
      },
    );
  }

  const getPhotoFromLibrary = async () => {
    const hasPermissions = await askForPermissions()
    if (!hasPermissions) {
      return
    }

    const img = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.All,
      allowsEditing: true,
      aspect: [16, 16],
      quality: 0.3
    })

    saveImage(img)
  }

  const takePhoto = async () => {
    const hasPermissions = await askForPermissions()
    if (!hasPermissions) {
      return
    }

    const img = await ImagePicker.launchCameraAsync({
      quality: 0.3,
      allowsEditing: true,
      aspect: [16, 16]
    })

    saveImage(img)
  }

  const saveImage = async (img) => {
    await uploadImage(img.uri, userId)

    const storage = firebase.storage().ref('users').child(userId).child('photo')
    const photoUrl = await storage.getDownloadURL()

    const userFirebase = firebase.auth().currentUser;
    await userFirebase.updateProfile({ photoUrl: photoUrl })

    onPick(photoUrl)
  }

  const uploadImage = async (imageUri, userId) => {
    const ref = firebase.storage().ref('users').child(userId).child('photo')

    try {
      // convert to blob
      const blob = await prepareBlob(imageUri)
      const snapshot = await ref.put(blob)

      let downloadUrl = await ref.getDownloadURL()

      blob.close()

      return downloadUrl

    } catch (error) {
      console.log('error', error);
    }
  }

  return (
    <View style={styles.avatar}>
      {
        image ? (
          <NetworkImage
            source={{ uri: image }}
            indicator={ProgressBar}
            indicatorProps={{
              size: 40,
              borderWidth: 0,
              color: THEME.MAIN_COLOR,
              unfilledColor: 'rgba(200, 200, 200, 0.2)',
            }}
            style={styles.avatarImg}
            imageStyle={styles.avatarImg}
          />
        ) : (
          <View
            style={styles.avatarMock}>
            <AntDesign
              style={styles.userMock}
              name="user"
              size={52}
              color={THEME.MAIN_COLOR}
            />
          </View>
        )
      }

      <TouchableOpacity
        // onPress={takePhoto}
        onPress={addImage}
        activeOpacity={100}
        style={styles.camera}
      >
        <CameraIcon />
      </TouchableOpacity>
    </View>
  )
}

const styles = StyleSheet.create({
  avatarMock: {
    width: getSize(80),
    height: getSize(80),
    borderRadius: getSize(80 / 2),
    backgroundColor: THEME.TEXT_COLOR_LIGHT,
    zIndex: 0,
    elevation: 1,
  },
  userMock: {
    textAlign: 'center',
    paddingTop: getSize(15),
  },
  camera: {
    position: 'absolute',
    right: getSize(-10),
    top: getSize(58 / 2),
    elevation: 4,
    zIndex: 999,
  },
  avatar: {
    width: getSize(80),
    marginRight: getSize(20),
  },
  avatarImg: {
    width: getSize(80),
    height: getSize(80),
    borderRadius: getSize(80 / 2),
  },
})