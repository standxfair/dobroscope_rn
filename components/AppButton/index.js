import React from 'react';
import { StyleSheet, View, TouchableOpacity } from 'react-native';
import { THEME, getSize } from '../../styles/theme';
import { AppText } from '../ui/AppText'

export const AppButton = ({ style, title = 'Поделиться', onPress = () => { } }) => {
  return (
    <View style={style ? style.shareButtonWrapper : styles.shareButtonWrapper}>
      <TouchableOpacity
        underlayColor={THEME.MAIN_COLOR_LIGHT}
        activeOpacity={0.5}
        style={style ? style.shareButton : styles.shareButton}
        onPress={onPress}
      >
        <AppText style={style ? style.shareButtonText : styles.shareButtonText}>{title}</AppText>
      </TouchableOpacity>
    </View>
  )
}

const styles = StyleSheet.create({
  shareButtonWrapper: {
    paddingTop: getSize(28),
    paddingBottom: getSize(60),
    zIndex: 10,
  },
  shareButton: {
    marginRight: getSize(50),
    marginLeft: getSize(50),
    paddingTop: getSize(10),
    paddingBottom: getSize(10),
    backgroundColor: THEME.MAIN_COLOR,
    borderRadius: getSize(30),

    shadowColor: THEME.MAIN_COLOR,
    shadowOffset: {
      width: 0,
      height: 6,
    },
    shadowOpacity: (0.37),
    shadowRadius: (7.49),
    elevation: (12),
  },
  shareButtonText: {
    textAlign: 'center',
    fontSize: getSize(14),
    color: THEME.TEXT_COLOR_LIGHT,
    textTransform: 'uppercase',
  },
})