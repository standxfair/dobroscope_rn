import _ from 'underscore'
import { INITIAL_FEED, FETCH_FEED, ADD_ITEM_TO_FEED, ADD_COMMENT, LIKE_POST } from '../constants/feed'

const initialState = { ...INITIAL_FEED }

export const feed = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_FEED:
      state = { ...state, items: action.payload.feed, loading: action.payload.loading }
      break;

    case ADD_ITEM_TO_FEED:
      const { items } = state
      let resItems = [...items]
      resItems.push(action.payload)

      state = { ...state, items: resItems }
      break;

    case ADD_COMMENT:
      const elem = state.items.find(item => item.id === action.payload.itemId)
      if (elem.comments) {
        elem.comments.push(action.payload.comment)
      } else {
        elem.comments = [action.payload.comment]
      }

      const newStateItems = _.without(state.items, elem)
      state = { ...state, items: [...newStateItems, elem] }
      break;

    case LIKE_POST:
      let post = state.items.find(item => item.id === action.payload.postId)
      post.isUserLiked = action.payload.isUserLiked
      post.likesCount = action.payload.likesCount

      state = { ...state, items: [..._.without(state.items, post), post] }
      break;

    default:
      break;
  }

  return state
}