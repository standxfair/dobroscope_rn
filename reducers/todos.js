import { GET_TODOS, TODOS, FETCH_TODOS, SHOW_LOADER, HIDE_LOADER, SHOW_ERROR, CLEAR_ERROR, FETCH_TODOS_DICT, SET_CUSTOM_TODO, FETCH_CUSTOM_TODOS, SET_TODO_COUNTER, UPDATE_TODO_COUNTER, SET_FIRST_TODO_DONE_STATUS, DELETE_CUSTOM_TODO, FETCH_TODAY_TODOS, SET_DISABLE_TODOS_CHECK } from '../constants/todos'

const initialState = { ...TODOS }

export const todos = (state = initialState, action) => {
  switch (action.type) {
    case GET_TODOS:
      state = { ...state, ...action.payload }
      break;

    case SHOW_LOADER:
      state = { ...state, loading: true, }
      break;

    case HIDE_LOADER:
      state = { ...state, loading: false, }
      break;

    case HIDE_LOADER:
      state = { ...state, loading: false, }
      break;

    case SHOW_ERROR:
      state = { ...state, error: action.payload, }
      break;

    case CLEAR_ERROR:
      state = { ...state, error: null, }
      break;

    case FETCH_TODOS:
      state = { ...state, currentTodos: action.payload, }
      break;

    case FETCH_CUSTOM_TODOS:
      state = { ...state, customTodos: action.payload, }
      break;

    case FETCH_TODOS_DICT:
      state = { ...state, dict: action.payload, }
      break;

    case SET_DISABLE_TODOS_CHECK:
      state = { ...state, isDisable: action.payload, }
      break;

    case SET_CUSTOM_TODO:
      let customTodos = [...state.customTodos]
      customTodos.push(action.payload)

      state = { ...state, customTodos, }
      break;

    case SET_TODO_COUNTER:
    case UPDATE_TODO_COUNTER:
      state = { ...state, totalCounter: action.payload }
      break;

    case SET_FIRST_TODO_DONE_STATUS:
      state = { ...state, isFirstTodoDone: action.payload }
      break;

    case DELETE_CUSTOM_TODO:
      const result = state.customTodos.filter(todo => todo.id !== action.payload)
      state = { ...state, customTodos: result }
      break;

    case FETCH_TODAY_TODOS:
      state = { ...state, todayTodos: action.payload }

    default:
      break;
  }

  return state
}