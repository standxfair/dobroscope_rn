import { FETCH_MOTIVATION_DICT, SET_TODAY_MOTIVATION, MOTIVATION, SET_MOTIVATION_LOADING } from '../constants/motivation'

const initialState = { ...MOTIVATION }

export const motivation = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_MOTIVATION_DICT:
      state = { ...state, dict: action.payload }
      break;

    case SET_TODAY_MOTIVATION:
      state = { ...state, todayMotivation: action.payload }
      break;

    case SET_MOTIVATION_LOADING:
      state = { ...state, loading: action.payload }
      break;

    default:
      break;
  }

  return state
}