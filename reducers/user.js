import {
  SET_USER_DATA, USER_INITIAL_DATA, SET_USER_STATUS, SET_USER_NAME, SET_USER_EMAIL, SET_USER_POSTS_COUNTER, SET_USER_LIKES_COUNTER, SET_AGREE_RECIVE_EMAILS, SET_AGREE_RECIVE_PUSH
} from '../constants/user'

const initialState = { ...USER_INITIAL_DATA }

export const user = (state = initialState, action) => {
  switch (action.type) {
    case SET_USER_DATA:
      state = { ...state, ...action.payload }
      break;

    case SET_USER_STATUS:
      state = { ...state, isNewUser: action.payload }
      break;

    case SET_USER_NAME:
      state = { ...state, displayName: action.payload }
      break;

    case SET_USER_EMAIL:
      state = { ...state, email: action.payload }
      break;

    case SET_USER_POSTS_COUNTER:
      state = { ...state, totalPosts: action.payload }
      break;

    case SET_USER_LIKES_COUNTER:
      state = { ...state, totalLikes: action.payload }
      break;

    case SET_AGREE_RECIVE_EMAILS:
      state = { ...state, agreeReciveEmails: action.payload }
      break;

    case SET_AGREE_RECIVE_PUSH:
      state = { ...state, agreeRecivePush: action.payload }
      break;

    default:
      break;
  }

  return state
}