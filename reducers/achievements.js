import { GET_ACHIEVS, GET_ACHIEVS_DICT, ACHIEVS_INITIAL_STATE, SET_ACHIEVS } from '../constants/achievements'

const initialState = { ...ACHIEVS_INITIAL_STATE }

export const achievements = (state = initialState, action) => {
  switch (action.type) {
    case GET_ACHIEVS_DICT:
      state = { ...state, dict: action.payload }
      break;

    case GET_ACHIEVS:
      state = { ...state, achievements: action.payload }
      break;

    case SET_ACHIEVS:
      state = { ...state, achievements: action.payload }
      break;

    default:
      break;
  }

  return state
}