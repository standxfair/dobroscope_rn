import React, { useState, useEffect } from 'react';
import { KeyboardAvoidingView, TouchableWithoutFeedback, Keyboard, StatusBar, Platform } from 'react-native';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import * as firebase from 'firebase';
import { Provider } from 'react-redux'
import { ActionSheetProvider } from '@expo/react-native-action-sheet'
import { firebaseConfig } from './config/config'
import { isIOS } from './helpers/is'
import { THEME } from './styles/theme';
import { bootstrap } from './bootstrap'
import store from './store'
import { AppContainer } from './components/AppContainer';
import 'moment/locale/ru'
import ErrorBoundary from 'react-native-error-boundary'
import AppLoading from 'expo-app-loading';
import { LogBox } from 'react-native';


export default function App() {
  const [isReady, setIsReady] = useState(false)

  const initializeFirebase = () => {
    firebase.initializeApp(firebaseConfig)
  }

  useEffect(() => {
    if (!firebase.apps.length) {
      initializeFirebase()
    }
    LogBox.ignoreLogs(['Warning: Async Storage has been extracted from react-native core']);

  }, [])

  if (!isReady) {
    return (
      <ErrorBoundary>
        <AppLoading
          startAsync={bootstrap}
          onError={(error) => console.log(error)}
          onFinish={() => setIsReady(true)}
        />
      </ErrorBoundary>
    )
  }

  return (
    <ErrorBoundary>
      <Provider store={store}>
        <ActionSheetProvider>
          <KeyboardAvoidingView
            style={{ flex: 1 }}
            behavior={Platform.OS == "ios" ? "padding" : "height"}
          >
            <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
              <SafeAreaProvider>
                <StatusBar
                  barStyle={'dark-content'}
                  backgroundColor={!isIOS ? THEME.TEXT_COLOR_LIGHT : undefined} // на iOS заливка статусбара нужным цветом происходит автоматом
                />
                <AppContainer />
              </SafeAreaProvider>
            </TouchableWithoutFeedback>
          </KeyboardAvoidingView>
        </ActionSheetProvider>
      </Provider>
    </ErrorBoundary>
  );
}