import * as Permissions from 'expo-permissions';
import { Alert } from 'react-native'

export const prepareBlob = async (imageUri) => {
  const blob = await new Promise((reslove, reject) => {
    const xml = new XMLHttpRequest()

    xml.onload = () => {
      reslove(xml.response)
    }

    xml.onerror = (e) => {
      console.log('e', e);
      reject(new TypeError('Ошибка загрузки изображения'))
    }

    xml.responseType = 'blob'
    xml.open('GET', imageUri, true)
    xml.send()
  })

  return blob
}


export const askForPermissions = async () => {
  const { status } = await Permissions.askAsync(
    Permissions.CAMERA,
    Permissions.CAMERA_ROLL,
  )
  if (status !== 'granted') {
    Alert.alert('Ошибка, вы не дали прав на создание фото')
    return false
  }
  return true
}