import isValid from '../helpers/isValid'
import _ from 'underscore'

export const validateHandler = (type = null, value = null, shema, errors, setErrors, formFields = null) => {
  let validate = {}
  // валидируем все поля
  if (!!formFields) {
    validate = isValid(formFields, shema)

    if (!validate.valid) {
      setErrors({ ...errors, ...validate.state, })
    } else {
      setErrors({})
    }
    // валидируем одно поле
  } else {
    validate = isValid({ [type]: value }, { [type]: shema[type] })

    if (!validate.valid) {
      setErrors({ ...errors, ...validate.state, })
    } else if (errors[type]) {
      setErrors(_.omit(errors, type))
    }
  }

  if (validate.valid) {
    return true
  } else {
    return false
  }
}