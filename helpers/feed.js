import { FIREBASE_URL } from '../constants/url'
import { getUserAvatar } from './user'


export const fetchFeedItems = async () => {
  let result
  try {
    result = await fetch(`${FIREBASE_URL}/feed.json`, {
      method: 'GET',
      headers: { 'Content-Type': 'application/json' },
    })
  } catch (error) {
    console.log('error', error);
  }

  result = await result.json()

  return result || []
}

export const postFeedItem = async (item) => {
  try {
    const response = await fetch(`${FIREBASE_URL}/feed.json`, {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({ ...item })
    })

    const data = await response.json()
    if (data.name) {
      return data.name
    }
  } catch (error) {
    console.log('error', error);
  }

  return null
}

export const postComment = async (item, postId) => {

  try {
    const response = await fetch(`${FIREBASE_URL}/feed/${postId}/comments.json`, {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({ ...item })
    })

    const data = await response.json()
    if (data.name) {
      return data.name
    }
  } catch (error) {
    console.log('error', error);
  }

  return null
}

export const setIsLiked = async (postId, likes) => {
  const body = likes.length ? { ...likes } : null
  try {
    const response = await fetch(`${FIREBASE_URL}/feed/${postId}/likes.json`, {
      method: 'PATCH',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(body)
    })

  } catch (error) {
    console.log('error', error);
  }
}

export const getPostLikes = async (postId) => {
  try {
    const response = await fetch(`${FIREBASE_URL}/feed/${postId}/likes.json`, {
      method: 'GET',
      headers: { 'Content-Type': 'application/json' },
    })
    const likes = await response.json()

    return likes
  } catch (error) {
    console.log('error', error);
  }
}


export const mapFeedItems = async (feed, userId) => {
  let feed_ = Object.keys(feed).map(id => {
    let post = {
      id,
      ...feed[id]
    }
    if (!!post.comments) {
      post.comments = Object.keys(post.comments).map(commentId => ({
        id: commentId,
        ...post.comments[commentId],
      }))
    }

    if (!!post.likes && post.likes.length) {
      post.likesCount = post.likes.length

      if (post.likes.find(item => item === userId)) {
        post.isUserLiked = true
      } else {
        post.isUserLiked = false
      }
    } else {
      post.isUserLiked = false
    }

    return post
  })

  await setAvatars(feed_)

  return feed_
}

export const setAvatars = async (feed) => {
  for (let item of feed) {
    const url = await getUserAvatar(item.userId)
    item.avatar = url
  }
}