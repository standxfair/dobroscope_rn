import { FIREBASE_URL } from '../constants/url'

export const sendFeedbackMessage = async (item) => {
  try {
    const response = await fetch(`${FIREBASE_URL}/feedback.json`, {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({ ...item })
    })

    const data = await response.json()
    if (data.name) {
      return data.name
    }
  } catch (error) {
    console.log('error', error);
  }

  return null
}