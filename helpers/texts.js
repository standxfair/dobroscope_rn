import { FIREBASE_URL } from '../constants/url'

class TextsApi {
  async getTextBase(section) {
    let result = ''
    try {
      const response = await fetch(`${FIREBASE_URL}/texts/${section}.json`, {
        method: 'GET',
        headers: { 'Content-Type': 'application/json' }
      })

      result = await response.json()
    } catch (error) {
      console.log('error', error);
    }
    return result
  }

  async getAgreementText() {
    const text = await this.getTextBase('agreement')
    return text
  }

  async getAboutText() {
    const text = await this.getTextBase('about')
    return text
  }
}

export default new TextsApi()