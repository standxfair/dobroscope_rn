import moment from 'moment'
import { DB_DATE_FORMAT } from '../constants/app'

export const isNextDayVisit = (lastLoginInTime) => {
  const now = moment().format('DD.MM.YYYY');
  const lastVisit = moment(lastLoginInTime).format('DD.MM.YYYY');

  return now !== lastVisit
}

export const isNeedAddNextDayMotivation = (lastItemDate) =>
  moment().format(DB_DATE_FORMAT) !== lastItemDate