import { FIREBASE_URL } from '../constants/url'
import { MAX_NEW_TODOS_NUMBER } from '../constants/todos'
import { isNextDayVisit } from './dateTime'
import _ from 'underscore'

export class TodosHelper {

  static setTodosForToday = (
    dict,
    activeTodosIds = null,
    lastLogin = null,
    finishedIds = [],
    resetTodos = false,
  ) => {
    const addTodos = (activeTodosIds, dict) => {
      while (activeTodosIds.length < MAX_NEW_TODOS_NUMBER) {
        dict.forEach(todo => {
          const isTodoDone = finishedIds.find(id => parseInt(todo.id, 10) === parseInt(id, 10))

          const isAlreadyHas = activeTodosIds.find(id => parseInt(id, 10) === parseInt(todo.id, 10))

          if (!isTodoDone && !isAlreadyHas && activeTodosIds.length < MAX_NEW_TODOS_NUMBER) {
            activeTodosIds.push(todo.id)
          }
        })
      }

      return activeTodosIds
    }

    let activeTodosIds_ = activeTodosIds ? [...activeTodosIds] : []
    // новый пользователь, задания не назначены
    if (!activeTodosIds) {
      let result = []
      while (result.length < 5) {
        const index = _.random(0, dict.length)
        const elem = dict[index]
        if (elem && !result.includes(elem)) {
          result.push(elem)
        }
      }

      result = result.map(todo => {
        return {
          ...todo, isChecked: false
        }
      })

      return result
      // есть назначенные задания
    } else {
      if (!dict) {
        return
      }

      let result
      if (resetTodos) {
        activeTodosIds_ = [...addTodos(activeTodosIds_, dict)]
      }
      const resultIds = [...activeTodosIds_]
      result = resultIds.map(id => {
        const el = dict.find(todo => todo.id === id)
        if (el) {
          return { ...el, isChecked: finishedIds.includes(el.id) }
        }
      })
      return result
    }
  }

  static getTodosIds = (todos, active = true) => {
    let ids = []
    let resultTodos = []

    if (active) {
      resultTodos = todos.filter(todo => !todo.isChecked)
    } else {
      resultTodos = todos.filter(todo => todo.isChecked)
    }

    for (const el of resultTodos) {
      ids.push(el.id)
    }

    return ids
  }

  static getLastId = (todos) => todos.length ? parseInt(todos[todos.length - 1].id, 10) : 0


  static filterCustomTodosForToday = (customTodos, lastLogin) => {
    return customTodos.filter((todo => !todo.isChecked))
  }

  static getNewRandomTodos = ({ dict, finishedIds = [] }) => {
    const TODOS_DAY_MIN_COUNT = 5
    const result = []
    const newTodos = dict.filter(item => {
      return !finishedIds.find(id => parseInt(id, 10) === item.id)
    })
    const resultDict = newTodos.length ? newTodos : dict
    while (result.length < TODOS_DAY_MIN_COUNT) {
      const index = _.random(0, resultDict.length)
      const elem = newTodos[index]
      if (elem && !result.includes(elem)) {
        result.push(newTodos[index])
      }
    }
    return result
  }
}

//записываем список всех заданий в БД
export const exportTodos = async (todos) => {
  try {
    const response = await fetch(`${FIREBASE_URL}/todosDict.json`, {
      method: 'PUT',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({ ...todos })
    })
  } catch (error) {
    console.log('error', error);
  }
}

// получаем пользователя
export const fetchUserById = async (userId) => {
  try {
    const response = await fetch(`${FIREBASE_URL}/users/${userId}.json`, {
      method: 'GET',
      headers: { 'Content-Type': 'application/json' },
    })

    const data = await response.json()

    if (data) {
      return data
    } else {
      return null
    }

  } catch (error) {
    console.log('error', error);
  }
}

export const saveTodosState = async (userId, activeIds, finishedIds) => {
  try {
    const response = await fetch(`${FIREBASE_URL}/users/${userId}.json`, {
      method: 'PATCH',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({ activeIds, finishedIds })
    })
  } catch (error) {
    console.log('error', error);
  }
}

export const updateCustomTodo = async (userId, todoId) => {
  try {
    await fetch(`${FIREBASE_URL}/users/${userId}/customTodos/${todoId}.json`, {
      method: 'PATCH',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({ isChecked: true })
    })
  } catch (error) {
    console.log('error', error);
  }
}

// записываем в базу пользовательское задание
export const pushCustomTodo = async (userId, title, date) => {
  try {
    const response = await fetch(`${FIREBASE_URL}/users/${userId}/customTodos.json`, {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({ title, isChecked: false, date })
    })

    const data = await response.json()

    return data
  } catch (error) {
    console.log('error', error);
  }
}

export const updateCounter = async (userId, counter) => {
  try {
    const response = await fetch(`${FIREBASE_URL}/users/${userId}.json`, {
      method: 'PATCH',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({ totalCounter: counter })
    })

    const data = await response.json()

    return data
  } catch (error) {
    console.log('error', error);
  }
}

export const checkFirstTodoDone = async (userId) => {
  try {
    await fetch(`${FIREBASE_URL}/users/${userId}.json`, {
      method: 'PATCH',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({ isFirstTodoDone: true })
    })
  } catch (error) {
    console.log('error', error);
  }
}

export const deleteCustomTodo = async (userId, todoId) => {
  try {
    await fetch(`${FIREBASE_URL}/users/${userId}/customTodos/${todoId}.json`, {
      method: 'DELETE',
      headers: { 'Content-Type': 'application/json' },
    })
  } catch (error) {
    console.log('error', error);
  }
}

export const saveTodoForTodayFinished = async (userId, title, id, date) => {
  try {
    await fetch(`${FIREBASE_URL}/users/${userId}/todayFinishedTodos.json`, {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({ id, title, date })
    })
  } catch (error) {
    console.log('error', error);
  }
}

export const fetchTodayFinishedTodos = async (userId) => {
  try {
    const response = await fetch(`${FIREBASE_URL}/users/${userId}/todayFinishedTodos.json`, {
      method: 'GET',
      headers: { 'Content-Type': 'application/json' },
    })

    const data = await response.json()

    return Object.keys(data).map(id => ({ id, title: data[id].title }))
  } catch (error) {
    console.log('error', error);
  }
}

export const fetchTodayFinishedCustomTodos = async (userId) => {
  try {
    const response = await fetch(`${FIREBASE_URL}/users/${userId}/customTodos.json`, {
      method: 'GET',
      headers: { 'Content-Type': 'application/json' },
    })
    const data = await response.json() || {}

    return Object.keys(data).map(id => ({
      id,
      title: data[id].title,
      date: data[id].date,
      isChecked: data[id].isChecked,
    }))
  } catch (error) {
    console.log('error', error);
  }
}

export const clearTodayFinishedTodos = async (userId) => {
  try {
    await fetch(`${FIREBASE_URL}/users/${userId}/todayFinishedTodos.json`, {
      method: 'DELETE',
      headers: { 'Content-Type': 'application/json' },
    })
  } catch (error) {
    console.log('error', error);
  }
}

export const deleteTodayFinishedTodo = async (userId, todoId) => {
  try {
    await fetch(`${FIREBASE_URL}/users/${userId}/todayFinishedTodos/${todoId}.json`, {
      method: 'DELETE',
      headers: { 'Content-Type': 'application/json' },
    })
  } catch (error) {
    console.log('error', error);
  }
}

export const isAllTodosDictShown = (dict, history) => {
  const dictIds = dict.map(item => item.id)

  for (let idx of dictIds) {
    if (!history.find(id => id === idx)) {
      return false
    }
  }

  return true
}