import * as firebase from 'firebase';
import { FIREBASE_URL } from '../constants/url'
import { USER_MODEL } from '../constants/user'
import moment from 'moment'

export const setUserModel = ({ activeIds, displayName, lastLoginTime }) => ({
  ...USER_MODEL,
  activeIds,
  displayName,
  lastLoginTime
})

export const saveNewUser = async (userId, activeIds, displayName) => {
  const userData = setUserModel({
    activeIds,
    displayName,
    lastLoginTime: moment().format()
  })
  try {
    await fetch(`${FIREBASE_URL}/users/${userId}.json`, {
      method: 'PATCH',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(userData)
    })

  } catch (error) {
    console.log('error', error);
  }
}

export const setEulaAccept = async ({ isEulaAccepted, userId }) => {
  try {
    await fetch(`${FIREBASE_URL}/users/${userId}.json`, {
      method: 'PATCH',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({ isEulaAccepted })
    })

  } catch (error) {
    console.log('error', error);
  }
}

export const updateUser = async ({ data, userId }) => {
  try {
    await fetch(`${FIREBASE_URL}/users/${userId}.json`, {
      method: 'PATCH',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({ data })
    })

  } catch (error) {
    console.log('error', error);
  }
}

export const getLastLoginTime = async (userId) => {
  try {
    const response = await fetch(`${FIREBASE_URL}/users/${userId}/lastLoginTime.json`, {
      method: 'GET',
      headers: { 'Content-Type': 'application/json' },
    })

    const data = await response.json()
    return data
  } catch (error) {
    console.log('error', error);
  }
}

export const setLastLoginTime = async (userId, lastLoginTime) => {
  try {
    await fetch(`${FIREBASE_URL}/users/${userId}.json`, {
      method: 'PATCH',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({ lastLoginTime })
    })
  } catch (error) {
    console.log('error', error);
  }
}

export const getUserAvatar = async (userId) => {
  const userFirebase = firebase.auth().currentUser;

  let avatar = userFirebase.photoURL
  if (!avatar) {
    const storage = firebase.storage().ref('users').child(userId).child('photo')
    try {
      avatar = await storage.getDownloadURL()
    } catch (error) {
      // console.log('error', error);
    }
  }
  return avatar
}

export const addUserTotalPostsCounter = async (userId, totalPosts) => {
  try {
    await fetch(`${FIREBASE_URL}/users/${userId}.json`, {
      method: 'PATCH',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({ totalPosts })
    })
  } catch (error) {
    console.log('error', error);
  }
}

export const addUserTotalLikesCounter = async (userId, totalLikes) => {
  try {
    await fetch(`${FIREBASE_URL}/users/${userId}.json`, {
      method: 'PATCH',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({ totalLikes })
    })
  } catch (error) {
    console.log('error', error);
  }
}

export const setUserEmailAgreement = async (userId, agreeReciveEmails) => {
  try {
    await fetch(`${FIREBASE_URL}/users/${userId}.json`, {
      method: 'PATCH',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({ agreeReciveEmails })
    })
  } catch (error) {
    console.log('error', error);
  }
}

export const setUserPushAgreement = async (userId, agreeRecivePush) => {
  try {
    await fetch(`${FIREBASE_URL}/users/${userId}.json`, {
      method: 'PATCH',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({ agreeRecivePush })
    })
  } catch (error) {
    console.log('error', error);
  }
}

export const setUserEmailOnFirebase = async (userId, email) => {
  try {
    await fetch(`${FIREBASE_URL}/users/${userId}.json`, {
      method: 'PATCH',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({ email })
    })
  } catch (error) {
    console.log('error', error);
  }
}