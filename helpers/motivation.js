import { FIREBASE_URL } from '../constants/url'
import { DB_DATE_FORMAT } from '../constants/app'
import moment from 'moment'
import _ from 'underscore'

export const exportMotivationDict = async (motivationDict) => {
  try {
    await fetch(`${FIREBASE_URL}/motivationDict.json`, {
      method: 'PUT',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({ ...motivationDict })
    })
  } catch (error) {
    console.log('error', error);
  }
}

export const addNewMotivItem = (history, dict) => {
  let result = []

  // берем все не показанные цитаты
  let dict_ = dict.filter(el => {
    const res = history.find(hist => hist.item.id === el.id)
    if (!res) {
      return el
    }
  })

  const newEl = {
    date: moment().format(DB_DATE_FORMAT),
    item: {
      ...dict_[_.random(0, dict_.length - 1)]
    },
  }

  result.push(newEl)
  result = [
    ...result,
    ...history.map(item => {
      const el = dict.find(d => d.id === item.id)
      return {
        ...item,
        ...el
      }
    })
  ]

  return result
}

export const updateMotivation = async (userId, item, date) => {
  try {
    await fetch(`${FIREBASE_URL}/users/${userId}/motivation.json`, {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({ item, date })
    })
  } catch (error) {
    console.log('error', error);
  }
}

// export const updateMotivationList = async (userId, data) => {
//   try {
//     await fetch(`${FIREBASE_URL}/users/${userId}/motivation.json`, {
//       method: 'POST',
//       headers: { 'Content-Type': 'application/json' },
//       body: JSON.stringify(data)
//     })
//   } catch (error) {
//     console.log('error', error);
//   }
// }

export const deleteMotivation = async (userId) => {
  try {
    await fetch(`${FIREBASE_URL}/users/${userId}/motivation.json`, {
      method: 'DELETE',
      headers: { 'Content-Type': 'application/json' },
    })
  } catch (error) {
    console.log('error', error);
  }
}

export const getMotivation = async (userId) => {
  try {
    const response = await fetch(`${FIREBASE_URL}/users/${userId}/motivation.json`, {
      method: 'GET',
      headers: { 'Content-Type': 'application/json' },
    })

    const data = await response.json()

    return data
  } catch (error) {
    console.log('error', error);
  }
}

export const isAllDictShown = (dict, history) => {
  const dictIds = dict.map(item => item.id)
  const historyIds = history.map(el => el.item.id)

  for (let idx of dictIds) {
    if (!historyIds.find(id => id === idx)) {
      return false
    }
  }

  return true
}

const getUniqueMotivationsDict = ({ history, dict }) => {
  return dict.filter(el => {
    const res = history.find(hist => hist.item.id === el.id)
    if (!res) {
      return el
    }
  })
}

const getMotivationDates = (dateStart) => {
  const dateEnd = moment().format(DB_DATE_FORMAT)
  const dates = []
  let currentDate = dateStart
  while (currentDate !== dateEnd) {
    const date = moment(currentDate, DB_DATE_FORMAT)
      .add(1, 'days')
      .format(DB_DATE_FORMAT)
    dates.push(date)
    currentDate = date
  }
  return dates
}

export const fillMotivation = ({ dateStart, dict, history }) => {
  const dates = getMotivationDates(dateStart)
  const dict_ = getUniqueMotivationsDict({ dict, history })
  return dates.map((date, index) => {
    return {
      date,
      item: {
        ...dict_[index]
      },
    }
  })
}
