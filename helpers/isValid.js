//  model - валидируемая модель
//  schema - схема валидации (см. https://github.com/flatiron/revalidator)
//  CONFORM_DATA - дополнительные данные которые нужно пробросить в conform или error  (т.к. оттуда не добраться до редакса)
function is_Valid(model = {}, schema = {}, CONFORM_DATA = {}) {

  let FORMATS = {

    // eslint-disable-next-line
    'email': /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+/,
    'ip-address': /^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/i,
    'ipv6': /^([0-9A-Fa-f]{1,4}:){7}[0-9A-Fa-f]{1,4}$/,
    'date-time': /^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}(?:.\d{1,3})?Z$/,
    'date': /^\d{4}-\d{2}-\d{2}$/,
    'time': /^\d{2}:\d{2}:\d{2}$/,
    'color': /^#[a-z0-9]{6}|#[a-z0-9]{3}|(?:rgb\(\s*(?:[+-]?\d+%?)\s*,\s*(?:[+-]?\d+%?)\s*,\s*(?:[+-]?\d+%?)\s*\))aqua|black|blue|fuchsia|gray|green|lime|maroon|navy|olive|orange|purple|red|silver|teal|white|yellow$/i,

    // eslint-disable-next-line
    'host-name': /^(([a-zA-Z]|[a-zA-Z][a-zA-Z0-9\-]*[a-zA-Z0-9])\.)*([A-Za-z]|[A-Za-z][A-Za-z0-9\-]*[A-Za-z0-9])/,
  };


  let SCHEMA = {};
  for (let schem in schema)
    if (model[schem] !== undefined || schema[schem]['without_model'])
      SCHEMA[schem] = schema[schem];

  // return isValid(model, SCHEMA);


  // console.log(model, SCHEMA);

  let IF_VALID_GLOB = true;
  let ERROR_GLOB_MSG = {};


  for (let SCHEMA_NAME in SCHEMA) {
    if (SCHEMA.hasOwnProperty(SCHEMA_NAME)) {


      let SCHEM = SCHEMA[SCHEMA_NAME];
      let CHECK_VALUE = model[SCHEMA_NAME] || undefined;

      if (!SCHEM.message && !SCHEM.messages) {
        console.error('not fount message or messages on ' + SCHEMA_NAME, SCHEM);
        continue;
      }



      for (var ITEM_SCHEM in SCHEM) {
        if (SCHEM.hasOwnProperty(ITEM_SCHEM)) {


          if (ITEM_SCHEM === 'message' || ITEM_SCHEM === 'messages')
            continue;


          let IF_ERRO = false;
          let ITEM_SCHEM_VALUE = SCHEM[ITEM_SCHEM];
          let MESSAGE = (SCHEM.messages && SCHEM.messages[ITEM_SCHEM]) ? SCHEM.messages[ITEM_SCHEM] : SCHEM.message;







          // if ( CHECK_VALUE === 'DOB' ) {
          //     console.log('SCHEMA_NAME', SCHEMA_NAME);
          //     console.log('CHECK_VALUE', CHECK_VALUE);
          //     console.log('SCHEM', SCHEM);
          //     console.log('MESSAGE', MESSAGE);
          //     console.log('ITEM_SCHEM', ITEM_SCHEM);
          //     console.log('ITEM_SCHEM_VALUE', ITEM_SCHEM_VALUE);
          //     console.log('***********************************');
          // }




          // eslint-disable-next-line
          switch (ITEM_SCHEM) {
            case 'type':

              //  http://javascript.ru/typeof
              if (CHECK_VALUE && typeof (CHECK_VALUE) !== ITEM_SCHEM_VALUE)
                IF_ERRO = true;

              break;
            case 'required':

              if (ITEM_SCHEM_VALUE === true && CHECK_VALUE === undefined)
                IF_ERRO = true;

              break;
            case 'pattern':
              if (CHECK_VALUE && !ITEM_SCHEM_VALUE.test(CHECK_VALUE))
                IF_ERRO = true;

              break;
            case 'minItems':

              if (typeof (CHECK_VALUE) !== 'object' || CHECK_VALUE.length < ITEM_SCHEM_VALUE)
                IF_ERRO = true;

              break;
            case 'maxItems':

              if (typeof (CHECK_VALUE) !== 'object' || CHECK_VALUE.length > ITEM_SCHEM_VALUE)
                IF_ERRO = true;

              break;
            case 'enum':

              if (!ITEM_SCHEM_VALUE.indexOf(CHECK_VALUE) + 1)
                IF_ERRO = true;

              break;
            case 'minLength':

              if (CHECK_VALUE.length < ITEM_SCHEM_VALUE)
                IF_ERRO = true;

              break;
            case 'maxLength':

              if (CHECK_VALUE.length > ITEM_SCHEM_VALUE)
                IF_ERRO = true;

              break;
            case 'format':

              if (!FORMATS[ITEM_SCHEM_VALUE])
                console.error('Not found format - ' + ITEM_SCHEM_VALUE);

              if (FORMATS[ITEM_SCHEM_VALUE] && !FORMATS[ITEM_SCHEM_VALUE].test(CHECK_VALUE))
                IF_ERRO = true;

              break;
            case 'conform':
            case 'error':

              if (typeof (ITEM_SCHEM_VALUE) === 'function') {

                let res_func = {};

                if (SCHEM['without_model'])
                  res_func = ITEM_SCHEM_VALUE(model, SCHEMA_NAME, CONFORM_DATA);
                else
                  res_func = ITEM_SCHEM_VALUE(CHECK_VALUE, model, SCHEMA_NAME, CONFORM_DATA);


                if (res_func !== undefined && res_func !== null && res_func !== false) {
                  IF_ERRO = true;

                  if (typeof (res_func) === 'string')
                    MESSAGE = res_func;
                }

              }

              break;
          }



          // MESSAGE = MESSAGE + ' (' + ITEM_SCHEM + ')';


          if (!ERROR_GLOB_MSG[SCHEMA_NAME])
            ERROR_GLOB_MSG[SCHEMA_NAME] = null;

          if (IF_ERRO)
            ERROR_GLOB_MSG[SCHEMA_NAME] = MESSAGE;

          if (IF_VALID_GLOB && IF_ERRO)
            IF_VALID_GLOB = false;




        }
      }

    }
  }


  // console.log({
  //     valid: IF_VALID_GLOB,
  //     state: ERROR_GLOB_MSG
  // });

  return {
    valid: IF_VALID_GLOB,
    state: ERROR_GLOB_MSG
  };

}

export default is_Valid;