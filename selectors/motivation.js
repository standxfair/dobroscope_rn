import { createSelector } from 'reselect'
import moment from 'moment'
import { DB_DATE_FORMAT } from '../constants/app'

export const getMotivation = state => state.motivation.todayMotivation

export const getSortedMotivation = createSelector(
  [getMotivation], (motivation) => {
    if (!motivation || !motivation.length) {
      return []
    }

    let res = motivation.filter(item => {
      return item.date !== moment().format(DB_DATE_FORMAT)
    })

    res = res.sort((a, b) => {
      if (moment(a.date) > moment(b.date)) {
        return -1
      } else {
        return 1
      }
    })

    return res
  })


export const getCurrentDayMotivation = createSelector(
  [getMotivation], (motivation) => {
    if (!motivation || !motivation.length) {
      return
    }
    const today = moment().format(DB_DATE_FORMAT)
    const todayItem = motivation.find(el => el.date === today)
    return todayItem ? todayItem.item : null
  })