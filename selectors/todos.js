import { createSelector } from 'reselect'
import moment from 'moment'
import { DB_DATE_FORMAT } from '../constants/app'

export const getTodos = state => state.todos.todayTodos


export const getFilteredByTodayTodos = createSelector(
  [getTodos], (todos) => {
    if (!todos) {
      return []
    }

    return todos.filter(todo => {
      const today = moment().format(DB_DATE_FORMAT)
      return todo?.date === today
    })
  })