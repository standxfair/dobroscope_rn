import { createSelector } from 'reselect'
import moment from 'moment'

export const getFeed = state => state.feed.items


export const getSortedFeed = createSelector(
  [getFeed], (feed) => {
    if (!feed || !feed.length) {
      return []
    }

    const res = feed.sort((a, b) => {
      if (moment(a.date) > moment(b.date)) {
        return -1
      } else {
        return 1
      }
    })

    return res
  })