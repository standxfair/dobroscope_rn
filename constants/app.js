export const DB_DATE_FORMAT = 'MM-DD-YYYY'
export const DB_DATE_TIME_FORMAT = "YYYY-MM-DD HH:mm:ss"
export const COMMENT_TEXT_LENGHT = 1080
export const TODOS_MIN_LENGTH = 5
