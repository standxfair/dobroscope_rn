export const INITIAL_FEED = {
  items: [],
  loading: true,
}

export const FETCH_FEED = 'FETCH_FEED'
export const ADD_ITEM_TO_FEED = 'ADD_ITEM_TO_FEED'
export const LIKE_POST = 'LIKE_POST'
export const ADD_COMMENT = 'ADD_COMMENT'