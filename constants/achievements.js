export const GET_ACHIEVS = 'GET_ACHIEVS'
export const GET_ACHIEVS_DICT = 'GET_ACHIEVS_DICT'
export const SET_ACHIEVS = 'SET_ACHIEVS'

export const ACHIEVS_INITIAL_STATE = {
  dict: [],
  achievements: []
}

export const achievementsEnum = {
  TEN: '10',
  TEN_5: '50',
  TEN_10: '100',
  DONATE: 'donater',
  BEST_DONATE: 'bestDonater'
}