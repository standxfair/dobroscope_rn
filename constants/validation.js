export const USER_SHCEMA = {
  email: {
    required: true,
    type: 'string',
    format: 'email',
    messages: {
      type: 'Проверьте правильность почты',
      format: 'Проверьте правильность почты',
      required: 'Укажите почту',
    }
  },
  name: {
    required: true,
    type: 'string',
    messages: {
      required: 'Поле обязательно к заполнению',
    },
    pattern: /^[\s\-a-zA-zА-Я-а-я]{1,100}$/i,
  },
}

export const USER_AUTH_SCHEMA = {
  email: {
    required: true,
    type: 'string',
    format: 'email',
    messages: {
      type: 'Проверьте правильность почты',
      format: 'Проверьте правильность почты',
      required: 'Укажите почту',
    }
  },
  password: {
    required: true,
    type: 'string',
    messages: {
      required: 'Введите пароль',
    }
  }
}

export const CONTACTS_SHCEMA = {
  email: {
    required: true,
    type: 'string',
    format: 'email',
    messages: {
      type: 'Проверьте правильность почты',
      format: 'Проверьте правильность почты',
      required: 'Укажите почту',
    }
  },
  name: {
    required: true,
    type: 'string',
    messages: {
      required: 'Поле обязательно к заполнению',
    },
    pattern: /^[\s\-a-zA-zА-Я-а-я]{1,100}$/i,
  },
  message: {
    required: true,
    type: 'string',
    messages: {
      required: 'Поле обязательно к заполнению',
    },
  },
}

export const LOGIN_SHCEMA = {
  name: {
    required: true,
    type: 'string',
    message: 'Укажите имя',
  },
}