export const GET_TODOS = 'GET_TODOS'
export const SHOW_ERROR = 'SHOW_ERROR'
export const CLEAR_ERROR = 'CLEAR_ERROR '
export const FETCH_TODOS = 'FETCH_TODOS'
export const FETCH_CUSTOM_TODOS = 'FETCH_CUSTOM_TODOS'
export const FETCH_TODOS_DICT = 'FETCH_TODOS_DICT'
export const SHOW_LOADER = 'SHOW_LOADER'
export const HIDE_LOADER = 'HIDE_LOADER'
export const SET_CUSTOM_TODO = 'SET_CUSTOM_TODO'
export const UPDATE_TODO_COUNTER = 'UPDATE_TODO_COUNTER'
export const SET_TODO_COUNTER = 'SET_TODO_COUNTER'
export const SET_FIRST_TODO_DONE_STATUS = 'SET_FIRST_TODO_DONE_STATUS'
export const DELETE_CUSTOM_TODO = 'DELETE_CUSTOM_TODO'
export const DELETE_TODO = 'DELETE_TODO'
export const FETCH_TODAY_TODOS = 'FETCH_TODAY_TODOS'
export const SET_DISABLE_TODOS_CHECK = 'SET_DISABLE_TODOS_CHECK'

export const TODOS = {
  currentTodos: [],
  customTodos: [],
  totalCounter: 0,
  isFirstTodoDone: false,
  loading: true,
  todayTodos: null,
  isDisable: false,
}

export const TODOS_DICT = [
  {
    id: '1',
    title: 'Позвони родителям',
  },
  {
    id: '2',
    title: 'Купи что-нибудь у уличного продавца',
  },
  {
    id: '3',
    title: 'Накорми бездомного',
  },
  {
    id: '4',
    title: 'Выкинь кем-то брошенный на тротуар мусор в урну',
  },
  {
    id: '5',
    title: 'Сделай комплимент незнакомому человеку',
  },
  {
    id: '6',
    title: 'Убраться во дворе',
  },
  {
    id: '7',
    title: 'Поменять лампочку в подъезде',
  },
  {
    id: '8',
    title: 'Стать донором',
  },
  {
    id: '9',
    title: 'Стать волонтёром',
  },
  {
    id: '10',
    title: 'Уступить место в общественном транспорте',
  },
]

export const MAX_NEW_TODOS_NUMBER = 5