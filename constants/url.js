export const FIREBASE_URL = 'https://dobroscope-default-rtdb.firebaseio.com/'

export const VK_GROUP = 'https://vk.com/dobroscope'
export const externalLinks = {
  VK_GROUP: 'https://vk.com/dobroscope',
  TG_LINK: 'https://t.me/dobroscope',
  INSAGRAM_LINK: 'https://www.instagram.com/dobroscopeapp/',
  HOMEPAGE: 'http://dobroscope.com'
}