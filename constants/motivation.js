export const FETCH_MOTIVATION_DICT = 'FETCH_MOTIVATION_DICT'
export const SET_TODAY_MOTIVATION = 'SET_TODAY_MOTIVATION'
export const SET_MOTIVATION_LOADING = 'SET_MOTIVATION_LOADING'

export const MOTIVATION_DICT = [
  {
    id: 1,
    title: 'Живут лишь те, кто творит добро.',
    author: 'Л.Н. Толстой',
    type: 'Цитата',
  },
  {
    id: 2,
    title: 'Не упускайте случая делать добро.',
    author: 'М. Твен',
    type: 'Цитата',
  },
  {
    id: 3,
    title: 'Никакая сила не может изменить природу добра.',
    author: '',
    type: 'Мотивация',
  },
  {
    id: 4,
    title: 'Кто хочет приносить пользу, тот даже со связанными руками может сделать много добра.',
    author: 'Ф.М. Достоевский',
    type: 'Цитата',
  },
  {
    id: 5,
    title: 'Живут лишь те, кто творит добро.',
    author: 'Л.Н. Толстой',
    type: 'Цитата',
  },
]

export const MOTIVATION = {
  dict: [],
  todayMotivation: [],
  loading: false,
}