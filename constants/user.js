export const SET_USER_DATA = 'SET_USER_DATA'
export const SET_USER_STATUS = 'SET_USER_STATUS'
export const SET_USER_NAME = 'SET_USER_NAME'
export const SET_USER_EMAIL = 'SET_USER_EMAIL'
export const SET_USER_POSTS_COUNTER = 'SET_USER_POSTS_COUNTER'
export const SET_USER_LIKES_COUNTER = 'SET_USER_LIKES_COUNTER'
export const SET_AGREE_RECIVE_EMAILS = 'SET_AGREE_RECIVE_EMAILS'
export const SET_AGREE_RECIVE_PUSH = 'SET_AGREE_RECIVE_PUSH'

// модель для редакса
export const USER_INITIAL_DATA = {
  uid: '',
  isNewUser: false,
  totalPosts: 0,
  totalLikes: 0,
  agreeReciveEmails: false,
  agreeRecivePush: false,
}

// модель для базы
export const USER_MODEL = {
  totalCounter: 0,
  isFirstTodoDone: false,
  totalPosts: 0,
  totalLikes: 0,
  agreeReciveEmails: false,
  agreeRecivePush: false,
  email: '',
  achievements: []
}